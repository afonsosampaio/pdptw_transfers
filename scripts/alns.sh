#!/bin/bash

set -e
#get the root dir (3rd ancestor of the location where this script is stored)
SRC_DIR=`dirname "$BASH_SOURCE"`/../..

function runBenchmark(){
  instances=($(cat ./scripts/instances_json.lst))
  outputfile=alns_long.log
  #run the benchmark
  printf "%s\n" "${instances[@]}" | parallel --no-notice -P 2 -k --eta --colsep ' ' "./ALNS/main -f {} -p ./ALNS/params.json -b -o ALNS/results.csv" >> $outputfile
}

#pushd $SRC_DIR
runBenchmark
#popd
