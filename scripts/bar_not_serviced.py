#! /usr/bin/env python3.6

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import colors as mcolors
from matplotlib.lines import Line2D
import numpy as np
import json as js
# from matplotlib.font_manager import FontProperties

font = {'family' : 'normal',
        'size'   : 24}

mpl.rc('font', **font)


patterns = [ "/" , "\\" , "|" , "-" , "+" , "x", "o", "O", ".", "*" ]

fig, axs = plt.subplots(1,2,sharex=False,sharey=True)

plt.subplots_adjust(left=0.05, bottom=0.08, right=0.95, top=0.96, wspace=0.05, hspace=0.30)

ind1 = np.arange(20)
RA = [22,18,22,21,21,17,24,20,19,22,18,15,8,16,13,18,12,13,12,15]
QA = [11,11,18,12,12,10,14,11,14,7,11,9,5,11,9,10,11,6,6,10]

b1 = axs[0].bar(ind1,RA,0.4,label='without transfers',hatch=patterns[1],edgecolor='blue',alpha=0.5)
b2 = axs[0].bar(ind1+0.4,QA,0.4,label='with transfers',hatch=patterns[0])

axs[0].tick_params(axis='both', which='major', labelsize=20)

ins = ['$L_0$','$L_1$','$L_2$','$L_3$','$L_4$','$L_5$','$L_6$','$L_7$','$L_8$','$L_9$','$M_0$','$M_1$','$M_2$','$M_3$','$M_4$','$M_5$','$M_6$','$M_7$','$M_8$','$M_9$']
axs[0].set_xticks(np.arange(0,20,1)+0.2)
axs[0].set_xticklabels(ins)

axs[0].set_title('50 requests')
axs[0].set_xlabel('Instance')
axs[0].set_ylabel('# requests not inserted')
axs[0].grid()
axs[0].grid(which='minor', linestyle=':', linewidth='0.5')
axs[0].legend()

SA = [41,44,41,51,45,42,47,36,48,41,37,28,25,27,24,37,26,24,35,34]
TA = [18,23,26,30,23,27,30,22,29,18,22,18,16,17,15,24,20,14,23,24]

c1 = axs[1].bar(ind1,SA,0.4,label='without transfers',hatch=patterns[1],edgecolor='blue',alpha=0.5)
c2 = axs[1].bar(ind1+0.4,TA,0.4,label='with transfers',hatch=patterns[0])

axs[1].set_title('100 requests')
axs[1].tick_params(axis='both', which='major', labelsize=20)
axs[1].set_xlabel('Instance')
axs[1].set_xticks(np.arange(0,20,1)+0.25)
axs[1].set_xticklabels(ins)

axs[1].grid()
axs[1].grid(which='minor', linestyle=':', linewidth='0.5')
axs[1].legend()

plt.show()
