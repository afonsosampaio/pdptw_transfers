#!/bin/bash

set -e
#get the root dir (3rd ancestor of the location where this script is stored)
SRC_DIR=`dirname "$BASH_SOURCE"`/../..

function runBenchmark(){
  instances=($(cat ./scripts/instances.lst))

  #run the benchmark
  printf "%s\n" "${instances[@]}" | parallel --no-notice -P 2 -k --eta --colsep ' ' "./Ropke/main_general.py -f {} -t 0"
}

#pushd $SRC_DIR
runBenchmark
#popd
