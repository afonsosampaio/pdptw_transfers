#ifndef WORST_H
#define WORST_H

#include <iostream>  //TODO: not needed!
#include <queue>
#include "Metrics.h"
#include "Operator_base.h"
#include "Route.h"

using namespace std;

class RemoveWorst : public ALNS_Operator {
 public:
  RemoveWorst(string, vector<requestData*>*, double, double,
              default_random_engine*, uniform_real_distribution<double>*,
              double);
  int execute(Solution*);
  void updateScore(int);

 private:
  priority_queue<pair<distanceType, int> > requests_order;
  vector<requestData*>* requests;
  // default_random_engine* rand_eng;
  // uniform_real_distribution<double>* uni_dist;
  double min_removal;
  double max_removal;
};

#endif
