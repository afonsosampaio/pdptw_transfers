#include "insertion_BasicTransfer.h"

InsertBasicTrs::InsertBasicTrs(string nm, vector<requestData*>* reqs,
                               Metric* met, int sel, default_random_engine* gen,
                               uniform_real_distribution<double>* dist,
                               double r)
    : ALNS_Operator(nm, gen, dist, r) {
  // basic_insertion = basic;
  requests = reqs;
  metricMatrix = met;
  selectionOrder = sel;
  insertionTracking.reserve(512);  // FIXME
  int n = 2 * requests->size();
  for (int i = 0; i <= n; i++) customers.push_back(nodeInfo());
}

// select requests based on the distance between p and d
// once ordered, insertion does not affect the order
int InsertBasicTrs::greedyDistance(Solution* solution) {
  // ALERT("greedTransfer\n");
  int n = requests->size();
  Route* rp;
  Route* rd;
  int p, d, t, r;
  distanceType cost;
  int toInsert = 0;
  transferIterator it;
  routesIterator rIT;
  bool feasible;

  for (int i = 0; i < n; ++i) {
    p = requests->at(i)->pickup;
    d = requests->at(i)->delivery;
    if (insertionTracking[i] == false) {
      cost = metricMatrix->distance(p, d);
      requests_order.push(make_pair(cost, i));
      toInsert++;
    }
  }

  while (!requests_order.empty()) {
    r = requests_order.top().second;
    p = requests->at(r)->pickup;
    d = requests->at(r)->delivery;

    // ALERT("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
    // insertions_pq should be empty!
    while (!insertions_pq.empty()) insertions_pq.pop();

    for (int i = 0; i < solution->numRoutes; ++i) {
      rp = solution->routes[i];
      // iterate transfers visited by rp
      for (it = rp->first_transfer(); it != rp->last_transfer(); ++it) {
        t = it->first;
        feasible = rp->evaluateInsertWithTransfer(p, t, -1, -1, nullptr, -1,
                                                  insertions_pq);
      }
    }

    // best insertion over all routes
    // cost(c), before1(b1), after1(a1), before2(b2), after2(a2), transfer(t),
    // route1(r1), route2(r2)
    if (!insertions_pq.empty())  // its feasible
    {
      rp = insertions_pq.top().route1;
      rd = insertions_pq.top().route2;
      t = insertions_pq.top().transfer;

      // DEBUG
      // cout<<"Insertion: "<<p<<"--"<<t<<"--"<<d<<endl;
      // cout<<rp<<": "<<insertions_pq.top().before1<<"
      // "<<insertions_pq.top().after1<<endl; cout<<rd<<":
      // "<<insertions_pq.top().before2<<" "<<insertions_pq.top().after2<<endl;
      // ALERT("pickup:");
      // rp->show(1);
      // for(rIT=rp->first_depRoute(t); rIT!=rp->last_depRoute(t); ++rIT)
      // (*rIT)->show(1);
      // for(rIT=rp->first_arrRoute(t); rIT!=rp->last_arrRoute(t); ++rIT)
      // (*rIT)->show(1);
      // ALERT("delivery:");
      // rd->show(1);
      // for(rIT=rd->first_depRoute(t); rIT!=rd->last_depRoute(t); ++rIT)
      // (*rIT)->show(1);
      // for(rIT=rd->first_arrRoute(t); rIT!=rd->last_arrRoute(t); ++rIT)
      // (*rIT)->show(1);
      assert(rp != nullptr and rd != nullptr);

      rp->addTrsDrop(rd, t, *(requests->at(r)));
      rd->addTrsPick(rp, t, *(requests->at(r)));
      rp->insertCustomer(p, insertions_pq.top().before1,
                         insertions_pq.top().after1);
      rp->updateEarliest(p);
      rp->updateLatest(p);
      rd->insertCustomer(d, insertions_pq.top().before2,
                         insertions_pq.top().after2);
      rd->updateEarliest(d);
      rd->updateLatest(d);
      // rp->flagRoute();
      // rd->flagRoute();
      // rd->earliestTimes();    //calls rp->earliestTimes
      // rp->latestTimes();      //calls rd->latestTimes

      insertionTracking[r] = true;
      toInsert--;

      assert(rp->isFeasible());
      assert(rd->isFeasible());
      // ALERT("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
    }
    // else
    //   cout<<"Insertion for "<<p<<","<<d<<" is infeasible (with
    //   transfer)...\n";
    requests_order.pop();
  }
  // WATCH(toInsert);

  return toInsert;
}

// select requests with the difference between (best) insertion with transfer
// and (best) insertion without transfer: max Ft - Fn
//
int InsertBasicTrs::regretTransfer(Solution* solution) {
  //   ALERT("REGRET-Transfer insertion\n");
  int n = requests->size();
  Route* rp;
  Route* rd;
  int p, d, t, r;
  distanceType cost;
  int toInsert = 0;
  transferIterator it;
  routesIterator rIT;
  bool feasible;
  set<Route*>::iterator e1;
  set<Route*>::iterator e2;

  // save transfer visit times for the solution
  transferIterator itT;
  pair<timeType, timeType> transferEarliest;
  pair<timeType, timeType> transferLatest;
  for (int k = 0; k < solution->numRoutes; ++k) {
    transfers[k].clear();
    rp = solution->routes[k];
    for (itT = rp->first_transfer(); itT != rp->last_transfer(); ++itT) {
      transferEarliest = rp->earliestInfoTrans(itT->first);
      transferLatest = rp->latestInfoTrans(itT->first);
      transfers[k][itT->first] =
          make_tuple(transferEarliest.first, transferLatest.first,
                     transferEarliest.second, transferLatest.second);
    }
  }

  // computes the change in objective for each (request,route) insertion
  for (int i = 0; i < n; ++i) {
    p = requests->at(i)->pickup;
    d = requests->at(i)->delivery;
    // current earliest and latest times for customers in current solution
    customers[p].service = solution->customers[p].service;
    customers[d].service = solution->customers[d].service;

    if (insertionTracking[i] == false)  // requests not yet inserted
    {
      customers[p].service.set(0, 0);
      customers[d].service.set(0, 0);

      toInsert++;
      // compute (best) insertion with transfer
      while (!insertions_pq.empty())  // insertions_pq should be empty!
        insertions_pq.pop();
      for (int k = 0; k < solution->numRoutes; ++k) {
        rp = solution->routes[k];
        // iterate transfers visited by rp
        for (it = rp->first_transfer(); it != rp->last_transfer(); ++it) {
          t = it->first;
          feasible = rp->evaluateInsertWithTransfer(p, t, -1, -1, nullptr, -1,
                                                    insertions_pq);
        }
      }
      if (!insertions_pq
               .empty())  // its feasible, //best insertion over all routes
      {
        f_changeTR[i].cost = insertions_pq.top().cost;
        f_changeTR[i].route1 = insertions_pq.top().route1;
        f_changeTR[i].route2 = insertions_pq.top().route2;
        f_changeTR[i].transfer = insertions_pq.top().transfer;
        f_changeTR[i].before1 = insertions_pq.top().before1;
        f_changeTR[i].after1 = insertions_pq.top().after1;
        f_changeTR[i].before2 = insertions_pq.top().before2;
        f_changeTR[i].after2 = insertions_pq.top().after2;

        assert(f_changeTR[i].route1 != nullptr and
               f_changeTR[i].route2 != nullptr);
      } else {
        f_changeTR[i].cost = INF_DISTANCE;
        f_changeTR[i].route1 = nullptr;
        f_changeTR[i].route2 = nullptr;
      }

      // compute (best) insertion without transfer
      for (int k = 0; k < solution->numRoutes; ++k) {
        rp = solution->routes[k];
        // insertions_pq should be empty!
        while (!insertions_pq.empty()) insertions_pq.pop();
        rp->evaluateReqInsertion(
            p, d, insertions_pq);    // TODO: check insertion with transfers
        if (!insertions_pq.empty())  // its feasible
        {
          cost = insertions_pq.top().cost;  // best insertion for this route
          f_change[k][i] = cost;
        } else
          f_change[k][i] = INF_DISTANCE;  // not feasible on this route
      }
    }  // if
  }    // for

  distanceType max_difference, withTr, withoutTr, min_insertion;
  int best_request, best_route, firstRoute, secondRoute;
  bool stop = false;
  while (!stop) {
    // compute maximum difference
    max_difference = -INF_DISTANCE;
    best_request = -1;  // if -1, no feasible request found
    best_route = -1;

    // look for the best insertion, if any
    for (int i = 0; i < n; ++i) {
      if (insertionTracking[i] == false) {
        // get the best insertion (route) for request i
        min_insertion = INF_DISTANCE;
        firstRoute = -1;  // if -1, insertion using transfer
        for (int k = 0; k < solution->numRoutes; ++k) {
          if (min_insertion > f_change[k][i]) {
            min_insertion = f_change[k][i];
            firstRoute = k;
          }
        }
        // the best insertion cost, using transfer, for i is in
        // f_changeTR[i].cost
        if (min_insertion < INF_DISTANCE and
            f_changeTR[i].cost <
                INF_DISTANCE)  // feasible with and wihtout transfer
        {
          cost = min_insertion - f_changeTR[i].cost;
          firstRoute = -1;
        } else {
          if (min_insertion >= INF_DISTANCE and
              f_changeTR[i].cost >= INF_DISTANCE)  // infeasible request
            cost = -INF_DISTANCE;
          else if (min_insertion >=
                   INF_DISTANCE)  // infeasible without transfer
          {
            cost = -f_changeTR[i].cost;
            firstRoute = -1;
          } else if (f_changeTR[i].cost >=
                     INF_DISTANCE)  // infeasible with transfer
          {
            cost = -INF_DISTANCE;  // do not consider insertions without
                                   // transfer
            //  cost = -min_insertion;
            // firstRoute = firstRoute;
          }
        }
        if (max_difference < cost) {
          max_difference = cost;
          best_route = firstRoute;
          best_request = i;
        }
      }  // if
    }    // for

    stop = true;
    if (best_request != -1)  // a feasible (insertion) request was found
    {
      stop = false;
      p = requests->at(best_request)->pickup;
      d = requests->at(best_request)->delivery;

      rp = f_changeTR[best_request].route1;
      rd = f_changeTR[best_request].route2;
      t = f_changeTR[best_request].transfer;

      assert(rp != nullptr and rd != nullptr);  // DEBUG
      assert(best_route == -1);

      rp->addTrsDrop(rd, t, *(requests->at(best_request)));
      rd->addTrsPick(rp, t, *(requests->at(best_request)));
      rp->insertCustomer(p, f_changeTR[best_request].before1,
                         f_changeTR[best_request].after1);
      rp->updateEarliest(p);
      rp->updateLatest(p);
      rd->insertCustomer(d, f_changeTR[best_request].before2,
                         f_changeTR[best_request].after2);
      rd->updateEarliest(d);
      rd->updateLatest(d);

      toInsert--;
      insertionTracking[best_request] = true;

      assert(rp->isFeasible());  // DEBUG
      assert(rd->isFeasible());  // DEBUG

      //-----------------------------------------
      routesToUpdate.clear();
      routesToUpdate.insert(rp);
      routesToUpdate.insert(rd);

      // if a customer has its service windows changed, the correspondent route
      // need to be updated
      for (int i = 0; i < n; ++i) {
        if (insertionTracking[i] == true) {
          p = requests->at(i)->pickup;
          d = requests->at(i)->delivery;
          if (!(customers[p].service == solution->customers[p].service)) {
            routesToUpdate.insert(solution->assignment[p]);
            customers[p].service = solution->customers[p].service;
          }
          if (!(customers[d].service == solution->customers[d].service)) {
            routesToUpdate.insert(solution->assignment[d]);
            customers[d].service = solution->customers[d].service;
          }
        }
      }

      // WARNING: it can happen that customers do not change, but transfers do!
      bool save_time;
      for (int k = 0; k < solution->numRoutes; ++k) {
        rp = solution->routes[k];
        for (itT = rp->first_transfer(); itT != rp->last_transfer(); ++itT) {
          save_time = false;
          transferEarliest = rp->earliestInfoTrans(itT->first);
          transferLatest = rp->latestInfoTrans(itT->first);
          if (transferEarliest.first != get<0>(transfers[k][itT->first])) {
            get<0>(transfers[k][itT->first]) = transferEarliest.first;
            save_time = true;
          }
          if (transferLatest.first != get<1>(transfers[k][itT->first])) {
            get<1>(transfers[k][itT->first]) = transferLatest.first;
            save_time = true;
          }
          if (transferEarliest.second != get<2>(transfers[k][itT->first])) {
            get<2>(transfers[k][itT->first]) = transferEarliest.second;
            save_time = true;
          }
          if (transferLatest.second != get<3>(transfers[k][itT->first])) {
            get<3>(transfers[k][itT->first]) = transferLatest.second;
            save_time = true;
          }

          if (save_time)
            if (routesToUpdate.count(rp) == 0) routesToUpdate.insert(rp);
        }
      }

      // update the selected routes (insertions costs are changed with the new
      // route) and any other modified route due to the insertion
      for (auto& route : routesToUpdate) {
        best_route = -1;
        for (int k = 0; k < solution->numRoutes; ++k)
          if (route == solution->routes[k]) {
            best_route = k;
            break;
          }
        assert(best_route != -1);  // DEBUG

        // f_i_k (insertion without transfer)
        for (int i = 0; i < n; ++i) {
          if (insertionTracking[i] == false) {
            p = requests->at(i)->pickup;
            d = requests->at(i)->delivery;

            // insertions_pq should be empty!
            while (!insertions_pq.empty()) insertions_pq.pop();
            route->evaluateReqInsertion(p, d, insertions_pq);
            if (!insertions_pq.empty())  // its feasible
            {
              cost = insertions_pq.top().cost;  // best insertion for this route
              f_change[best_route][i] = cost;
            } else
              f_change[best_route][i] =
                  INF_DISTANCE;  // not feasible on this route
          }                      // if
        }                        // for
      }                          // for

      // f_i (insertion with transfer)
      for (int i = 0; i < n; ++i) {
        // TODO: check whether an infeasible insertion can become feasible!
        if (insertionTracking[i] == false and
            f_changeTR[i].cost < INF_DISTANCE) {
          p = requests->at(i)->pickup;
          d = requests->at(i)->delivery;

          rp = f_changeTR[i].route1;
          rd = f_changeTR[i].route2;
          assert(rp != nullptr and rd != nullptr);

          e1 = routesToUpdate.find(rp);
          e2 = routesToUpdate.find(rd);

          // one (or both) of the two routes in --(i+)---t---(i-)-- is modified
          if (e1 != routesToUpdate.end() or e2 != routesToUpdate.end()) {
            // recompute best insertion with transfer
            while (!insertions_pq.empty())  // insertions_pq should be empty!
              insertions_pq.pop();
            for (int k = 0; k < solution->numRoutes; ++k) {
              rp = solution->routes[k];
              // iterate transfers visited by rp
              for (it = rp->first_transfer(); it != rp->last_transfer(); ++it) {
                t = it->first;
                feasible = rp->evaluateInsertWithTransfer(p, t, -1, -1, nullptr,
                                                          -1, insertions_pq);
              }
            }
            if (!insertions_pq.empty())  // its feasible, //best insertion over
                                         // all routes
            {
              f_changeTR[i].cost = insertions_pq.top().cost;
              f_changeTR[i].route1 = insertions_pq.top().route1;
              f_changeTR[i].route2 = insertions_pq.top().route2;
              f_changeTR[i].transfer = insertions_pq.top().transfer;
              f_changeTR[i].before1 = insertions_pq.top().before1;
              f_changeTR[i].after1 = insertions_pq.top().after1;
              f_changeTR[i].before2 = insertions_pq.top().before2;
              f_changeTR[i].after2 = insertions_pq.top().after2;

              assert(f_changeTR[i].route1 != nullptr and
                     f_changeTR[i].route2 != nullptr);
            } else  // infeasible with transfer...
            {
              f_changeTR[i].cost = INF_DISTANCE;
              f_changeTR[i].route1 = nullptr;
              f_changeTR[i].route2 = nullptr;
            }
          }
        }
      }  // for
    }    // if

  }  // while

  return toInsert;
}

// select requests based on the cost to insert with transfer
// after an insertion, the insertion cost for reqs having one of the two
// used routes in the best insertion with transfer
int InsertBasicTrs::greedyBasic(Solution* solution) {
  // ALERT("greedyBasic with transfer\n");
  int n = requests->size();
  Route* rp;
  Route* rd;
  int p, d, t, r;
  distanceType cost;
  int toInsert = 0;
  transferIterator it;
  routesIterator rIT;
  bool feasible;
  set<Route*>::iterator e1;
  set<Route*>::iterator e2;

  // save transfer visit times for the solution
  transferIterator itT;
  pair<timeType, timeType> transferEarliest;
  pair<timeType, timeType> transferLatest;
  for (int k = 0; k < solution->numRoutes; ++k) {
    transfers[k].clear();
    rp = solution->routes[k];
    for (itT = rp->first_transfer(); itT != rp->last_transfer(); ++itT) {
      transferEarliest = rp->earliestInfoTrans(itT->first);
      transferLatest = rp->latestInfoTrans(itT->first);
      transfers[k][itT->first] =
          make_tuple(transferEarliest.first, transferLatest.first,
                     transferEarliest.second, transferLatest.second);
    }
  }

  // computes the change in objective for each (request,route) insertion
  for (int i = 0; i < n; ++i) {
    p = requests->at(i)->pickup;
    d = requests->at(i)->delivery;
    // current earliest and latest times for customers in current solution
    customers[p].service = solution->customers[p].service;
    customers[d].service = solution->customers[d].service;

    if (insertionTracking[i] == false)  // requests not yet inserted
    {
      customers[p].service.set(0, 0);
      customers[d].service.set(0, 0);

      toInsert++;
      // compute (best) insertion with transfer
      while (!insertions_pq.empty())  // insertions_pq should be empty!
        insertions_pq.pop();
      for (int k = 0; k < solution->numRoutes; ++k) {
        rp = solution->routes[k];
        // iterate transfers visited by rp
        for (it = rp->first_transfer(); it != rp->last_transfer(); ++it) {
          t = it->first;
          feasible = rp->evaluateInsertWithTransfer(p, t, -1, -1, nullptr, -1,
                                                    insertions_pq);
        }
      }
      if (!insertions_pq
               .empty())  // its feasible, //best insertion over all routes
      {
        f_changeTR[i].cost = insertions_pq.top().cost;
        f_changeTR[i].route1 = insertions_pq.top().route1;
        f_changeTR[i].route2 = insertions_pq.top().route2;
        f_changeTR[i].transfer = insertions_pq.top().transfer;
        f_changeTR[i].before1 = insertions_pq.top().before1;
        f_changeTR[i].after1 = insertions_pq.top().after1;
        f_changeTR[i].before2 = insertions_pq.top().before2;
        f_changeTR[i].after2 = insertions_pq.top().after2;

        assert(f_changeTR[i].route1 != nullptr and
               f_changeTR[i].route2 != nullptr);
      } else {
        f_changeTR[i].cost = INF_DISTANCE;
        f_changeTR[i].route1 = nullptr;
        f_changeTR[i].route2 = nullptr;
      }
    }  // if
  }    // for

  // WATCH(toInsert);

  distanceType min_insertion;
  int best_request;
  int firstRoute, secondRoute;
  bool stop = false;

  while (!stop) {
    min_insertion = INF_DISTANCE;
    best_request = -1;
    // look for the best insertion, if any
    for (int i = 0; i < n; ++i)
      if (insertionTracking[i] == false)
        if (min_insertion > f_changeTR[i].cost) {
          min_insertion = f_changeTR[i].cost;
          best_request = i;
        }

    stop = true;
    if (best_request != -1) {
      stop = false;
      p = requests->at(best_request)->pickup;
      d = requests->at(best_request)->delivery;

      rp = f_changeTR[best_request].route1;
      rd = f_changeTR[best_request].route2;
      t = f_changeTR[best_request].transfer;

      assert(rp != nullptr and rd != nullptr);  // DEBUG

      rp->addTrsDrop(rd, t, *(requests->at(best_request)));
      rd->addTrsPick(rp, t, *(requests->at(best_request)));
      rp->insertCustomer(p, f_changeTR[best_request].before1,
                         f_changeTR[best_request].after1);
      rp->updateEarliest(p);
      rp->updateLatest(p);
      rd->insertCustomer(d, f_changeTR[best_request].before2,
                         f_changeTR[best_request].after2);
      rd->updateEarliest(d);
      rd->updateLatest(d);

      toInsert--;
      insertionTracking[best_request] = true;

      assert(rp->isFeasible());  // DEBUG
      assert(rd->isFeasible());  // DEBUG

      //-----------------------------------------
      routesToUpdate.clear();
      routesToUpdate.insert(rp);
      routesToUpdate.insert(rd);

      // if a customer has its service windows changed, the correspondent route
      // need to be updated
      for (int i = 0; i < n; ++i) {
        if (insertionTracking[i] == true) {
          p = requests->at(i)->pickup;
          d = requests->at(i)->delivery;
          if (!(customers[p].service == solution->customers[p].service)) {
            routesToUpdate.insert(solution->assignment[p]);
            customers[p].service = solution->customers[p].service;
          }
          if (!(customers[d].service == solution->customers[d].service)) {
            routesToUpdate.insert(solution->assignment[d]);
            customers[d].service = solution->customers[d].service;
          }
        }
      }  // for

      // WARNING: it can happen that customers do not change, but transfers do!
      bool save_time;
      for (int k = 0; k < solution->numRoutes; ++k) {
        rp = solution->routes[k];
        for (itT = rp->first_transfer(); itT != rp->last_transfer(); ++itT) {
          save_time = false;
          transferEarliest = rp->earliestInfoTrans(itT->first);
          transferLatest = rp->latestInfoTrans(itT->first);
          if (transferEarliest.first != get<0>(transfers[k][itT->first])) {
            get<0>(transfers[k][itT->first]) = transferEarliest.first;
            save_time = true;
          }
          if (transferLatest.first != get<1>(transfers[k][itT->first])) {
            get<1>(transfers[k][itT->first]) = transferLatest.first;
            save_time = true;
          }
          if (transferEarliest.second != get<2>(transfers[k][itT->first])) {
            get<2>(transfers[k][itT->first]) = transferEarliest.second;
            save_time = true;
          }
          if (transferLatest.second != get<3>(transfers[k][itT->first])) {
            get<3>(transfers[k][itT->first]) = transferLatest.second;
            save_time = true;
          }

          if (save_time)
            if (routesToUpdate.count(rp) == 0) routesToUpdate.insert(rp);
        }
      }

      // update the selected routes (insertions costs are changed with the new
      // route) and any other modified route due to the insertion
      for (int i = 0; i < n; ++i) {
        // TODO: check whether an infeasible insertion can become feasible!
        if (insertionTracking[i] == false and
            f_changeTR[i].cost < INF_DISTANCE) {
          p = requests->at(i)->pickup;
          d = requests->at(i)->delivery;

          rp = f_changeTR[i].route1;
          rd = f_changeTR[i].route2;
          assert(rp != nullptr and rd != nullptr);

          e1 = routesToUpdate.find(rp);
          e2 = routesToUpdate.find(rd);

          // one (or both) of the two routes in --(i+)---t---(i-)-- is modified
          if (e1 != routesToUpdate.end() or e2 != routesToUpdate.end()) {
            // recompute best insertion with transfer
            while (!insertions_pq.empty())  // insertions_pq should be empty!
              insertions_pq.pop();
            for (int k = 0; k < solution->numRoutes; ++k) {
              rp = solution->routes[k];
              // iterate transfers visited by rp
              for (it = rp->first_transfer(); it != rp->last_transfer(); ++it) {
                t = it->first;
                feasible = rp->evaluateInsertWithTransfer(p, t, -1, -1, nullptr,
                                                          -1, insertions_pq);
              }
            }
            if (!insertions_pq.empty())  // its feasible, //best insertion over
                                         // all routes
            {
              f_changeTR[i].cost = insertions_pq.top().cost;
              f_changeTR[i].route1 = insertions_pq.top().route1;
              f_changeTR[i].route2 = insertions_pq.top().route2;
              f_changeTR[i].transfer = insertions_pq.top().transfer;
              f_changeTR[i].before1 = insertions_pq.top().before1;
              f_changeTR[i].after1 = insertions_pq.top().after1;
              f_changeTR[i].before2 = insertions_pq.top().before2;
              f_changeTR[i].after2 = insertions_pq.top().after2;

              assert(f_changeTR[i].route1 != nullptr and
                     f_changeTR[i].route2 != nullptr);
            } else  // infeasible with transfer...
            {
              f_changeTR[i].cost = INF_DISTANCE;
              f_changeTR[i].route1 = nullptr;
              f_changeTR[i].route2 = nullptr;
            }
          }
        }
      }  // for

    }  // if
  }    // while

  // WATCH(toInsert);
  return toInsert;
}

int InsertBasicTrs::execute(Solution* solution) {
  // controls information on the performance of the operator
  local_count++;

  int n = requests->size();
  Route* rp;
  Route* rd;
  int p, d, r;
  int toInsert;
  distanceType cost;

  assert(requests_order.empty() == true);  // DEBUG

  // intialize requests to be inserted
  toInsert = 0;
  for (int i = 0; i < n; ++i) {
    p = requests->at(i)->pickup;
    d = requests->at(i)->delivery;
    insertionTracking[i] = true;
    // entire request is outside the routing?
    if (solution->assignment[p] == nullptr and
        solution->assignment[d] == nullptr) {
      insertionTracking[i] = false;
      toInsert++;
    }
  }
  // WATCH(toInsert);
  inserted = toInsert;

  // the order in which requests are selected for insertion
  switch (selectionOrder) {
    case DISTANCE:  // NOTE: does not need to be reordered after an insertion,
                    // so order and insert
      // ALERT("Distance selection(T)\n");
      toInsert = greedyDistance(solution);
      break;
    case REGRET:
      // ALERT("Regret transfer selection(T))\n");
      toInsert = regretTransfer(solution);
      break;
    case GREEDY:
      // ALERT("Greedy selection(T)\n");
      toInsert = greedyBasic(solution);
      break;
      // case BEST_RND:
      //  ALERT("Random best selection\n");
      // break;
  }

  inserted -= toInsert;

  // ALERT("Done\n");

  // TODO: requests_order should be empty (emptied) at the end!
  if (toInsert > 0)  // there are still requests to be inserted
    return -1 * toInsert;
  return 0;
}

void InsertBasicTrs::updateScore(int reward) {
  assert(inserted >= 0);
  int multiplier = 2 * inserted;
  score += multiplier * reward;
}
