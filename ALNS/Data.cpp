#include "Data.h"

ostream& operator<<(std::ostream& out, problemData& data) {
  out << "requests :";
  for (auto& x : *(data.requests))
    cout << x.first << ":(" << x.second.pickup << "," << x.second.delivery
         << ") ";
  out << '\n';

  out << "depots: ";
  for (auto& x : *(data.depots)) out << x << " ";
  out << endl;

  out << "transfers: ";
  for (auto& x : *(data.transfers)) out << x << " ";
  out << endl;

  out << "locations :";
  for (auto& x : *(data.locations))
    out << x.first << ": [" << x.second.timeWindows.getStart() << ","
        << x.second.timeWindows.getEnd() << "] "
        << "at (" << x.second.coord_x << "," << x.second.coord_y << ") ";
  out << '\n';

  return out;
}

bool operator<(const insertionData& x, const insertionData& y) {
  return tie(x.cost, x.before1) >
         tie(y.cost, y.before1);  //> for increase order
}
