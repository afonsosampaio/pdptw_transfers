#ifndef CLUSTER_H
#define CLUSTER_H

#include <iostream>  //TODO: not needed!
#include <queue>
#include <set>
#include "Operator_base.h"
#include "Roulette.h"
#include "Route.h"

using namespace std;

typedef pair<int, int> pairType;

class RemoveCluster : public ALNS_Operator {
 public:
  RemoveCluster(string, vector<requestData*>*, Metric*, default_random_engine*,
                uniform_real_distribution<double>*, double);
  int execute(Solution*);
  void updateScore(int);

 private:
  vector<requestData*>* requests;
  priority_queue<pair<distanceType, pairType> > pickups_order;
  priority_queue<pair<distanceType, pairType> > delivery_order;
  map<int, vector<int>*> pickClusters;
  map<int, vector<int>*> delivClusters;

  Metric* metrics;
  int clustering[MAX_CLUSTERS];
  distanceType clust_diameters[MAX_CLUSTERS];
  set<Route*> routes_cluster;
};

#endif
