#include "insertion_Basic.h"

InsertBasic::InsertBasic(string nm, vector<requestData*>* reqs, Metric* met,
                         int sel, default_random_engine* gen,
                         uniform_real_distribution<double>* dist, double r)
    : ALNS_Operator(nm, gen, dist, r) {
  requests = reqs;
  metricMatrix = met;
  selectionOrder = sel;
  insertionTracking.reserve(512);  // FIXME
  bestValues.reserve(512);  // FIXME (should have been the maxmiun possible
                            // number of feasible insertions...)
  int n = 2 * requests->size();
  for (int i = 0; i <= n; i++) customers.push_back(nodeInfo());
}

// select requests based on the distance between p and d
// once ordered, insertion does not affect the order
int InsertBasic::greedyDistance(Solution* solution) {
  // ALERT("greedyDistance\n");
  int n = requests->size();
  Route* rp;
  Route* rd;
  int p, d, r;
  distanceType cost;
  int toInsert = 0;

  for (int i = 0; i < n; ++i) {
    p = requests->at(i)->pickup;
    d = requests->at(i)->delivery;
    if (insertionTracking[i] == false) {
      cost = metricMatrix->distance(p, d);
      requests_order.push(make_pair(cost, i));
      toInsert++;
    }
  }

  while (!requests_order.empty()) {
    r = requests_order.top().second;
    p = requests->at(r)->pickup;
    d = requests->at(r)->delivery;

    // insertions_pq should be empty!
    while (!insertions_pq.empty()) insertions_pq.pop();

    // ALERT("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n");

    for (int i = 0; i < solution->numRoutes; ++i) {
      rp = solution->routes[i];
      rp->evaluateReqInsertion(p, d, insertions_pq);
    }

    // best insertion over all routes
    if (!insertions_pq.empty())  // its feasible
    {
      rp = insertions_pq.top().route1;
      rp->insertRequest(p, d, insertions_pq.top());
      insertionTracking[r] = true;
      toInsert--;
      // if(!rp->isFeasible())
      // {
      //   ALERT("Infeasible\n");
      //   rp->show(1);
      //   rp->showDepartures();
      //   rp->showArrivals();
      // }
      assert(rp->isFeasible());
    }
    // else
    //   cout<<"Insertion for "<<p<<","<<d<<" is infeasible...\n";
    requests_order.pop();
  }
  // WATCH(toInsert);
  return toInsert;
}

// select requests based on the cost to insert in a route
// after an insertion, the insertion cost of all reqs on the route should be
// recalculated
int InsertBasic::greedyBasic(Solution* solution) {
  // ALERT("greedyBasic\n");
  int n = requests->size();
  Route* rp;
  Route* rd;
  int p, d, r;
  distanceType cost;
  int toInsert = 0;

  // save transfer visit times for the solution
  transferIterator itT;
  pair<timeType, timeType> transferEarliest;
  pair<timeType, timeType> transferLatest;
  for (int k = 0; k < solution->numRoutes; ++k) {
    transfers[k].clear();
    rp = solution->routes[k];
    for (itT = rp->first_transfer(); itT != rp->last_transfer(); ++itT) {
      transferEarliest = rp->earliestInfoTrans(itT->first);
      transferLatest = rp->latestInfoTrans(itT->first);
      transfers[k][itT->first] =
          make_tuple(transferEarliest.first, transferLatest.first,
                     transferEarliest.second, transferLatest.second);
    }
  }

  for (int i = 0; i < n; ++i) {
    p = requests->at(i)->pickup;
    d = requests->at(i)->delivery;
    // current earliest and latest times for customers in current solution
    customers[p].service = solution->customers[p].service;
    customers[d].service = solution->customers[d].service;

    if (insertionTracking[i] == false)  // requests not yet inserted
    {
      toInsert++;
      // p = requests->at(i)->pickup;
      // d = requests->at(i)->delivery;

      customers[p].service.set(0, 0);
      customers[d].service.set(0, 0);

      // compute all insertions intitialy
      for (int k = 0; k < solution->numRoutes; ++k) {
        rp = solution->routes[k];
        // insertions_pq should be empty!
        while (!insertions_pq.empty()) insertions_pq.pop();
        rp->evaluateReqInsertion(p, d, insertions_pq);
        if (!insertions_pq.empty())  // its feasible
        {
          cost = insertions_pq.top().cost;  // best insertion for this route
          f_change[k][i] = cost;
        } else
          f_change[k][i] = INF_DISTANCE;  // not feasible on this route
      }
    }
  }

  distanceType min_insertion;
  int best_route;
  int best_request;
  bool stop = false;

  while (!stop) {
    min_insertion = INF_DISTANCE;
    best_route = -1;
    best_request = -1;
    // look for the best insertion, if any
    for (int i = 0; i < n; ++i)
      if (insertionTracking[i] == false)
        for (int k = 0; k < solution->numRoutes; ++k) {
          if (min_insertion > f_change[k][i]) {
            min_insertion = f_change[k][i];
            best_route = k;
            best_request = i;
          }
        }
    stop = true;
    if (best_route != -1)  // a feasible request, route pair was found
    {
      stop = false;
      p = requests->at(best_request)->pickup;
      d = requests->at(best_request)->delivery;
      rp = solution->routes[best_route];
      // bad design that we have to compute the (best) insertion for the request
      // on the route again...
      while (!insertions_pq.empty()) insertions_pq.pop();
      rp->evaluateReqInsertion(p, d, insertions_pq);
      if (!insertions_pq.empty())  // its feasible (should be!)
      {
        cost = insertions_pq.top().cost;
        rp->insertRequest(p, d, insertions_pq.top());
        insertionTracking[best_request] = true;
        toInsert--;
        assert(rp->isFeasible());
      } else
        assert(!insertions_pq.empty());  // should not be case...

      //------------------------------------
      routesToUpdate.clear();
      routesToUpdate.insert(rp);

      // if a customer has its service windows changed, the correspondent route
      // need to be updated
      for (int i = 0; i < n; ++i) {
        if (insertionTracking[i] == true) {
          p = requests->at(i)->pickup;
          d = requests->at(i)->delivery;
          if (!(customers[p].service == solution->customers[p].service)) {
            routesToUpdate.insert(solution->assignment[p]);
            customers[p].service = solution->customers[p].service;
          }
          if (!(customers[d].service == solution->customers[d].service)) {
            routesToUpdate.insert(solution->assignment[d]);
            customers[d].service = solution->customers[d].service;
          }
        }
      }

      // WARNING: it can happen that customers do not change, but transfers do!
      bool save_time;
      for (int k = 0; k < solution->numRoutes; ++k) {
        rp = solution->routes[k];
        for (itT = rp->first_transfer(); itT != rp->last_transfer(); ++itT) {
          save_time = false;
          transferEarliest = rp->earliestInfoTrans(itT->first);
          transferLatest = rp->latestInfoTrans(itT->first);
          if (transferEarliest.first != get<0>(transfers[k][itT->first])) {
            get<0>(transfers[k][itT->first]) = transferEarliest.first;
            save_time = true;
          }
          if (transferLatest.first != get<1>(transfers[k][itT->first])) {
            get<1>(transfers[k][itT->first]) = transferLatest.first;
            save_time = true;
          }
          if (transferEarliest.second != get<2>(transfers[k][itT->first])) {
            get<2>(transfers[k][itT->first]) = transferEarliest.second;
            save_time = true;
          }
          if (transferLatest.second != get<3>(transfers[k][itT->first])) {
            get<3>(transfers[k][itT->first]) = transferLatest.second;
            save_time = true;
          }

          if (save_time)
            if (routesToUpdate.count(rp) == 0) routesToUpdate.insert(rp);
        }
      }

      // update the selected route (insertions costs are changed with the new
      // route) and any other modified route due to the insertion
      for (auto& route : routesToUpdate) {
        best_route = -1;
        for (int k = 0; k < solution->numRoutes; ++k)
          if (route == solution->routes[k]) {
            best_route = k;
            break;
          }
        assert(best_route != -1);  // DEBUG

        for (int i = 0; i < n; ++i) {
          if (insertionTracking[i] == false) {
            p = requests->at(i)->pickup;
            d = requests->at(i)->delivery;

            // insertions_pq should be empty!
            while (!insertions_pq.empty()) insertions_pq.pop();
            route->evaluateReqInsertion(p, d, insertions_pq);
            if (!insertions_pq.empty())  // its feasible
            {
              cost = insertions_pq.top().cost;  // best insertion for this route
              f_change[best_route][i] = cost;
            } else
              f_change[best_route][i] =
                  INF_DISTANCE;  // not feasible on this route
          }                      // if
        }                        // for
      }                          // for
    }                            // if(best_route != -1)
  }                              // while

  return toInsert;
}

// select requests accordingly with the difference between best and second best
// insertions if second best insertion is not defined, its value is 0 after an
// insertion, the insertion cost of all reqs on the route should be recalculated
int InsertBasic::regret2(Solution* solution) {
  //   ALERT("REGRET-2 insertion\n");
  int n = requests->size();
  Route* rp;
  Route* rd;
  int p, d, r;
  distanceType cost;
  int toInsert = 0;

  // save transfer visit times for the solution
  transferIterator itT;
  pair<timeType, timeType> transferEarliest;
  pair<timeType, timeType> transferLatest;
  for (int k = 0; k < solution->numRoutes; ++k) {
    transfers[k].clear();
    rp = solution->routes[k];
    for (itT = rp->first_transfer(); itT != rp->last_transfer(); ++itT) {
      transferEarliest = rp->earliestInfoTrans(itT->first);
      transferLatest = rp->latestInfoTrans(itT->first);
      transfers[k][itT->first] =
          make_tuple(transferEarliest.first, transferLatest.first,
                     transferEarliest.second, transferLatest.second);
    }
  }
  // for(int k=0; k<solution->numRoutes; ++k)
  //   solution->routes[k]->show(1);
  //
  // for(int k=0; k<solution->numRoutes; ++k)
  // {
  //   rp = solution->routes[k];
  //   for(itT=rp->first_transfer(); itT!=rp->last_transfer(); ++itT)
  //     cout<<itT->first<<" "<<get<0>(transfers[k][itT->first])<<"
  //     "<<get<1>(transfers[k][itT->first])<<" "<<
  //                           get<2>(transfers[k][itT->first])<<"
  //                           "<<get<3>(transfers[k][itT->first])<<endl;
  // }

  // computes the change in objective for each insertion
  for (int i = 0; i < n; ++i) {
    p = requests->at(i)->pickup;
    d = requests->at(i)->delivery;
    // current earliest and latest times for customers in current solution
    customers[p].service = solution->customers[p].service;
    customers[d].service = solution->customers[d].service;

    if (insertionTracking[i] == false)  // requests not yet inserted
    {
      toInsert++;

      customers[p].service.set(0, 0);
      customers[d].service.set(0, 0);

      // compute all insertions intitialy
      for (int k = 0; k < solution->numRoutes; ++k) {
        rp = solution->routes[k];
        // insertions_pq should be empty!
        while (!insertions_pq.empty()) insertions_pq.pop();
        rp->evaluateReqInsertion(
            p, d, insertions_pq);    // TODO: check insertion with transfers
        if (!insertions_pq.empty())  // its feasible
        {
          cost = insertions_pq.top().cost;  // best insertion for this route
          f_change[k][i] = cost;
        } else
          f_change[k][i] = INF_DISTANCE;  // not feasible on this route
      }
    }
  }

  distanceType max_difference, firstBest, secondBest;
  int best_route, firstRoute, secondRoute;
  int best_request;
  bool stop = false;

  while (!stop) {
    max_difference = -INF_DISTANCE;
    best_route = -1;
    best_request = -1;

    // look for the maximum diference between the best and second best
    // insertions
    for (int i = 0; i < n; ++i) {
      if (insertionTracking[i] == false)  // not yet inserted
      {
        firstBest = secondBest = INF_DISTANCE;
        firstRoute = secondRoute = -1;
        for (int k = 0; k < solution->numRoutes;
             ++k) {  // finds the best and second best insertions for request i
          if (f_change[k][i] < firstBest) {
            secondBest = firstBest;
            firstBest = f_change[k][i];
            secondRoute = firstRoute;
            firstRoute = k;
          }
          if (f_change[k][i] > firstBest and f_change[k][i] < secondBest) {
            secondBest = f_change[k][i];
            secondRoute = k;
          }
        }

        assert(firstBest <= secondBest);  // DEBUG

        if (secondRoute != -1)  // second best insertion is defined
          cost = secondBest - firstBest;
        else
          cost = -firstBest;  // INF_DISTANCE

        if (firstRoute == -1)  // infeasible insertion
          cost = -INF_DISTANCE;

        if (max_difference < cost) {
          max_difference = cost;
          best_route = firstRoute;
          best_request = i;
        }
      }
    }  // for

    stop = true;
    if (best_route != -1)  // a feasible (request,route) pair was found
    {
      stop = false;
      p = requests->at(best_request)->pickup;
      d = requests->at(best_request)->delivery;
      rp = solution->routes[best_route];

      // bad design that we have to compute the (best) insertion for the request
      // on the route again...
      while (!insertions_pq.empty()) insertions_pq.pop();
      rp->evaluateReqInsertion(p, d, insertions_pq);
      if (!insertions_pq.empty())  // its feasible (should be!)
      {
        cost = insertions_pq.top().cost;
        rp->insertRequest(p, d, insertions_pq.top());
        insertionTracking[best_request] = true;
        toInsert--;
        assert(rp->isFeasible());  // DEBUG
      } else
        assert(!insertions_pq.empty());

      //------------------------------------
      routesToUpdate.clear();
      routesToUpdate.insert(rp);

      // if a customer has its service windows changed, the correspondent route
      // need to be updated
      for (int i = 0; i < n; ++i) {
        if (insertionTracking[i] == true) {
          p = requests->at(i)->pickup;
          d = requests->at(i)->delivery;
          if (!(customers[p].service == solution->customers[p].service)) {
            routesToUpdate.insert(solution->assignment[p]);
            customers[p].service = solution->customers[p].service;
          }
          if (!(customers[d].service == solution->customers[d].service)) {
            routesToUpdate.insert(solution->assignment[d]);
            customers[d].service = solution->customers[d].service;
          }
        }
      }

      // WARNING: it can happen that customers do not change, but transfers do!
      bool save_time;
      for (int k = 0; k < solution->numRoutes; ++k) {
        rp = solution->routes[k];
        for (itT = rp->first_transfer(); itT != rp->last_transfer(); ++itT) {
          save_time = false;
          transferEarliest = rp->earliestInfoTrans(itT->first);
          transferLatest = rp->latestInfoTrans(itT->first);
          if (transferEarliest.first != get<0>(transfers[k][itT->first])) {
            get<0>(transfers[k][itT->first]) = transferEarliest.first;
            save_time = true;
          }
          if (transferLatest.first != get<1>(transfers[k][itT->first])) {
            get<1>(transfers[k][itT->first]) = transferLatest.first;
            save_time = true;
          }
          if (transferEarliest.second != get<2>(transfers[k][itT->first])) {
            get<2>(transfers[k][itT->first]) = transferEarliest.second;
            save_time = true;
          }
          if (transferLatest.second != get<3>(transfers[k][itT->first])) {
            get<3>(transfers[k][itT->first]) = transferLatest.second;
            save_time = true;
          }

          if (save_time)
            if (routesToUpdate.count(rp) == 0) routesToUpdate.insert(rp);
        }
      }

      // update the selected route (insertions costs are changed with the new
      // route) and any other modified route due to the insertion
      for (auto& route : routesToUpdate) {
        best_route = -1;
        for (int k = 0; k < solution->numRoutes; ++k)
          if (route == solution->routes[k]) {
            best_route = k;
            break;
          }
        assert(best_route != -1);  // DEBUG

        for (int i = 0; i < n; ++i) {
          if (insertionTracking[i] == false) {
            p = requests->at(i)->pickup;
            d = requests->at(i)->delivery;

            // insertions_pq should be empty!
            while (!insertions_pq.empty()) insertions_pq.pop();
            route->evaluateReqInsertion(p, d, insertions_pq);
            if (!insertions_pq.empty())  // its feasible
              f_change[best_route][i] = insertions_pq.top().cost;
            else
              f_change[best_route][i] =
                  INF_DISTANCE;  // not feasible on this route
          }                      // if
        }                        // for
      }                          // for
    }                            // if(best_route != -1)
  }                              // while

  return toInsert;
}

int InsertBasic::rndBest(Solution* solution) {
  // ALERT("bestrandom insertion\n");
  int n = requests->size();
  Route* rp;
  Route* rd;
  int p, d, r;
  distanceType cost;
  int toInsert = 0;
  int totalFeasInsertions;

  // save transfer visit times for the solution
  transferIterator itT;
  pair<timeType, timeType> transferEarliest;
  pair<timeType, timeType> transferLatest;
  for (int k = 0; k < solution->numRoutes; ++k) {
    transfers[k].clear();
    rp = solution->routes[k];
    for (itT = rp->first_transfer(); itT != rp->last_transfer(); ++itT) {
      transferEarliest = rp->earliestInfoTrans(itT->first);
      transferLatest = rp->latestInfoTrans(itT->first);
      transfers[k][itT->first] =
          make_tuple(transferEarliest.first, transferLatest.first,
                     transferEarliest.second, transferLatest.second);
    }
  }

  // computes the change in objective for each insertion
  totalFeasInsertions = 0;
  for (int i = 0; i < n; ++i) {
    p = requests->at(i)->pickup;
    d = requests->at(i)->delivery;

    // current earliest and latest times for customers in current solution
    customers[p].service = solution->customers[p].service;
    customers[d].service = solution->customers[d].service;

    if (insertionTracking[i] == false)  // requests not yet inserted
    {
      toInsert++;
      customers[p].service.set(0, 0);
      customers[d].service.set(0, 0);

      // compute all insertions intitialy
      for (int k = 0; k < solution->numRoutes; ++k) {
        rp = solution->routes[k];
        // insertions_pq should be empty!
        while (!insertions_pq.empty()) insertions_pq.pop();
        rp->evaluateReqInsertion(
            p, d, insertions_pq);    // TODO: check insertion with transfers
        if (!insertions_pq.empty())  // its feasible
        {
          cost = insertions_pq.top().cost;  // best insertion for this route
          f_change[k][i] = cost;
          totalFeasInsertions++;  // not needed now!
        } else
          f_change[k][i] = INF_DISTANCE;  // not feasible on this route
      }
    }
  }

  distanceType min_insertion;
  int best_route;
  int best_request;
  bool stop = false;

  // WATCH(totalFeasInsertions); //DEBUG

  while (!stop) {
    totalFeasInsertions = 0;
    bestValues.clear();
    // look for the best insertion, if any, for each request
    for (int i = 0; i < n; ++i) {
      if (insertionTracking[i] == false)
        for (int k = 0; k < solution->numRoutes; ++k) {
          if (f_change[k][i] < INF_DISTANCE)  // a feasible insertion
          {
            totalFeasInsertions++;
            bestValues.push_back(f_change[k][i]);
          }
        }
    }

    int limit = ceil(totalFeasInsertions * 0.3);  // at least x% of the requests
    double rand_nun = (*uni_dist)(*rand_eng);
    int q = (limit + 1) * rand_nun;

    // selects the q_th (0,1,2...q) best insertion
    int route_q = -1;
    int request_q = -1;
    bool aux_flag = false;

    if (totalFeasInsertions > 0)
      nth_element(bestValues.begin(), bestValues.begin() + q, bestValues.end());
    else
      aux_flag = true;  // no feasible insertion....

    for (int i = 0; i < n && !aux_flag; ++i) {
      if (insertionTracking[i] == false)  // only not inserted requests
        for (int k = 0; k < solution->numRoutes && !aux_flag; ++k) {
          if (f_change[k][i] == bestValues[q])  // a feasible insertion
          {
            route_q = k;
            request_q = i;
            aux_flag = true;
          }
        }
    }

    stop = true;
    if (request_q != -1)  // there exists a feasible insertion
    {
      stop = false;
      p = requests->at(request_q)->pickup;
      d = requests->at(request_q)->delivery;
      rp = solution->routes[route_q];
      // insertions_pq should be empty!
      while (!insertions_pq.empty()) insertions_pq.pop();
      rp->evaluateReqInsertion(p, d, insertions_pq);
      if (!insertions_pq.empty())  // its feasible  (there should be one!)
      {
        rp->insertRequest(p, d, insertions_pq.top());
        insertionTracking[request_q] = true;
        toInsert--;
        assert(rp->isFeasible());
      } else
        assert(!insertions_pq.empty());  // shold not be the case....

      //------------------------------------
      routesToUpdate.clear();
      routesToUpdate.insert(rp);

      // if a customer has its service windows changed, the correspondent route
      // need to be updated
      for (int i = 0; i < n; ++i) {
        if (insertionTracking[i] == true) {
          p = requests->at(i)->pickup;
          d = requests->at(i)->delivery;
          if (!(customers[p].service == solution->customers[p].service)) {
            routesToUpdate.insert(solution->assignment[p]);
            customers[p].service = solution->customers[p].service;
          }
          if (!(customers[d].service == solution->customers[d].service)) {
            routesToUpdate.insert(solution->assignment[d]);
            customers[d].service = solution->customers[d].service;
          }
        }
      }

      // WARNING: it can happen that customers do not change, but transfers do!
      bool save_time;
      for (int k = 0; k < solution->numRoutes; ++k) {
        rp = solution->routes[k];
        for (itT = rp->first_transfer(); itT != rp->last_transfer(); ++itT) {
          save_time = false;
          transferEarliest = rp->earliestInfoTrans(itT->first);
          transferLatest = rp->latestInfoTrans(itT->first);
          if (transferEarliest.first != get<0>(transfers[k][itT->first])) {
            get<0>(transfers[k][itT->first]) = transferEarliest.first;
            save_time = true;
          }
          if (transferLatest.first != get<1>(transfers[k][itT->first])) {
            get<1>(transfers[k][itT->first]) = transferLatest.first;
            save_time = true;
          }
          if (transferEarliest.second != get<2>(transfers[k][itT->first])) {
            get<2>(transfers[k][itT->first]) = transferEarliest.second;
            save_time = true;
          }
          if (transferLatest.second != get<3>(transfers[k][itT->first])) {
            get<3>(transfers[k][itT->first]) = transferLatest.second;
            save_time = true;
          }

          if (save_time)
            if (routesToUpdate.count(rp) == 0) routesToUpdate.insert(rp);
        }
      }

      // update the selected route (insertions costs are changed with the new
      // route) and any other modified route due to the insertion
      for (auto& route : routesToUpdate) {
        route_q = -1;
        for (int k = 0; k < solution->numRoutes; ++k)
          if (route == solution->routes[k]) {
            route_q = k;
            break;
          }
        assert(route_q != -1);  // DEBUG

        for (int i = 0; i < n; ++i) {
          if (insertionTracking[i] == false) {
            p = requests->at(i)->pickup;
            d = requests->at(i)->delivery;

            // insertions_pq should be empty!
            while (!insertions_pq.empty()) insertions_pq.pop();
            route->evaluateReqInsertion(p, d, insertions_pq);
            if (!insertions_pq.empty())  // its feasible
              f_change[route_q][i] =
                  insertions_pq.top().cost;  // best insertion for this route;
            else
              f_change[route_q][i] = INF_DISTANCE;  // not feasible on this
                                                    // route
          }  // if
        }    // for
      }      // for
    }        // if
  }          // while

  return toInsert;
}

int InsertBasic::execute(Solution* solution) {
  // controls information on the performance of the operator
  local_count++;

  int n = requests->size();
  Route* rp;
  Route* rd;
  int p, d, r;
  int toInsert;
  distanceType cost;

  assert(requests_order.empty() == true);  // DEBUG

  // intialize requests to be inserted
  toInsert = 0;
  for (int i = 0; i < n; ++i) {
    p = requests->at(i)->pickup;
    d = requests->at(i)->delivery;
    insertionTracking[i] = true;
    // entire request is outside the routing?
    if (solution->assignment[p] == nullptr and
        solution->assignment[d] == nullptr) {
      insertionTracking[i] = false;
      toInsert++;
      // NOTE: to be consistent, set customer.service(0) here
    }
  }

  inserted = toInsert;

  // the order in which requests are selected for insertion
  switch (selectionOrder) {
    case DISTANCE:  // NOTE: does not need to be reordered after an insertion,
                    // so order and insert
      // ALERT("Distance selection\n");
      toInsert = greedyDistance(solution);
      break;
    case GREEDY:
      // ALERT("Greedy selection\n");
      toInsert = greedyBasic(solution);
      break;
    case REGRET:
      // ALERT("Regret-2 insertion\n");
      toInsert = regret2(solution);
      break;
    case BEST_RND:
      // ALERT("best reandom insertion\n");
      toInsert = rndBest(solution);
      break;
  }

  inserted -= toInsert;

  // ALERT("Done\n");

  // TODO: requests_order should be empty (emptied) at the end!
  if (toInsert > 0)  // there are still requests to be inserted
    return -1 * toInsert;
  return 0;
}

void InsertBasic::updateScore(int reward) {
  assert(inserted >= 0);
  int multiplier = 1 * inserted;
  score += multiplier * reward;
}
