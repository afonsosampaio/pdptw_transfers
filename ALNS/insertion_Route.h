#ifndef INSERT_ROUTE_H
#define INSERT_ROUTE_H

#include "Data.h"
#include "Metrics.h"
#include "Operator_base.h"
#include "Route.h"
#include "Solution.h"

#include <queue>
#include <set>

using namespace std;

typedef pair<int, int> pairType;

class InsertNewRoute : public ALNS_Operator {
 public:
  InsertNewRoute(string, vector<requestData*>*, map<int, locationsData>*,
                 vector<int>*, vector<int>*, Metric*, default_random_engine*,
                 uniform_real_distribution<double>*, double);
  int execute(Solution*);
  void updateScore(int);

 private:
  Metric* metricMatrix;
  priority_queue<pair<distanceType, int> > requests_order;
  priority_queue<insertionData> insertions_pq;
  priority_queue<insertionData> insertions_dq;
  priority_queue<insertionData> insertions_pkp;
  vector<requestData*>* requests;
  vector<bool>
      insertionTracking;  // library implementation may optimize storage so that
                          // each value is stored in a single bit.

  // locations
  vector<int>* depots;
  vector<int>* transfers;
  map<int, locationsData>* locations;

  // clustering
  priority_queue<pair<distanceType, pairType> > pickups_order;
  priority_queue<pair<distanceType, pairType> > delivery_order;
  map<int, vector<int>*> pickClusters;
  map<int, vector<int>*> delivClusters;

  int clustering[MAX_CLUSTERS];
  distanceType clust_diameters[MAX_CLUSTERS];

  int insertWithoutTransfer(Solution*);
  int insertWithTransfer(Solution*);
};

#endif
