#ifndef SOLUTION_H
#define SOLUTION_H

#include <array>  //C++11
#include <vector>

// #include "Route.h"
#include "Data.h"

using namespace std;

/*
Keep the customers, but transfer visits are kept separate on each
route (Class Route)
*/
class Solution {
  friend class Route;

 private:
  int size;  // number of nodes
 public:
  vector<nodeInfo> customers;
  array<Route*, ROUTES_PER_SOLUTION> routes;  // routes in the solution
  vector<Route*> assignment;                  // route-node assignment
  vector<timeType> earliest;
  vector<timeType> latest;
  // vector< pair<int, int> > transfers;
  problemData* instance;
  double totalCost;
  int numRoutes;

  Solution(problemData*, int);
  void Show();
  void clear();
  void addRoute(Route*);
  void addRoute();
  void removeRoute(Route*);
  void removeRoute(int);
  int requestsTransferred();
  bool checkAssignment();
  void showNotIncluded();
  int getNotIncluded();
  friend bool operator==(Solution&, Solution&);
  Solution& operator=(Solution&);
};

#endif
