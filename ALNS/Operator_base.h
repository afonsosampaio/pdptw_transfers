#ifndef OPERATOR_H
#define OPERATOR_H

#include <random>  //C++11
#include "Solution.h"

#define MAX_SEGMENTS 150

class ALNS_Operator {
 protected:
  string name;
  // for use in the adptive adjustment
  double weight;
  int score;
  int global_count;
  int local_count;
  double rho;

  // for use in computing statistics
  int total_accepted;
  int total_improved;
  int total_incumbent;
  int accepted;
  int improved;
  int incumbent;
  vector<tuple<double, double, double, double> > charts;
  int i_store;
  int update_frequency, segments;
  int total_acc, total_imp, total_inc;

  // solution information
  Solution* solution;
  // aux to generate random number
  default_random_engine* rand_eng;
  uniform_real_distribution<double>* uni_dist;

 public:
  ALNS_Operator(string n, default_random_engine* gen,
                uniform_real_distribution<double>* dist, double r)
      : name(n),
        rand_eng(gen),
        uni_dist(dist),
        rho(r),
        score(0),
        global_count(0),
        local_count(0),
        total_accepted(0),
        accepted(0),
        total_improved(0),
        improved(0),
        total_incumbent(0),
        incumbent(0),
        i_store(0) {
    charts.reserve(MAX_SEGMENTS);
    for (int i = 0; i < MAX_SEGMENTS; ++i)
      charts.push_back(make_tuple(0.0, 0.0, 0.0, 0.0));
    update_frequency = 2;
    segments = 1;
    total_acc = total_imp = total_inc = 0;
  }
  double getWeight() { return weight; }
  void setWeight(double w) { weight = w; }
  void updatePerformance(int, int, int);
  void showKPIs();
  void statistics(bool, bool, bool);
  virtual void updateScore(int) = 0;
  virtual int execute(Solution*) = 0;
  friend ostream& operator<<(std::ostream&, ALNS_Operator&);
};

#endif
