#include "Route.h"

Route::Route(Solution* sol, Metric* m, int f, int l, int sd, int ed)
    : solution(sol),
      metricMatrix(m),
      first_visit(f),
      last_visit(l),
      start_depot(sd),
      end_depot(ed) {
  length = 0;
  int next = first_visit;
  while (next != -1) {
    length++;
    solution->assignment[next] = this;
    if (isTransferNode(next))
      next = transfer_visits[next].succ;
    else
      next = solution->customers[next].succ;
  }
};

void Route::construct() {
  int next = first_visit;
  while (next != -1) {
    length++;
    if (isTransferNode(next))
      next = transfer_visits[next].succ;
    else {
      solution->assignment[next] = this;
      next = solution->customers[next].succ;
    }
  }
}

void Route::connectTransfers() {
  int p, d;
  for (auto& t : transfer_visits) {
    // cout<<"At transfer "<<t.first<<endl; //DEBUG
    // ALERT("Arrival: ");
    // for(auto& route : t.second.arrival_routes)
    //   ALERT(route);
    // ALERT("\nDeparture: ");
    // for(auto& route : t.second.departure_routes)
    //   ALERT(route);
    // cout<<"\n";  //DEBUG
    // requests picked at t
    t.second.arrival_routes.clear();
    t.second.departure_routes.clear();
    for (int i = 0; i < t.second.pickups.size(); ++i) {
      p = t.second.pickups[i];
      t.second.arrival_routes.insert(solution->assignment[p]);
      // cout<<"Pickup "<<p<< "("<<solution->assignment[p]<<")/"; //DEBUG
    }
    // requests dropped at t
    for (int i = 0; i < t.second.drops.size(); ++i) {
      d = t.second.drops[i];
      t.second.departure_routes.insert(solution->assignment[d]);
      // cout<<"Delivery "<<d<< "("<<solution->assignment[d]<<")/"; //DEBUG
    }
    // cout<<endl; //DEBUG
  }
}

/// limit: maximum number of iterations for an 'empty' transfer
void Route::pruneTransfers(int limit) {
  int p, d;
  for (auto& t : transfer_visits) {
    // requests picked at t
    for (auto it = t.second.pickups.begin(); it != t.second.pickups.end();
         ++it) {
      p = *it;
      if (solution->assignment[p] == nullptr)
        t.second.pickups.erase(it--);  // it=erase(it), it--;
    }
    // requests dropped at t
    for (auto it = t.second.drops.begin(); it != t.second.drops.end(); ++it) {
      d = *it;
      if (solution->assignment[d] == nullptr)
        t.second.drops.erase(it--);  // it=erase(it), it--;
    }
    // check whether operations are still performed at t
    if (t.second.pickups.size() < 1 and t.second.drops.size() < 1)
      t.second.emptyIterations++;
    else
      t.second.emptyIterations = 0;
  }

  // eliminate not used transfers...
  int pred;
  int succ;
  int element;

  for (auto ite = transfer_visits.begin(); ite != transfer_visits.end();) {
    if (ite->second.emptyIterations > limit) {
      // ALERT("---------------------------------------------------\n");
      // show(1);
      // ALERT("Delete transfer: ");
      // cout<<ite->first<<endl;

      element = ite->first;
      pred = ite->second.pred;
      succ = ite->second.succ;

      if (pred != -1) {
        if (isTransferNode(pred))
          transfer_visits[pred].succ = succ;
        else
          solution->customers[pred].succ = succ;
      }
      if (succ != -1) {
        if (isTransferNode(succ))
          transfer_visits[succ].pred = pred;
        else
          solution->customers[succ].pred = pred;
      }

      if (first_visit == element) first_visit = succ;
      if (last_visit == element) last_visit = pred;

      // NOTE: timinng will be computed after all removals
      // flagRoute();
      // earliestTimes();
      // latestTimes();
      transfer_visits.erase(ite++);
      length--;
    } else
      ++ite;
  }
}

// just to reinitialize a route
void Route::reset() {
  first_visit = last_visit = start_depot = end_depot = -1;
  length = 0;
  route_cost = weight = 0.0;
  transfer_visits.clear();
}

// used when removing a route and its elements from a solution
void Route::clear() {
  int next = first_visit;
  while (next != -1) {
    if (isTransferNode(next))
      next = transfer_visits[next].succ;
    else {
      solution->assignment[next] = nullptr;
      next = solution->customers[next].succ;
    }
  }

  first_visit = last_visit = start_depot = end_depot = -1;
  length = 0;
  route_cost = weight = 0.0;
  transfer_visits.clear();
}

// a route is empty if it does not visit any customer (p or d)
// or only transfers
bool Route::isEmpty() {
  int next = first_visit;

  while (next != -1) {
    if (isTransferNode(next))
      next = transfer_visits[next].succ;
    else
      return false;
  }
  return true;
}

bool Route::visitsTransfer() { return (transfer_visits.size() > 0); }

bool Route::visitsTransfer(int t) { return (transfer_visits.count(t) != 0); }

int Route::fullReqsServiced() {
  int next = first_visit;
  int rel;
  int visits = 0;
  while (next != -1) {
    if (isTransferNode(next))
      next = transfer_visits[next].succ;
    else {
      rel = solution->customers[next].related;
      if (solution->assignment[rel] == this) visits++;

      next = solution->customers[next].succ;
    }
  }

  // visits is double!
  return visits / 2;
}

int Route::transReqsServiced() {
  int next = first_visit;
  int rel;
  int visits = 0;
  while (next != -1) {
    if (isTransferNode(next))
      next = transfer_visits[next].succ;
    else {
      rel = solution->customers[next].related;
      if (solution->assignment[rel] != this) visits++;

      next = solution->customers[next].succ;
    }
  }

  return visits;
}

void Route::insertRequest(int pick, int deliv, const insertionData& insertion) {
  int ip, jp;  // between i,j
  int id, jd;  // between i,j

  ip = insertion.before1;
  jp = insertion.after1;
  id = insertion.before2;
  jd = insertion.after2;

  if (ip != id and jp != jd) {
    insertCustomer(pick, ip, jp);
    updateLatest(pick);
    updateEarliest(pick);
    insertCustomer(deliv, id, jd);
    updateLatest(deliv);
    updateEarliest(deliv);  // TODO:since same route, just one update is needed
  } else {
    insertPair(pick, deliv, ip, jp);
    // cout<<pick<<" "<<deliv<<" immediately after the other \n";
  }
  getCost();
}

// prev p--d after, insert on the same route
void Route::insertPair(int pickup, int delivery, int prev, int after) {
  nodeInfo* itP = &(solution->customers[pickup]);
  nodeInfo* itD = &(solution->customers[delivery]);

  if (first_visit == -1) {
    length = 2;
    first_visit = pickup;
    last_visit = delivery;
    itP->pred = -1;
    itP->succ = delivery;
    itD->pred = pickup;
    itD->succ = -1;
  } else {
    itP->succ = delivery;
    itD->pred = pickup;
    itP->pred = prev;
    if (prev == start_depot) itP->pred = -1;
    itD->succ = after;
    if (after == end_depot) itD->succ = -1;

    if (isTransferNode(prev))
      transfer_visits[prev].succ = pickup;
    else {
      if (prev != start_depot)
        solution->customers[prev].succ = pickup;
      else
        first_visit = pickup;
    }

    if (isTransferNode(after))
      transfer_visits[after].pred = delivery;
    else {
      if (after != end_depot)
        solution->customers[after].pred = delivery;
      else
        last_visit = delivery;
    }
    length += 2;  // p and d inserted
  }

  solution->assignment[pickup] = this;
  solution->assignment[delivery] = this;

  // update earliest and latest times
  timeType E_p =
      solution->instance->locations->at(pickup).timeWindows.getStart();
  timeType L_d =
      solution->instance->locations->at(delivery).timeWindows.getEnd();

  timeType earliest, latest, prev_earliest, next_latest, travel_prev,
      travel_next, travel;
  timeType earliest_aux, latest_aux;

  // earliest time at previous visit (if transfer, departure)
  prev_earliest =
      solution->instance->locations->at(start_depot).timeWindows.getStart();
  if (prev != start_depot) {
    if (isTransferNode(prev))
      prev_earliest = transfer_visits[prev].departure.getStart();
    else
      prev_earliest = solution->customers[prev].service.getStart();
  }
  // latest time at following visit (if transfer, arrival)
  next_latest =
      solution->instance->locations->at(end_depot).timeWindows.getEnd();
  if (after != end_depot) {
    if (isTransferNode(after))
      next_latest = transfer_visits[after].arrival.getEnd();
    else
      next_latest = solution->customers[after].service.getEnd();
  }

  travel_prev = metricMatrix->time(prev, pickup);
  earliest = max(E_p, prev_earliest + travel_prev);
  travel_next = metricMatrix->time(delivery, after);
  latest = min(L_d, next_latest - travel_next);

  travel = metricMatrix->time(pickup, delivery);
  latest_aux =
      latest - travel;  // latest_aux = min(pick_L, latest - travel_req);
  earliest_aux =
      earliest + travel;  // earliest_aux = max(deliv_E, earliest + travel_req);

  itP->service.set(earliest, latest_aux);
  itD->service.set(earliest_aux, latest);
  updateLatest(pickup);
  updateEarliest(delivery);
}

// TODO:: assert pred(after)=prev succ(prev)=after
void Route::insertCustomer(int element, int prev, int after) {
  nodeInfo* it = &(solution->customers[element]);

  if (first_visit == -1)  // prev=after=dept
  {
    length = 1;
    first_visit = element;
    last_visit = element;
    it->pred = -1;
    it->succ = -1;
  } else {
    it->succ = after;
    if (after == end_depot) it->succ = -1;
    it->pred = prev;
    if (prev == start_depot) it->pred = -1;

    if (isTransferNode(prev))
      transfer_visits[prev].succ = element;
    else {
      if (prev != start_depot)
        solution->customers[prev].succ = element;
      else
        first_visit = element;
    }

    if (isTransferNode(after))
      transfer_visits[after].pred = element;
    else {
      if (after != end_depot)
        solution->customers[after].pred = element;
      else
        last_visit = element;
    }
    length++;
  }

  solution->assignment[element] = this;

  // update earliest and latest times
  timeType E =
      solution->instance->locations->at(element).timeWindows.getStart();
  timeType L = solution->instance->locations->at(element).timeWindows.getEnd();

  timeType earliest, latest, prev_earliest, next_latest, travel_prev,
      travel_next;

  // earliest time at previous visit (if transfer, departure)
  prev_earliest =
      solution->instance->locations->at(start_depot).timeWindows.getStart();
  if (prev != start_depot) {
    if (isTransferNode(prev))
      prev_earliest = transfer_visits[prev].departure.getStart();
    else
      prev_earliest = solution->customers[prev].service.getStart();
  }
  // latest time at following visit (if transfer, arrival)
  next_latest =
      solution->instance->locations->at(end_depot).timeWindows.getEnd();
  if (after != end_depot) {
    if (isTransferNode(after))
      next_latest = transfer_visits[after].arrival.getEnd();
    else
      next_latest = solution->customers[after].service.getEnd();
  }

  travel_prev = metricMatrix->time(prev, element);
  earliest = max(E, prev_earliest + travel_prev);
  travel_next = metricMatrix->time(element, after);
  latest = min(L, next_latest - travel_next);

  it->service.set(earliest, latest);

  // updateLatest(element);
  // updateEarliest(element);
}

/*Methods that change route structure*/
void Route::appendNode(int node) {
  nodeInfo* it = &(solution->customers[node]);  // log(size) for map and
                                                // constant for unordered_map
  // should always find it!
  solution->assignment[node] = this;
  length++;
  if (first_visit == -1)  // first node in route
  {
    first_visit = node;
    last_visit = node;
    it->pred = -1;
    it->succ = -1;
  } else {
    it->succ = -1;
    it->pred = last_visit;

    if (isTransferNode(last_visit))
      transfer_visits[last_visit].succ = node;
    else
      solution->customers[last_visit].succ = node;  // log(size)
    last_visit = node;
  }
}

void Route::appendTransfer(int transfer) {
  transferIterator it = transfer_visits.find(transfer);  // log(size)
  if (it == transfer_visits.end())                       // not in the map yet
  {
    pair<transferIterator, bool> ret;
    ret = transfer_visits.emplace(transfer, transferInfo());
    it = ret.first;
  }
  length++;
  if (first_visit == -1)  // first node in route
  {
    first_visit = transfer;
    last_visit = transfer;
    it->second.pred = -1;
    it->second.succ = -1;
  } else {
    it->second.succ = -1;
    it->second.pred = last_visit;

    if (isTransferNode(last_visit))
      transfer_visits[last_visit].succ = transfer;
    else
      solution->customers[last_visit].succ = transfer;  // log(size)
    last_visit = transfer;
  }
}

// remove an element (not a transfer)
bool Route::removeElement(int element) {
  if (isTransferNode(element))  // use the propoer method!
    return false;

  nodeInfo* it =
      &(solution->customers[element]);  // O(1) for unordered_map log() for map

  int pred = it->pred;
  int succ = it->succ;

  if (pred != -1) {
    if (isTransferNode(pred))
      transfer_visits[pred].succ = succ;
    else
      solution->customers[pred].succ = succ;
  }
  if (succ != -1) {
    if (isTransferNode(succ))
      transfer_visits[succ].pred = pred;
    else
      solution->customers[succ].pred = pred;
  }

  if (first_visit == element) first_visit = succ;
  if (last_visit == element) last_visit = pred;

  solution->assignment[element] = nullptr;
  length--;

  return true;
}

// TODO: << stream operator?
void Route::show(int level) {
  if (level > 0) {
    cout << this << " (" << start_depot << "," << end_depot
         << ") cost: " << route_cost << endl;
    int next = first_visit;
    while (next != -1) {
      cout << next << "(";
      if (isTransferNode(next)) {
        cout << transfer_visits[next].arrival.getStart() << ","
             << transfer_visits[next].arrival.getEnd() << ","
             << transfer_visits[next].departure.getStart() << ","
             << transfer_visits[next].departure.getEnd() << ");";
        next = transfer_visits[next].succ;
      } else {
        cout << solution->customers[next].service.getStart() << ","
             << solution->customers[next].service.getEnd() << ");";
        // cout<<solution->customers[next].service.getStart()<<","<<solution->customers[next].service.getEnd()<<")
        // ["<< solution->assignment[next] <<"]--";
        next = solution->customers[next].succ;
      }
    }

    cout << "\nTransfers:\n";
    for (auto& x : transfer_visits) {
      cout << x.first << ": P[";
      for (int i = 0; i < x.second.pickups.size(); ++i) {
        assert(solution->assignment[x.second.pickups[i]] != nullptr);
        cout << x.second.pickups[i] << ",";
      }
      cout << "]; D[";
      for (int i = 0; i < x.second.drops.size(); ++i) {
        assert(solution->assignment[x.second.drops[i]] != nullptr);
        cout << x.second.drops[i] << ",";
      }
      cout << "]\n";
      cout << "arrivals: ";
      for (auto& route : x.second.arrival_routes) {
        assert(route != nullptr);
        cout << route << ",";
      }
      cout << " departures: ";
      for (auto& route : x.second.departure_routes) {
        assert(route != nullptr);
        cout << route << ",";
      }
      cout << endl;  // DEBUG
    }
    cout << "------------\n";
  } else {
    int next = first_visit;
    cout << this << " (" << start_depot << "," << end_depot
         << ") cost: " << route_cost << endl;
    while (next != -1) {
      cout << next << " ";
      if (isTransferNode(next))
        next = transfer_visits[next].succ;
      else
        next = solution->customers[next].succ;
    }
    cout << endl;
  }
}

void Route::showTemp() {
  cout << this << ":" << endl;
  int next = first_visit;
  while (next != -1) {
    cout << next << "(";
    if (isTransferNode(next)) {
      cout << transfer_visits[next].aux_arrival.getStart() << ","
           << transfer_visits[next].aux_arrival.getEnd() << ","
           << transfer_visits[next].aux_departure.getStart() << ","
           << transfer_visits[next].aux_departure.getEnd() << ");";
      next = transfer_visits[next].succ;
    } else {
      cout << solution->earliest[next] << "," << solution->latest[next] << ");";
      next = solution->customers[next].succ;
    }
  }
  cout << "\nTransfers:\n";
  for (auto& x : transfer_visits) {
    cout << x.first << ": P[";
    for (int i = 0; i < x.second.pickups.size(); ++i)
      cout << x.second.pickups[i] << ",";
    cout << "]; D[";
    for (int i = 0; i < x.second.drops.size(); ++i)
      cout << x.second.drops[i] << ",";
    cout << "]\n";
    cout << "arrivals: ";
    for (auto& route : x.second.arrival_routes) cout << route << ",";
    cout << " departures: ";
    for (auto& route : x.second.departure_routes) cout << route << ",";
    cout << endl;  // DEBUG
  }
  cout << "\n--------------------\n";
}

void Route::showDepartures() {
  for (auto& x : transfer_visits) {
    cout << " departures at " << x.first << endl;
    for (auto& route : x.second.departure_routes) route->show(1);
  }
}

void Route::showArrivals() {
  for (auto& x : transfer_visits) {
    cout << " arrivals at " << x.first << endl;
    for (auto& route : x.second.arrival_routes) route->show(1);
  }
}

// pre compute earlier and latest times
void Route::flagRoute() {
  int next = first_visit;
  while (next != -1) {
    if (isTransferNode(next)) {
      transfer_visits[next].arrival.set(-1, -1);
      transfer_visits[next].departure.set(-1, -1);
      next = transfer_visits[next].succ;
    } else {
      solution->customers[next].service.set(-1, -1);
      next = solution->customers[next].succ;
    }
  }
  updatedFlagE = false;
  updatedFlagL = false;
}

/*Informational methods*/
int Route::predecessor(int element) {
  if (isTransferNode(element)) return transfer_visits[element].pred;
  return solution->customers[element].pred;
}

int Route::successor(int element) {
  if (isTransferNode(element)) return transfer_visits[element].succ;
  return solution->customers[element].succ;
}

// evaluates cost change if inserting an element between before and after
distanceType Route::evaluateInsertion(int element, int before, int after) {
  distanceType dist_i, dist_j, dist_k;
  dist_i = metricMatrix->distance(before, element);
  dist_j = metricMatrix->distance(element, after);
  dist_k = metricMatrix->distance(before, after);

  return dist_i + dist_j - dist_k;
}

// evaluates the cost change if before--p--d--after
distanceType Route::evaluateReqInsertion(int p, int d, int before, int after) {
  distanceType dist_i, dist_j, dist_k, dist_l;
  dist_i = metricMatrix->distance(before, p);
  dist_j = metricMatrix->distance(p, d);
  dist_k = metricMatrix->distance(d, after);
  dist_l = metricMatrix->distance(before, after);

  return dist_i + dist_j + dist_k - dist_l;
}

// evaluates feasible insertions for pick and deliv on the same route (no
// transfer)
bool Route::evaluateReqInsertion(int pick, int deliv,
                                 priority_queue<insertionData>& insertions) {
  int prev = start_depot;
  int next = first_visit;
  int prev_d, next_d;
  timeType earliest, latest;
  timeType earliest_aux, latest_aux;
  timeType prev_earliest;
  timeType next_latest;
  timeType travel_prev, travel_next, travel_req;
  timeType update;
  distanceType increase;
  distanceType best_increase = INF_DISTANCE;  // FIXME: not used!

  timeType pick_E =
      solution->instance->locations->at(pick).timeWindows.getStart();
  timeType pick_L =
      solution->instance->locations->at(pick).timeWindows.getEnd();
  timeType deliv_E =
      solution->instance->locations->at(deliv).timeWindows.getStart();
  timeType deliv_L =
      solution->instance->locations->at(deliv).timeWindows.getEnd();

  // goes forward in the route i.e. current-->next!
  prev = start_depot;
  next = first_visit;
  prev_earliest =
      solution->instance->locations->at(start_depot).timeWindows.getStart();
  while (next != -1) {
    saveAuxiliaryTimes();  // roll back
    for (auto& x : transfer_visits) {
      for (auto& route : x.second.arrival_routes) route->saveAuxiliaryTimes();
      for (auto& route : x.second.departure_routes) route->saveAuxiliaryTimes();
    }

    if (prev != start_depot) {
      if (isTransferNode(prev))
        prev_earliest = transfer_visits[prev].aux_departure.getStart();
      else
        prev_earliest =
            solution
                ->earliest[prev];  // customer_visits[prev].service.getStart();
    }
    if (isTransferNode(next))
      next_latest = transfer_visits[next].aux_arrival.getEnd();
    else
      next_latest =
          solution->latest[next];  // customer_visits[next].service.getEnd();

    travel_prev = metricMatrix->time(prev, pick);
    earliest = max(pick_E, prev_earliest + travel_prev);
    travel_next = metricMatrix->time(pick, next);
    latest = min(pick_L, next_latest - travel_next);

    if (latest >= earliest)  // pickup is feasible
    {
      // cout<<"inserting "<<pick<<" between "<<prev<<","<<next<<" is feasible
      // "<<earliest<<" "<<latest<<endl;
      increase = evaluateInsertion(pick, prev, next);
      // update the auxiliary vectors
      if (isTransferNode(next))
        earliest_aux = transfer_visits[next].aux_arrival.getStart();
      else
        earliest_aux = solution->earliest[next];
      update = max(earliest_aux, earliest + travel_next);
      if (update != earliest_aux)  // solution->earliest[next]
      {
        // cout<<"earliest time at "<<next<<" changed!\n";
        if (isTransferNode(next))
          transfer_visits[next].aux_arrival.setStart(update);
        else
          solution->earliest[next] = update;
        updateAuxiliaryEarliest(next);
      }
      // updateAuxiliaryLatest(prev); delivery is inserted after.

      prev_d = next;
      if (isTransferNode(prev_d))
        next_d = transfer_visits[prev_d].succ;
      else
        next_d = solution->customers[prev_d].succ;
      while (next_d != -1) {
        // prev should never be the depot! (depot->d)
        if (isTransferNode(prev_d))
          prev_earliest = transfer_visits[prev_d].aux_departure.getStart();
        else
          prev_earliest =
              solution->earliest
                  [prev_d];  // customer_visits[prev_d].service.getStart();
        if (isTransferNode(next_d))
          next_latest = transfer_visits[next_d].aux_arrival.getEnd();
        else
          next_latest =
              solution->latest
                  [next_d];  // customer_visits[next_d].service.getEnd();

        travel_prev = metricMatrix->time(prev_d, deliv);
        earliest = max(deliv_E, prev_earliest + travel_prev);
        travel_next = metricMatrix->time(deliv, next_d);
        latest = min(deliv_L, next_latest - travel_next);
        if (latest >= earliest)  // delivery is feasible
        {
          // cout<<"\t inserting "<<deliv<<" between "<<prev_d<<","<<next_d<<"
          // is feasible "<<earliest<<" "<<latest<<endl; cout<<"total cost:
          // "<<increase + evaluateInsertion(deliv, prev_d, next_d)<<endl;
          insertions.push(
              insertionData(increase + evaluateInsertion(deliv, prev_d, next_d),
                            prev, next, prev_d, next_d, -1, this, nullptr));
        }

        prev_d = next_d;
        if (isTransferNode(prev_d))
          next_d = transfer_visits[prev_d].succ;
        else
          next_d = solution->customers[prev_d].succ;
      }
      // delivery just before depot
      prev_d = last_visit;
      next_d = end_depot;
      if (isTransferNode(prev_d))
        prev_earliest = transfer_visits[prev_d].aux_departure.getStart();
      else
        prev_earliest =
            solution->earliest
                [prev_d];  // customer_visits[prev_d].service.getStart();
      next_latest =
          solution->instance->locations->at(next_d).timeWindows.getEnd();

      travel_prev = metricMatrix->time(prev_d, deliv);
      earliest = max(deliv_E, prev_earliest + travel_prev);
      travel_next = metricMatrix->time(deliv, next_d);
      latest = min(deliv_L, next_latest - travel_next);
      if (latest >= earliest)  // delivery is feasible
      {
        // cout<<"\t inserting "<<deliv<<" between "<<prev_d<<","<<next_d<<" is
        // feasible "<<earliest<<" "<<latest<<endl; cout<<"total cost:
        // "<<increase + evaluateInsertion(deliv, prev_d, next_d)<<endl;
        insertions.push(
            insertionData(increase + evaluateInsertion(deliv, prev_d, next_d),
                          prev, next, prev_d, next_d, -1, this, nullptr));
      }
    }

    prev = next;
    if (isTransferNode(prev))
      next = transfer_visits[prev].succ;
    else
      next = solution->customers[prev].succ;
  }
  // now evaluates inserting p,d (d immediately after p)
  prev = start_depot;
  next = first_visit;  // last customer visit
  prev_earliest =
      solution->instance->locations->at(start_depot).timeWindows.getStart();
  while (next != -1) {
    if (prev != start_depot) {
      if (isTransferNode(prev))
        prev_earliest = transfer_visits[prev].aux_departure.getStart();
      else
        prev_earliest =
            solution
                ->earliest[prev];  // customer_visits[prev].service.getStart();
    }
    if (isTransferNode(next))
      next_latest = transfer_visits[next].aux_arrival.getEnd();
    else
      next_latest =
          solution->latest[next];  // customer_visits[next].service.getEnd();

    travel_prev = metricMatrix->time(prev, pick);
    travel_next = metricMatrix->time(deliv, next);
    travel_req = metricMatrix->time(pick, deliv);

    earliest = max(pick_E, prev_earliest + travel_prev);
    latest = min(deliv_L, next_latest - travel_next);

    latest_aux = min(pick_L, latest - travel_req);
    earliest_aux = max(deliv_E, earliest + travel_req);

    if (earliest <= latest_aux and earliest_aux <= latest) {
      // cout<<"--inserting "<<pick<<","<<deliv<<" between "<<prev<<","<<next<<"
      // is feasible "<<earliest<<" "<<latest<<endl; cout<<"total cost:
      // "<<evaluateReqInsertion(pick,deliv,prev,next)<<endl;
      insertions.push(
          insertionData(evaluateReqInsertion(pick, deliv, prev, next), prev,
                        next, prev, next, -1, this, nullptr));
    }
    prev = next;
    if (isTransferNode(prev))
      next = transfer_visits[prev].succ;
    else
      next = solution->customers[prev].succ;
  }
  // p-d immediatelly before depot
  prev = last_visit;
  if (last_visit == -1)  // route is empty
  {
    prev = start_depot;
    prev_earliest =
        solution->instance->locations->at(prev).timeWindows.getStart();
  } else {
    if (isTransferNode(last_visit))
      prev_earliest = transfer_visits[last_visit].aux_departure.getStart();
    else
      prev_earliest =
          solution->earliest
              [last_visit];  // customer_visits[last_visit].service.getStart();
  }
  next = end_depot;
  next_latest = solution->instance->locations->at(next).timeWindows.getEnd();

  travel_prev = metricMatrix->time(prev, pick);
  travel_next = metricMatrix->time(deliv, next);
  travel_req = metricMatrix->time(pick, deliv);

  earliest = max(pick_E, prev_earliest + travel_prev);
  latest = min(deliv_L, next_latest - travel_next);

  latest_aux = min(pick_L, latest - travel_req);       // latest - travel_req
  earliest_aux = max(deliv_E, earliest + travel_req);  // earliest + travel_req

  if (earliest <= latest_aux and earliest_aux <= latest) {
    // cout<<"inserting "<<pick<<","<<deliv<<" between "<<prev<<","<<next<<" is
    // feasible "<<earliest<<" "<<latest<<endl; cout<<"total cost:
    // "<<evaluateReqInsertion(pick,deliv,prev,next)<<endl;
    insertions.push(insertionData(evaluateReqInsertion(pick, deliv, prev, next),
                                  prev, next, prev, next, -1, this, nullptr));
  }

  return true;  // TODO: if no insertin was found, return false
}

// evaluate the insertion of customer before tranfer (if customer==pickup) or
// after (customer==delivery) only for delivery insertion: aux1 and aux2 (prev
// and next for the correspondent pickup) raux: pickup route increase: pickup
// detour
bool Route::evaluateInsertWithTransfer(
    int customer, int transfer, int aux1, int aux2, Route* raux,
    distanceType increase, priority_queue<insertionData>& insertions) {
  int prev = start_depot;
  int next = first_visit;
  int prev_d, next_d;
  timeType earliest, latest;
  timeType earliest_aux, latest_aux;
  timeType prev_earliest;
  timeType next_latest;
  timeType travel_prev, travel_next, travel_req;
  timeType update;
  bool stop, feasible;

  // TODO:check whether transfer is actuaaly visited by this route

  timeType E = solution->instance->locations->at(customer)
                   .timeWindows.getStart();  // eatliest service at the customer
  timeType L = solution->instance->locations->at(customer)
                   .timeWindows.getEnd();  // latesr service at the customer
  feasible = false;

  // check whether inserting a pickup or delivery
  if (solution->customers[customer].related >
      customer)  // NOTE:pickup id < delivery id
  {              // inserting a pickup in the subroute depot----transfer
    // save auxiliary times for all routes...
    for (int i = 0; i < solution->numRoutes;
         ++i)  // TODO: maybe not very route needs to be saved....
      if (solution->routes[i] != this)
        solution->routes[i]->saveAuxiliaryTimes();

    prev = start_depot;
    next = first_visit;
    prev_earliest =
        solution->instance->locations->at(start_depot).timeWindows.getStart();
    stop = false;  // stop when transfer is reached
    // saveAuxiliaryTimes();
    // goes forward in the route i.e. current-->next!
    while (!stop) {
      saveAuxiliaryTimes();  // roll back
      if (prev != start_depot) {
        if (isTransferNode(prev))
          prev_earliest = transfer_visits[prev].aux_departure.getStart();
        else
          prev_earliest =
              solution->earliest
                  [prev];  // customer_visits[prev].service.getStart();
      }
      if (isTransferNode(next)) {
        next_latest = transfer_visits[next].aux_arrival.getEnd();
        if (next == transfer)  // compute this iteration for transfer, but do
                               // not goes on!
          stop = true;
      } else
        next_latest =
            solution->latest[next];  // customer_visits[next].service.getEnd();

      travel_prev = metricMatrix->time(prev, customer);
      earliest = max(E, prev_earliest + travel_prev);
      travel_next = metricMatrix->time(customer, next);
      latest = min(L, next_latest - travel_next);
      if (latest >= earliest)  // pickup is feasible
      {
        increase = evaluateInsertion(customer, prev, next);

        // update the auxiliary vectors
        if (isTransferNode(next))
          earliest_aux = transfer_visits[next].aux_arrival.getStart();
        else
          earliest_aux = solution->earliest[next];
        update = max(earliest_aux, earliest + travel_next);
        if (update != earliest_aux)  // solution->earliest[next]
        {
          // cout<<"earliest time at "<<next<<" changed!\n";
          if (isTransferNode(next))
            transfer_visits[next].aux_arrival.setStart(update);
          else
            solution->earliest[next] = update;
          // save the times of impacted routes as well
          // for(auto& route : transfer_visits[transfer].departure_routes)
          //   route->saveAuxiliaryTimes();
          for (int i = 0; i < solution->numRoutes;
               ++i)  // TODO: maybe not very route needs to be saved....
            if (solution->routes[i] != this)
              solution->routes[i]->saveAuxiliaryTimes();

          updateAuxiliaryEarliest(next);
          if (isTransferNode(next))  // TODO:
          {
            for (auto& route : transfer_visits[next].departure_routes)
              route->updateAuxiliaryEarliest(next);
          }
          // ALERT("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
        }

        // call evaluateInsertWithTransfer for the delivery using routes
        // connected at transfer!
        for (auto& route : transfer_visits[transfer].departure_routes) {
          feasible = route->evaluateInsertWithTransfer(
              solution->customers[customer].related, transfer, prev, next, this,
              increase, insertions);
          // feasibility depends on whether the delivery can also be inserted
        }
        for (auto& route : transfer_visits[transfer].arrival_routes) {
          // exclude routes already considered
          for (auto& rte : transfer_visits[transfer].departure_routes)
            if (rte == route) continue;

          if (transfer_visits[transfer].aux_arrival.getStart() <=
              route->earliestInfoTransAux(transfer).first)
            if (transfer_visits[transfer].aux_arrival.getEnd() <=
                route->latestInfoTransAux(transfer).second)
              feasible = route->evaluateInsertWithTransfer(
                  solution->customers[customer].related, transfer, prev, next,
                  this, increase, insertions);
          // feasibility depends on whether the delivery can also be inserted
        }
        // TODO: routes visiting transfer (but not connect to this)
        connectedRoutes
            .clear();  // holds routes alredy connected at some transfer
        connectedRoutes.insert(this);  // DOES NOT CONSIDER THIS ONE!!! (loop!)
        for (auto& x : transfer_visits) {
          for (auto& route : x.second.arrival_routes)
            connectedRoutes.insert(route);
          for (auto& route : x.second.departure_routes)
            connectedRoutes.insert(route);
        }
        for (int i = 0; i < solution->numRoutes; ++i) {
          if (solution->routes[i]->visitsTransfer(transfer))
            if (connectedRoutes.count(solution->routes[i]) == 0) {
              assert(solution->routes[i] != this);
              if (transfer_visits[transfer].aux_arrival.getStart() <=
                  solution->routes[i]->earliestInfoTransAux(transfer).first)
                if (transfer_visits[transfer].aux_arrival.getEnd() <=
                    solution->routes[i]->latestInfoTransAux(transfer).second)
                  feasible = solution->routes[i]->evaluateInsertWithTransfer(
                      solution->customers[customer].related, transfer, prev,
                      next, this, increase, insertions);
            }
        }
        //--------------------------------------------------------
      }
      prev = next;
      if (isTransferNode(prev))
        next = transfer_visits[prev].succ;
      else
        next = solution->customers[prev].succ;
    }
  } else {        // inserting a delivery in the subroute transfer----depot
    prev = aux1;  // pickup insertion
    next = aux2;
    prev_d = transfer;
    if (isTransferNode(prev_d))
      next_d = transfer_visits[prev_d].succ;
    else
      next_d = solution->customers[prev_d].succ;
    // goes forward in the route i.e. current-->next!
    while (next_d != -1) {
      // NOTE: the function is never called with a delivery, unless if called by
      // the pickup on line 866 NOTE: i.e. timining is saved (saveAuxiliaryTimes
      // was called before) prev should never be the depot! (depot->d)
      if (isTransferNode(prev_d))
        prev_earliest = transfer_visits[prev_d].aux_departure.getStart();
      else
        prev_earliest =
            solution->earliest
                [prev_d];  // customer_visits[prev_d].service.getStart();
      if (isTransferNode(next_d))
        next_latest = transfer_visits[next_d].aux_arrival.getEnd();
      else
        next_latest =
            solution
                ->latest[next_d];  // customer_visits[next_d].service.getEnd();

      travel_prev = metricMatrix->time(prev_d, customer);
      earliest = max(E, prev_earliest + travel_prev);
      travel_next = metricMatrix->time(customer, next_d);
      latest = min(L, next_latest - travel_next);
      if (latest >= earliest)  // delivery is feasible
      {
        // insertionData(distanceType c, int b1, int a1, int b2, int a2, int t,
        // Route* r1, Route* r2)
        insertions.push(insertionData(
            increase + evaluateInsertion(customer, prev_d, next_d), prev, next,
            prev_d, next_d, transfer, raux, this));
        feasible = true;
      }

      prev_d = next_d;
      if (isTransferNode(prev_d))
        next_d = transfer_visits[prev_d].succ;
      else
        next_d = solution->customers[prev_d].succ;
    }
    // delivery just before depot
    prev_d = last_visit;
    next_d = end_depot;
    if (isTransferNode(prev_d))
      prev_earliest = transfer_visits[prev_d].aux_departure.getStart();
    else
      prev_earliest =
          solution->earliest
              [prev_d];  // customer_visits[prev_d].service.getStart();
    next_latest =
        solution->instance->locations->at(next_d).timeWindows.getEnd();

    travel_prev = metricMatrix->time(prev_d, customer);
    earliest = max(E, prev_earliest + travel_prev);
    travel_next = metricMatrix->time(customer, next_d);
    latest = min(L, next_latest - travel_next);
    if (latest >= earliest)  // delivery is feasible
    {
      // insertionData(distanceType c, int b1, int a1, int b2, int a2, int t,
      // Route* r1, Route* r2)
      insertions.push(
          insertionData(increase + evaluateInsertion(customer, prev_d, next_d),
                        prev, next, prev_d, next_d, transfer, raux, this));
      feasible = true;
    }
  }
  return feasible;
}

// insertion of a pickup(customer) before a transfer
// does not consider the insertion of the correspondent delivery
bool Route::evaluatePickupTrs(int customer, int transfer, timeType maxArrival,
                              priority_queue<insertionData>& insertions) {
  int prev = start_depot;
  int next = first_visit;
  timeType earliest, latest;
  timeType earliest_aux, latest_aux;
  timeType prev_earliest;
  timeType next_latest;
  timeType travel_prev, travel_next, travel_req;
  timeType update;
  distanceType increase;
  bool stop, feasible;

  // check whether transfer is actuaaly visited by this route
  transferIterator it;
  stop = true;
  for (it = transfer_visits.begin(); it != transfer_visits.end() and stop; ++it)
    if (it->first == transfer) stop = false;
  if (stop)  // transfer is not visited...
    return false;

  timeType E = solution->instance->locations->at(customer)
                   .timeWindows.getStart();  // eatliest service at the customer
  timeType L = solution->instance->locations->at(customer)
                   .timeWindows.getEnd();  // latesr service at the customer
  feasible = false;

  // inserting a pickup in the subroute depot----transfer
  prev = start_depot;
  next = first_visit;
  prev_earliest =
      solution->instance->locations->at(start_depot).timeWindows.getStart();
  stop = false;  // stop when transfer is reached

  // goes forward in the route i.e. current-->next!
  while (!stop) {
    saveAuxiliaryTimes();  // roll back //TODO:needed?
    if (prev != start_depot) {
      if (isTransferNode(prev))
        prev_earliest = transfer_visits[prev].aux_departure.getStart();
      else
        prev_earliest =
            solution
                ->earliest[prev];  // customer_visits[prev].service.getStart();
    }
    if (isTransferNode(next)) {
      next_latest = transfer_visits[next].aux_arrival.getEnd();
      if (next ==
          transfer)  // compute this iteration for transfer, but do not goes on!
        stop = true;
    } else
      next_latest =
          solution->latest[next];  // customer_visits[next].service.getEnd();

    travel_prev = metricMatrix->time(prev, customer);
    earliest = max(E, prev_earliest + travel_prev);
    travel_next = metricMatrix->time(customer, next);
    latest = min(L, next_latest - travel_next);
    if (latest >= earliest)  // pickup is feasible
    {
      increase = evaluateInsertion(customer, prev, next);
      // update the auxiliary vectors
      if (isTransferNode(next))
        earliest_aux = transfer_visits[next].aux_arrival.getStart();
      else
        earliest_aux = solution->earliest[next];
      update = max(earliest_aux, earliest + travel_next);
      if (update != earliest_aux)  // solution->earliest[next]
      {
        // cout<<"earliest time at "<<next<<" changed!\n";
        if (isTransferNode(next))
          transfer_visits[next].aux_arrival.setStart(update);
        else
          solution->earliest[next] = update;
        updateAuxiliaryEarliest(next);  // TODO:needed?
      }

      // check whether arrival at transfer is before maxArrival
      if (transfer_visits[transfer].aux_arrival.getStart() <= maxArrival) {
        insertions.push(insertionData(evaluateInsertion(customer, prev, next),
                                      prev, next, -1, -1, transfer, this,
                                      nullptr));
        feasible = true;
      }
      //(distanceType c, int b1, int a1, int b2, int a2, int t, Route* r1,
      //Route* r2)
    }
    prev = next;
    if (isTransferNode(prev))
      next = transfer_visits[prev].succ;
    else
      next = solution->customers[prev].succ;
  }
  return feasible;
}

// insertion of a delivery after a transfer
// does not consider the insertion of the correspondent pickup
// IDEA: if simetrical to pickup, the should it have a minimum departue?
bool Route::evaluateDeliveryTrs(int customer, int transfer,
                                priority_queue<insertionData>& insertions) {
  int prev_d, next_d;
  timeType earliest, latest;
  timeType earliest_aux, latest_aux;
  timeType prev_earliest;
  timeType next_latest;
  timeType travel_prev, travel_next, travel_req;
  timeType update;
  distanceType increase;
  bool stop, feasible;

  // check whether transfer is actuaaly visited by this route
  transferIterator it;
  stop = true;
  for (it = transfer_visits.begin(); it != transfer_visits.end() and stop; ++it)
    if (it->first == transfer) stop = false;
  if (stop)  // transfer is not visited...
    return false;

  timeType E = solution->instance->locations->at(customer)
                   .timeWindows.getStart();  // eatliest service at the customer
  timeType L = solution->instance->locations->at(customer)
                   .timeWindows.getEnd();  // latesr service at the customer
  feasible = false;

  // inserting a delivery in the subroute transfer----depot
  prev_d = transfer;
  if (isTransferNode(prev_d))
    next_d = transfer_visits[prev_d].succ;
  else
    next_d = solution->customers[prev_d].succ;
  // goes forward in the route i.e. current-->next!
  while (next_d != -1) {
    // prev should never be the depot! (depot->d)
    if (isTransferNode(prev_d))
      prev_earliest = transfer_visits[prev_d].departure.getStart();  // aux_
    else
      prev_earliest = solution->customers[prev_d]
                          .service.getStart();  // solution->earliest[prev_d];
    if (isTransferNode(next_d))
      next_latest = transfer_visits[next_d].arrival.getEnd();  // aux_
    else
      next_latest = solution->customers[next_d]
                        .service.getEnd();  // solution->latest[next_d];

    travel_prev = metricMatrix->time(prev_d, customer);
    earliest = max(E, prev_earliest + travel_prev);
    travel_next = metricMatrix->time(customer, next_d);
    latest = min(L, next_latest - travel_next);
    if (latest >= earliest)  // delivery is feasible
    {
      // insertionData(distanceType c, int b1, int a1, int b2, int a2, int t,
      // Route* r1, Route* r2)
      insertions.push(insertionData(evaluateInsertion(customer, prev_d, next_d),
                                    -1, -1, prev_d, next_d, transfer, nullptr,
                                    this));
      feasible = true;
    }

    prev_d = next_d;
    if (isTransferNode(prev_d))
      next_d = transfer_visits[prev_d].succ;
    else
      next_d = solution->customers[prev_d].succ;
  }
  // delivery just before depot
  prev_d = last_visit;
  next_d = end_depot;
  if (isTransferNode(prev_d))
    prev_earliest = transfer_visits[prev_d].departure.getStart();  //_aux
  else
    prev_earliest = solution->customers[prev_d]
                        .service.getStart();  // solution->earliest[prev_d];
  next_latest = solution->instance->locations->at(next_d).timeWindows.getEnd();

  travel_prev = metricMatrix->time(prev_d, customer);
  earliest = max(E, prev_earliest + travel_prev);
  travel_next = metricMatrix->time(customer, next_d);
  latest = min(L, next_latest - travel_next);
  if (latest >= earliest)  // delivery is feasible
  {
    // insertionData(distanceType c, int b1, int a1, int b2, int a2, int t,
    // Route* r1, Route* r2)
    insertions.push(insertionData(evaluateInsertion(customer, prev_d, next_d),
                                  -1, -1, prev_d, next_d, transfer, nullptr,
                                  this));
    feasible = true;
  }

  return feasible;
}

// save timing in the solution object
void Route::saveAuxiliaryTimes() {
  int next = first_visit;
  while (next != -1) {
    if (isTransferNode(next)) {
      transfer_visits[next].saveValues();
      next = transfer_visits[next].succ;
    } else {
      solution->earliest[next] = solution->customers[next].service.getStart();
      solution->latest[next] = solution->customers[next].service.getEnd();
      next = solution->customers[next].succ;
    }
  }
}

void Route::updateEarliest(int from) {
  // forward: earliest times
  timeType update;
  timeType travel;
  timeType earliest, aux;
  int prev, next;
  bool flag, transfer;
  pair<timeType, timeType> info;

  // ALERT("s");
  // first visit
  prev = from;
  if (isTransferNode(prev)) {  // first visit is a transfer location, earliest
                               // arrival does not change
    earliest = transfer_visits[prev].arrival.getStart();
    // update earliest departure
    for (auto& route : transfer_visits[prev].arrival_routes) {
      info = route->earliestInfoTrans(prev);
      earliest = max(earliest, info.first);  // TODO: service time at transfer
    }
    transfer_visits[prev].departure.setStart(earliest);
    next = transfer_visits[prev].succ;
  } else {  // customer
    next = solution->customers[prev].succ;
    earliest = solution->customers[prev].service.getStart();
  }

  flag = false;
  while (next != -1 and !flag) {
    travel = metricMatrix->time(prev, next);
    transfer = false;
    if (isTransferNode(next)) transfer = true;

    if (!transfer) {
      aux = solution->customers[next].service.getStart();
      update = max(aux, earliest + travel);
      if (update != aux) {
        solution->customers[next].service.setStart(update);
        earliest = update;
      } else  // does not need to go forward with updates
        flag = true;
    } else {
      aux = transfer_visits[next].arrival.getStart();
      update = max(aux, earliest + travel);
      if (update != aux) {
        earliest = update;
        transfer_visits[next].arrival.setStart(earliest);
        for (auto& route : transfer_visits[next].arrival_routes) {
          info = route->earliestInfoTrans(next);
          earliest =
              max(earliest, info.first);  // TODO: service time at transfer
        }
        transfer_visits[next].departure.setStart(earliest);

        // if earliest arrival change, then other routes (that depend on this
        // arrival) might also need to be updated...
        for (auto& route : transfer_visits[next].departure_routes)
          route->updateEarliest(next);
      }
      // just to be sure, compute the next update (flag=false)
    }

    prev = next;
    if (!transfer)
      next = solution->customers[prev].succ;
    else
      next = transfer_visits[prev].succ;
  }
  // ALERT("e");
}

void Route::updateLatest(int from) {
  timeType update;
  timeType travel;
  timeType latest, aux;
  int prev, next;
  bool flag, transfer;
  pair<timeType, timeType> info;

  // ALERT("s");
  // last visit
  prev = from;
  if (isTransferNode(
          prev)) {  // last visit is a transfer, lates departure does not change
    latest = transfer_visits[prev].departure.getEnd();
    // update latest arrival
    for (auto& route : transfer_visits[prev].departure_routes) {
      info = route->latestInfoTrans(prev);
      latest = min(latest, info.second);  // TODO: service time at transfer
    }
    transfer_visits[prev].arrival.setEnd(latest);
    next = transfer_visits[prev].pred;
  } else {  // customer
    next = solution->customers[prev].pred;
    latest = solution->customers[prev].service.getEnd();
  }

  flag = false;
  while (next != -1 and !flag) {
    travel = metricMatrix->time(next, prev);  // backwards
    transfer = false;
    if (isTransferNode(next)) transfer = true;

    if (!transfer) {
      aux = solution->customers[next].service.getEnd();
      update = min(aux, latest - travel);
      if (update != aux) {
        solution->customers[next].service.setEnd(update);
        latest = update;
      } else  // does not to go forward with updates
        flag = true;
    } else {
      aux = transfer_visits[next].departure.getEnd();
      update = min(aux, latest - travel);
      if (update != aux) {
        latest = update;
        transfer_visits[next].departure.setEnd(latest);
        for (auto& route :
             transfer_visits[next]
                 .departure_routes) {  // compute latest arrival, min of all
                                       // latest departures
          info = route->latestInfoTrans(next);
          latest = min(latest, info.second);  // TODO: service time at transfer
        }
        transfer_visits[next].arrival.setEnd(latest);

        // if latest departure change, then other routes (that the departure
        // depend on) might also need to be updated...
        for (auto& route : transfer_visits[next].arrival_routes)
          route->updateLatest(next);
      }
      // just to be sure, compute the next update (flag=false)
    }

    prev = next;
    if (!transfer)
      next = solution->customers[prev].pred;
    else
      next = transfer_visits[prev].pred;
  }
  // ALERT("e");
}

void Route::updateAuxiliaryEarliest(int from) {
  // forward: earliest times
  timeType update;
  timeType travel;
  timeType earliest, aux;
  int prev, next;
  bool flag, transfer;
  pair<timeType, timeType> info;

  // ALERT("s");
  // first visit
  prev = from;
  if (isTransferNode(prev)) {  // first visit is a transfer location, earliest
                               // arrival does not change
    earliest = transfer_visits[prev].aux_arrival.getStart();
    // update earliest departure
    for (auto& route : transfer_visits[prev].arrival_routes) {
      info = route->earliestInfoTransAux(prev);
      earliest = max(earliest, info.first);  // TODO: service time at transfer
    }
    transfer_visits[prev].aux_departure.setStart(earliest);
    next = transfer_visits[prev].succ;
  } else {  // customer
    next = solution->customers[prev].succ;
    earliest = solution->earliest[prev];
  }

  flag = false;
  while (next != -1 and !flag) {
    travel = metricMatrix->time(prev, next);
    transfer = false;
    if (isTransferNode(next)) transfer = true;

    if (!transfer) {
      aux = solution->earliest[next];
      update = max(aux, earliest + travel);
      if (update != aux) {
        solution->earliest[next] = update;
        earliest = update;
      } else  // does not need to go forward with updates
        flag = true;
    } else {
      aux = transfer_visits[next].aux_arrival.getStart();
      update = max(aux, earliest + travel);
      if (update != aux) {
        earliest = update;
        transfer_visits[next].aux_arrival.setStart(earliest);
        for (auto& route : transfer_visits[next].arrival_routes) {
          info = route->earliestInfoTransAux(next);
          earliest =
              max(earliest, info.first);  // TODO: service time at transfer
        }
        transfer_visits[next].aux_departure.setStart(earliest);

        // if earliest arrival change, then other routes (that depend on this
        // arrival) might also need to be updated...
        for (auto& route : transfer_visits[next].departure_routes)
          route->updateAuxiliaryEarliest(next);
      }
      // just to be sure, compute the next update (flag=false)
    }

    prev = next;
    if (!transfer)
      next = solution->customers[prev].succ;
    else
      next = transfer_visits[prev].succ;
  }
  // ALERT("e");
}

void Route::updateAuxiliaryLatest(int from) {
  timeType update;
  timeType travel;
  timeType latest, aux;
  int prev, next;
  bool flag, transfer;
  pair<timeType, timeType> info;

  // ALERT("s");
  // last visit
  prev = from;
  if (isTransferNode(
          prev)) {  // last visit is a transfer, lates departure does not change
    latest = transfer_visits[prev].aux_departure.getEnd();
    // update latest arrival
    for (auto& route : transfer_visits[prev].departure_routes) {
      info = route->latestInfoTransAux(prev);
      latest = min(latest, info.second);  // TODO: service time at transfer
    }
    transfer_visits[prev].aux_arrival.setEnd(latest);
    next = transfer_visits[prev].pred;
  } else {  // customer
    next = solution->customers[prev].pred;
    latest = solution->latest[prev];
  }

  flag = false;
  while (next != -1 and !flag) {
    travel = metricMatrix->time(next, prev);  // backwards
    transfer = false;
    if (isTransferNode(next)) transfer = true;

    if (!transfer) {
      aux = solution->latest[next];
      update = min(aux, latest - travel);
      if (update != aux) {
        solution->latest[next] = update;
        latest = update;
      } else  // does not to go forward with updates
        flag = true;
    } else {
      aux = transfer_visits[next].aux_departure.getEnd();
      update = min(aux, latest - travel);
      if (update != aux) {
        latest = update;
        transfer_visits[next].aux_departure.setEnd(latest);
        for (auto& route :
             transfer_visits[next]
                 .departure_routes) {  // compute latest arrival, min of all
                                       // latest departures
          info = route->latestInfoTransAux(next);
          latest = min(latest, info.second);  // TODO: service time at transfer
        }
        transfer_visits[next].aux_arrival.setEnd(latest);

        // if latest departure change, then other routes (that the departure
        // depend on) might also need to be updated...
        for (auto& route : transfer_visits[next].arrival_routes)
          route->updateAuxiliaryLatest(next);
      }
      // just to be sure, compute the next update (flag=false)
    }

    prev = next;
    if (!transfer)
      next = solution->customers[prev].pred;
    else
      next = transfer_visits[prev].pred;
  }
  // ALERT("e");
}

// element is not a transfer!
distanceType Route::evaluateRemoval(int element) {
  int pred = solution->customers[element].pred;
  int succ = solution->customers[element].succ;

  if (pred == -1) pred = start_depot;
  if (succ == -1) succ = end_depot;

  distanceType dist_i, dist_j, dist_k;
  dist_i = metricMatrix->distance(pred, element);
  dist_j = metricMatrix->distance(element, succ);
  dist_k = metricMatrix->distance(pred, succ);

  return dist_i + dist_j - dist_k;
}

// p and d on the same route, d immediatelly after p
distanceType Route::evaluateReqRemoval(int p, int d) {
  int pred = solution->customers[p].pred;
  int succ = solution->customers[d].succ;

  if (pred == -1) pred = start_depot;
  if (succ == -1) succ = end_depot;

  distanceType dist_i, dist_j, dist_k, dist_l;
  dist_i = metricMatrix->distance(pred, p);
  dist_j = metricMatrix->distance(p, d);
  dist_k = metricMatrix->distance(d, succ);
  dist_l = metricMatrix->distance(pred, succ);

  return dist_i + dist_j + dist_k - dist_l;
}

// computes latest service times (assuming the route has no transfers)
void Route::latestTimes() {
  int current = end_depot;
  int next = last_visit;  // last customer visit
  timeType latest = 0;
  timeType upper_window;
  timeType travel_time;
  pair<timeType, timeType> info;

  if (updatedFlagL) return;

  // goes backward in the route i.e. next-->current!
  latest = solution->instance->locations->at(end_depot).timeWindows.getEnd();
  while (next != -1) {  // latest = min(L_i,l_j-t_ij)
    upper_window = solution->instance->locations->at(next).timeWindows.getEnd();
    travel_time = metricMatrix->time(next, current);
    latest = min(upper_window, latest - travel_time);
    if (isTransferNode(next)) {
      // latest departure does not depend on transfers
      transfer_visits[next].departure.setEnd(latest);
      // latest arrival depends on transfers
      for (auto& route : transfer_visits[next].departure_routes) {
        info = route->latestInfoTrans(next);  // tie(a, b, c) = foo();
        if (info.second < 0)                  // not computed yet
        {
          route->latestTimes();
          info = route->latestInfoTrans(next);
        }
        latest = min(latest, info.second);  // TODO: service time at transfer
      }

      transfer_visits[next].arrival.setEnd(latest);
      current = next;
      next = transfer_visits[next].pred;
    } else {
      solution->customers[next].service.setEnd(latest);
      current = next;
      next = solution->customers[next].pred;
    }
  }
  updatedFlagL = true;
}

// computes earliest service times (assuming the route has no transfers)
void Route::earliestTimes() {
  int current = start_depot;
  int next = first_visit;  // last customer visit
  timeType earliest = 0;
  timeType lower_window;
  timeType travel_time;
  pair<timeType, timeType> info;

  if (updatedFlagE)  // already computed
    return;

  // cout<<"---"<<this<<endl;
  // for(auto&t : transfer_visits)
  //   for(auto& route : t.second.arrival_routes)
  //     cout<<route<<" ";
  // cout<<endl;
  // goes forward in the route i.e. current-->next!
  earliest =
      solution->instance->locations->at(start_depot).timeWindows.getStart();
  while (next != -1) {  // earliest = max(E_i,e_j+t_ij)
    lower_window =
        solution->instance->locations->at(next).timeWindows.getStart();
    travel_time = metricMatrix->time(current, next);
    earliest = max(lower_window, earliest + travel_time);
    if (isTransferNode(next)) {
      // earliest arrival does not depend on transfers
      transfer_visits[next].arrival.setStart(earliest);
      // earliest departure depends on transfers
      for (auto& route : transfer_visits[next].arrival_routes) {
        assert(route != nullptr);
        info = route->earliestInfoTrans(next);  // tie(a, b, c) = foo();
        if (info.first < 0)                     // not computed yet
        {
          route->earliestTimes();
          info = route->earliestInfoTrans(next);
        }
        earliest = max(earliest, info.first);  // TODO: service time at transfer
      }

      transfer_visits[next].departure.setStart(earliest);
      current = next;
      next = transfer_visits[next].succ;
    } else {
      solution->customers[next].service.setStart(earliest);
      current = next;
      next = solution->customers[next].succ;
    }
  }
  updatedFlagE = true;
}

pair<timeType, timeType> Route::earliestInfoTrans(int transfer) {
  timeType earliest_a = transfer_visits[transfer].arrival.getStart();
  timeType earliest_d = transfer_visits[transfer].departure.getStart();
  return make_pair(earliest_a, earliest_d);  // make_tuple for n > 2
}

pair<timeType, timeType> Route::earliestInfoTransAux(int transfer) {
  timeType earliest_a = transfer_visits[transfer].aux_arrival.getStart();
  timeType earliest_d = transfer_visits[transfer].aux_departure.getStart();
  return make_pair(earliest_a, earliest_d);  // make_tuple for n > 2
}

pair<timeType, timeType> Route::latestInfoTrans(int transfer) {
  timeType latest_a = transfer_visits[transfer].arrival.getEnd();
  timeType latest_d = transfer_visits[transfer].departure.getEnd();
  return make_pair(latest_a, latest_d);  // make_tuple for n > 2
}

pair<timeType, timeType> Route::latestInfoTransAux(int transfer) {
  timeType latest_a = transfer_visits[transfer].aux_arrival.getEnd();
  timeType latest_d = transfer_visits[transfer].aux_departure.getEnd();
  return make_pair(latest_a, latest_d);  // make_tuple for n > 2
}

bool Route::isTransferNode(int node) {
  transferIterator it = transfer_visits.find(node);  // log(size)
  if (it != transfer_visits.end()) return true;
  return false;
}

// true if new element; false otherwise (route already connected)
bool Route::addTrsPick(Route* route, int transfer, requestData& request) {
  pair<set<Route*>::iterator, bool> ret;
  // assuming transfer is already in *this
  transfer_visits[transfer].pickups.push_back(request.pickup);
  ret = transfer_visits[transfer].arrival_routes.insert(route);
  return ret.second;
}
// true if new element; false otherwise (route already connected)
bool Route::addTrsDrop(Route* route, int transfer, requestData& request) {
  pair<set<Route*>::iterator, bool> ret;
  // assuming transfer is already in *this
  transfer_visits[transfer].drops.push_back(request.delivery);
  ret = transfer_visits[transfer].departure_routes.insert(route);
  return ret.second;
}

// void Route::setInstance(problemData* data)
// { instance = data; }

void Route::setSolution(Solution* sol) { solution = sol; }

void Route::setMetrics(Metric* metric) { metricMatrix = metric; }

distanceType Route::getCost() {
  int next = first_visit;
  int current = start_depot;
  distanceType total = 0;
  while (next != -1) {
    total += metricMatrix->distance(current, next);
    current = next;
    if (isTransferNode(next))
      next = transfer_visits[next].succ;
    else
      next = solution->customers[next].succ;
  }
  total += metricMatrix->distance(current, end_depot);
  route_cost = total;
  return total;
}

double Route::getMatrixValue(Metric* metric) {
  int next = first_visit;
  int current = start_depot;
  distanceType total = 0;
  while (next != -1) {
    total += metricMatrix->matrixValue(current, next);
    current = next;
    if (isTransferNode(next))
      next = transfer_visits[next].succ;
    else
      next = solution->customers[next].succ;
  }
  total += metricMatrix->matrixValue(current, end_depot);
  route_cost = total;
  return total;
}

pair<timeType, timeType> Route::timeLenght() {
  assert(last_visit != -1);
  timeType earliest;
  timeType latest;
  if (isTransferNode(last_visit)) {
    earliest = transfer_visits[last_visit].departure.getStart();
    latest = transfer_visits[last_visit].departure.getEnd();
  } else {
    earliest = solution->customers[last_visit].service.getStart();
    latest = solution->customers[last_visit].service.getEnd();
  }

  earliest += metricMatrix->time(last_visit, end_depot);
  latest += metricMatrix->time(last_visit, end_depot);

  return make_pair(earliest, latest);  // make_tuple for n > 2
}

timeType Route::totalWaiting() {
  int next = first_visit;
  timeType wait = 0;

  while (next != -1) {
    if (isTransferNode(next)) {
      wait += transfer_visits[next].departure.getEnd() -
              transfer_visits[next].arrival.getStart();
      next = transfer_visits[next].succ;
    } else {
      wait += solution->customers[next].service.getEnd() -
              solution->customers[next].service.getStart();
      next = solution->customers[next].succ;
    }
  }

  return wait;
}

timeType Route::transferWaiting(int transfer) {
  timeType earliest_a = transfer_visits[transfer].arrival.getStart();
  timeType latest_d = transfer_visits[transfer].departure.getEnd();

  return latest_d - earliest_a;
}

tuple<int, int, timeType> Route::transferOperations(Route* croute) {
  int transfer = -1;
  int pickupOps, deliveryOps;
  int p, d;
  pair<timeType, timeType> earliestTr;
  pair<timeType, timeType> latestTr;
  timeType wait = 0;

  for (auto& x : transfer_visits) {
    for (auto& route : x.second.arrival_routes)
      if (route == croute) transfer = x.first;
    for (auto& route : x.second.departure_routes)
      if (route == croute) transfer = x.first;
  }

  pickupOps = deliveryOps = 0;
  if (transfer != -1) {
    earliestTr = croute->earliestInfoTrans(transfer);
    latestTr = croute->latestInfoTrans(transfer);

    cout << "Pickups:";
    for (int i = 0; i < transfer_visits[transfer].pickups.size(); ++i) {
      p = transfer_visits[transfer].pickups[i];
      if (solution->assignment[p] == croute) {
        deliveryOps++;
        cout << p << ",";
      }
    }
    cout << endl << "Deliveries:";
    for (int i = 0; i < transfer_visits[transfer].drops.size(); ++i) {
      d = transfer_visits[transfer].drops[i];
      if (solution->assignment[d] == croute) {
        pickupOps++;
        cout << d << ",";
      }
    }
    cout << endl;

    wait = 0;
    if (pickupOps > 0)
      wait = max(wait, earliestInfoTrans(transfer).first - earliestTr.first);
    if (deliveryOps > 0)
      wait = max(wait, earliestTr.first - earliestInfoTrans(transfer).first);
  }

  return make_tuple(pickupOps, deliveryOps, wait);
}

// feasibility methods
bool Route::isFeasible() {
  int next = first_visit;
  int current = start_depot;
  while (next != -1) {
    current = next;
    if (isTransferNode(next)) {
      if (transfer_visits[next].arrival.getStart() >
          transfer_visits[next].arrival.getEnd())
        return false;
      if (transfer_visits[next].departure.getStart() >
          transfer_visits[next].departure.getEnd())
        return false;
      next = transfer_visits[next].succ;
    } else {
      if (solution->customers[next].service.getStart() >
          solution->customers[next].service.getEnd())
        return false;
      next = solution->customers[next].succ;
    }
  }
  return true;
}

bool Route::multipleConnections() {
  pair<set<Route*>::iterator, bool> ret;

  allRoutes.clear();  // routes already connected

  for (auto& x : transfer_visits) {
    connectedRoutes.clear();  // Connected routes at this transfer location
    for (auto& route :
         x.second.arrival_routes)  // NOTE:if crossed, can be the same
      connectedRoutes.insert(route);
    for (auto& route : x.second.departure_routes) connectedRoutes.insert(route);

    // check wheather routes at this transfer was already connected at another
    // transfer
    for (auto& route : connectedRoutes)  // NOTE: connectedRoutes does not
                                         // contain repeated routes
    {
      ret = allRoutes.insert(route);
      if (ret.second == false) {
        // cout<<this<<endl;
        // cout<<*(ret.first);
        this->show(1);
        (*(ret.first))->show(1);
        WATCH(x.first);
        return false;  // route is connected with another route more than once
                       // (at another transfer)
      }
    }
  }
  return true;  // ok
}

Route& Route::operator=(Route& route) {
  first_visit = route.first_visit;
  last_visit = route.last_visit;
  start_depot = route.start_depot;
  end_depot = route.end_depot;
  route_cost = route.route_cost;
  length = route.length;
  weight = route.weight;

  // NOTE: need to be careful here since the Route ptrs in the sets arrival and
  // departure might reference to routes in other solution... NOTE: after an
  // assgnment (=), construct those sets from the vectors pickups and drops
  transfer_visits = route.transfer_visits;
}

void Route::jsonify(nlohmann::json& list_, nlohmann::json& instance) {
  int next = first_visit;
  for (nlohmann::json::iterator it = instance["depots"].begin();
       it != instance["depots"].end(); ++it)
    if ((*it)["id"] == start_depot) {
      list_.push_back(it.key());
      break;
    }

  while (next != -1) {
    if (isTransferNode(next)) {
      for (nlohmann::json::iterator it = instance["transfers"].begin();
           it != instance["transfers"].end(); ++it)
        if ((*it)["id"] == next) {
          list_.push_back(
              it.key() + ":(" +
              to_string(transfer_visits[next].arrival.getStart()) + "," +
              to_string(transfer_visits[next].arrival.getEnd()) + "," +
              to_string(transfer_visits[next].departure.getStart()) + "," +
              to_string(transfer_visits[next].departure.getEnd()) + ")");
          break;
        }
      // list_.push_back(to_string(next));
      next = transfer_visits[next].succ;
    } else {
      list_.push_back(
          to_string(next) + ":(" +
          to_string(solution->customers[next].service.getStart()) + "," +
          to_string(solution->customers[next].service.getEnd()) + ")");
      next = solution->customers[next].succ;
    }
  }
  for (nlohmann::json::iterator it = instance["depots"].begin();
       it != instance["depots"].end(); ++it)
    if ((*it)["id"] == end_depot) {
      list_.push_back(it.key());
      break;
    }
}
