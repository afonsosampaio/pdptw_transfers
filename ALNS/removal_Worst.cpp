#include "removal_Worst.h"

RemoveWorst::RemoveWorst(string nm, vector<requestData*>* reqs, double min,
                         double max, default_random_engine* gen,
                         uniform_real_distribution<double>* dist, double rho)
    : ALNS_Operator(nm, gen, dist, rho) {
  requests = reqs;
  // uni_dist = dist;
  // rand_eng = gen;
  min_removal = min;
  max_removal = max;
}

int RemoveWorst::execute(Solution* solution) {
  // controls information on the performance of the operator
  local_count++;

  // cout<<"Remove worst\n";
  int n = requests->size();
  Route* rp;
  Route* rd;
  int p, d, r;
  distanceType profit;

  for (int i = 0; i < n; ++i) {
    p = requests->at(i)->pickup;
    d = requests->at(i)->delivery;
    rp = solution->assignment[p];
    rd = solution->assignment[d];

    profit = 0;
    if (rp == rd and
        rp->successor(p) ==
            d)  // served by the same route //and rp.predecessor(d)==p
      profit = rp->evaluateReqRemoval(p, d);
    else {
      profit = rp->evaluateRemoval(p);
      profit = profit + rd->evaluateRemoval(d);
    }
    requests_order.push(make_pair(profit, i));
  }

  double rand_nun = (*uni_dist)(*rand_eng);
  int lower = REMOVAL_INTERVAL_MIN > int(n * min_removal)
                  ? REMOVAL_INTERVAL_MIN
                  : int(n * min_removal);  // at least % of the requests
  int upper = REMOVAL_INTERVAL_MAX > int(n * max_removal)
                  ? REMOVAL_INTERVAL_MAX
                  : int(n * max_removal);  // at most % of the requests

  int q = (upper - lower + 1) * rand_nun + lower;
  // cout<<"removing (worst)"<<q<<endl;
  while (!requests_order.empty() and q > 0) {
    r = requests_order.top().second;
    p = requests->at(r)->pickup;
    d = requests->at(r)->delivery;

    // cout<<"Removing: "<<p<<" "<<d<<endl;

    rp = solution->assignment[p];
    rd = solution->assignment[d];
    assert(rp != nullptr);  // DEBUG
    assert(rd != nullptr);  // DEBUG
    rp->removeElement(p);
    rd->removeElement(d);
    // solution->assignment[p] = nullptr; ..removeElement already puts null
    // solution->assignment[d] = nullptr;
    // cout<<"Removed: "<<p<<" "<<d<<endl;
    // cout<<"Removed: "<<p<<" "<<d<<endl;
    requests_order.pop();
    q--;
  }
  // clear the queue
  while (!requests_order.empty()) requests_order.pop();

  // remove 'empty' routes (not servig any request e.g. maybe visiting empty
  // transfer(s))
  for (int i = 0; i < solution->numRoutes; ++i)
    if (solution->routes[i]->isEmpty())
      solution->removeRoute(
          i--);  // after removal, check position i again (anoter route)

  return 0;  // TODO: return !=0 if not all q requests were removed
}

void RemoveWorst::updateScore(int reward) { score += reward; }
