#ifndef REMOVE_ROUTE_H
#define REMOVE_RANDOM_H

#include <iostream>  //TODO: not needed!
#include "Operator_base.h"
#include "Roulette.h"
#include "Route.h"

using namespace std;

class RemoveRoute : public ALNS_Operator {
 public:
  RemoveRoute(string, vector<requestData*>*, double, double,
              default_random_engine*, uniform_real_distribution<double>*,
              double);
  int execute(Solution*);
  void updateScore(int);

 private:
  vector<requestData*>* requests;
  RouletteWheel* wheel;

  Solution* sol;
  int routesRemoved;
  int deltaRoute;  // change in the number of routes
  double min_removal;
  double max_removal;
  // default_random_engine* rand_eng;
  // uniform_real_distribution<double>* uni_dist;
};

#endif
