#ifndef ROUTE_H
#define ROUTE_H

#include <map>
// #include <unordered_map>
#include <iostream>
#include <queue>
#include <set>
#include "Data.h"
#include "Metrics.h"
#include "Solution.h"
#include "json.h"

using namespace std;

class Route {
 private:
  int first_visit, last_visit;  // after and before the depot, respectively
  int start_depot, end_depot;
  int length;
  distanceType route_cost;
  double weight;
  map<int, transferInfo> transfer_visits;
  // problemData* instance;
  Solution* solution;  // the solution the route is associated to
  Metric* metricMatrix;
  bool updatedFlagE;
  bool updatedFlagL;

  // aux
  void updateAuxiliaryEarliest(int);
  void updateAuxiliaryLatest(int);
  void saveAuxiliaryTimes();
  pair<timeType, timeType> earliestInfoTransAux(int);
  pair<timeType, timeType> latestInfoTransAux(int);
  set<Route*> connectedRoutes;
  set<Route*> allRoutes;

 public:
  Route()
      : first_visit(-1),
        last_visit(-1),
        start_depot(-1),
        end_depot(-1),
        length(0),
        route_cost(0.0),
        weight(0){};
  Route(Solution*, Metric*, int, int, int, int);
  void clear();
  void reset();

  // instance Data
  // void setInstance(problemData*);
  void setMetrics(Metric*);

  void updateLatest(int);
  void updateEarliest(int);

  // structure
  void construct();
  void setSolution(Solution*);
  void setDepot(int start, int end) {
    start_depot = start;
    end_depot = end;
  }
  int getFirst() { return first_visit; }
  int getLast() { return last_visit; }
  void appendNode(int);
  bool removeElement(int);
  int predecessor(int);
  int successor(int);
  void insertCustomer(int, int, int);
  void insertPair(int, int, int, int);
  void insertRequest(int, int, const insertionData&);
  void flagRoute();

  // Transfers
  void appendTransfer(int);
  bool isTransferNode(int);
  void connectTransfers();
  void pruneTransfers(int);
  bool addTrsPick(Route*, int, requestData&);
  bool addTrsDrop(Route*, int, requestData&);
  pair<timeType, timeType> earliestInfoTrans(int);
  pair<timeType, timeType> latestInfoTrans(int);
  bool visitsTransfer();
  bool visitsTransfer(int);

  // information
  double getMatrixValue(Metric*);
  int getCost();
  void latestTimes();
  void earliestTimes();
  distanceType evaluateReqRemoval(int, int);
  distanceType evaluateRemoval(int);
  bool evaluateReqInsertion(int, int, priority_queue<insertionData>&);
  bool evaluateInsertWithTransfer(int, int, int, int, Route*, distanceType,
                                  priority_queue<insertionData>&);
  bool evaluatePickupTrs(int, int, timeType, priority_queue<insertionData>&);
  bool evaluateDeliveryTrs(int, int, priority_queue<insertionData>&);

  distanceType evaluateInsertion(int, int, int);
  distanceType evaluateReqInsertion(int, int, int, int);
  int getLength() { return length; }
  bool isEmpty();
  int fullReqsServiced();
  int transReqsServiced();
  pair<timeType, timeType> timeLenght();
  tuple<int, int, timeType> transferOperations(Route*);
  timeType transferWaiting(int);
  timeType totalWaiting();
  bool isFeasible();
  bool multipleConnections();

  // printing
  void show(int);
  void showTemp();
  void showDepartures();
  void showArrivals();
  void jsonify(nlohmann::json&, nlohmann::json&);

  // for use in the roullete wheel
  void setWeight(double w) { weight = w; }
  double getWeight() { return weight; }

  // operators overload
  Route& operator=(Route&);

  // iterators
  transferIterator first_transfer() { return transfer_visits.begin(); }
  transferIterator last_transfer() { return transfer_visits.end(); }

  routesIterator first_depRoute(int transfer) {
    return transfer_visits[transfer].departure_routes.begin();
  }
  routesIterator last_depRoute(int transfer) {
    return transfer_visits[transfer].departure_routes.end();
  }

  routesIterator first_arrRoute(int transfer) {
    return transfer_visits[transfer].arrival_routes.begin();
  }
  routesIterator last_arrRoute(int transfer) {
    return transfer_visits[transfer].arrival_routes.end();
  }
};

#endif
