#include "removal_Cluster.h"

RemoveCluster::RemoveCluster(string nm, vector<requestData*>* reqs, Metric* me,
                             default_random_engine* gen,
                             uniform_real_distribution<double>* dis, double r)
    : ALNS_Operator(nm, gen, dis, r) {
  requests = reqs;
  metrics = me;

  /*clusterize the instance*/
  int n = requests->size();
  distanceType dist;
  int ci, cj, c;

  int pi, pj;
  int di, dj;
  for (int i = 0; i < n; ++i) {
    pi = requests->at(i)->pickup;
    clustering[pi] = pi;
    clust_diameters[pi] = 0;
    for (int j = i + 1; j < n; ++j) {
      pj = requests->at(j)->pickup;
      dist = metrics->distance(pi, pj);  // NOTE:assuming symmetry
      pickups_order.push(make_pair(dist, make_pair(pi, pj)));
    }

    di = requests->at(i)->delivery;
    clustering[di] = di;
    clust_diameters[di] = 0;
    for (int j = i + 1; j < n; ++j) {
      dj = requests->at(j)->delivery;
      dist = metrics->distance(di, dj);  // NOTE:assuming symmetry
      delivery_order.push(make_pair(dist, make_pair(di, dj)));
    }
  }

  // pickups
  while (!pickups_order.empty()) {
    r = pickups_order.top().first;
    if (r <= CLUSTER_DIAMETER) {
      pi = pickups_order.top().second.first;
      pj = pickups_order.top().second.second;
      // merge if diameter is not exceeded
      ci = clustering[pi];
      cj = clustering[pj];
      if (ci != cj) {  // not in the same cluster
        c = min(ci, cj);
        cj = max(ci, cj);
        clust_diameters[c] = r < clust_diameters[c] ? clust_diameters[c] : r;
        for (int i = 0; i < n - 1; ++i) {  // merge
          pi = requests->at(i)->pickup;
          if (clustering[pj] == cj) clustering[pj] = c;
        }
      }
    }
    pickups_order.pop();
  }

  // deliveries
  while (!delivery_order.empty()) {
    r = delivery_order.top().first;
    if (r <= CLUSTER_DIAMETER) {
      di = delivery_order.top().second.first;
      dj = delivery_order.top().second.second;
      // merge if diameter is not exceeded
      ci = clustering[di];
      cj = clustering[dj];
      if (ci != cj) {  // not in the same cluster
        c = min(ci, cj);
        cj = max(ci, cj);
        clust_diameters[c] = r < clust_diameters[c] ? clust_diameters[c] : r;
        for (int i = 0; i < n - 1; ++i) {  // merge
          di = requests->at(i)->delivery;
          if (clustering[dj] == cj) clustering[dj] = c;
        }
      }
    }
    delivery_order.pop();
  }

  // DEBUG
  // for(int i=0; i<n; ++i)
  // {
  //   pi = requests->at(i)->pickup;
  //   cout<<pi<<" @cluster: "<<clustering[pi]<<endl;
  // }
  // DEBUG
  // for(int i=0; i<n; ++i)
  // {
  //   di = requests->at(i)->delivery;
  //   cout<<di<<" @cluster: "<<clustering[di]<<endl;
  // }

  // build the clusters vectors
  map<int, vector<int>*>::iterator it;
  vector<int>* cluster;
  for (int i = 0; i < n; ++i) {
    pi = requests->at(i)->pickup;
    di = requests->at(i)->delivery;
    // look for the pickup cluster
    it = pickClusters.find(clustering[pi]);
    if (it != pickClusters.end()) {
      cluster = it->second;
      cluster->push_back(pi);
    } else {
      cluster = new vector<int>();
      cluster->push_back(pi);
      pickClusters[clustering[pi]] = cluster;
    }
    // look for the delivery cluster
    it = delivClusters.find(clustering[di]);
    if (it != delivClusters.end()) {
      cluster = it->second;
      cluster->push_back(di);
    } else {
      cluster = new vector<int>();
      cluster->push_back(di);
      delivClusters[clustering[di]] = cluster;
    }
  }
  // DEBUG
  for (it = pickClusters.begin(); it != pickClusters.end(); ++it) {
    cluster = it->second;
    cout << it->first << ": ";
    for (int i = 0; i < cluster->size(); ++i) cout << cluster->at(i) << " ";
    cout << endl;
  }
  for (it = delivClusters.begin(); it != delivClusters.end(); ++it) {
    cluster = it->second;
    cout << it->first << ": ";
    for (int i = 0; i < cluster->size(); ++i) cout << cluster->at(i) << " ";
    cout << endl;
  }
}

// adjsut the computed clusters in the constructur accordingly to the routes
// serving the customer in the cluster i.e. if if two customer are served by the
// same vehicle, the size of the cluster is reduced by one.
int RemoveCluster::execute(Solution* solution) {
  map<int, vector<int>*>::iterator it;
  vector<int>* best_cluster = nullptr;
  vector<int>* cluster = nullptr;
  int best_size = 0;

  // NOTE: if p and d are on the same cluster, likely to be served by the same
  // vehilce...

  // controls information on the performance of the operator
  local_count++;

  // pickups
  for (it = pickClusters.begin(); it != pickClusters.end(); ++it) {
    routes_cluster.clear();
    cluster = it->second;
    for (int i = 0; i < cluster->size(); ++i)
      routes_cluster.insert(solution->assignment[cluster->at(i)]);
    if (routes_cluster.size() > best_size) {
      best_size = routes_cluster.size();
      best_cluster = cluster;
    }
  }
  // delivery
  for (it = delivClusters.begin(); it != delivClusters.end(); ++it) {
    routes_cluster.clear();
    cluster = it->second;
    for (int i = 0; i < cluster->size(); ++i)
      routes_cluster.insert(solution->assignment[cluster->at(i)]);
    if (routes_cluster.size() > best_size) {
      best_size = routes_cluster.size();
      best_cluster = cluster;
    }
  }

  // DEBUG
  // cout<<"Best Cluster: ";
  // for(int i=0; i<best_cluster->size(); ++i)
  //   cout<<best_cluster->at(i)<<" ";
  // cout<<"Number of different routes: "<<best_size<<endl;

  // cout<<"removing (cluster) "<<2*best_cluster->size()<<endl;
  int e1, e2;
  Route* r;
  for (int i = 0; i < best_cluster->size(); ++i) {
    e1 = best_cluster->at(i);
    e2 = solution->customers[e1].related;
    // cout<<"remove "<<e1<<" "<<e2<<endl;
    r = solution->assignment[e1];
    assert(r != nullptr);  // DEBUG
    r->removeElement(e1);
    r = solution->assignment[e2];
    assert(r != nullptr);  // DEBUG
    r->removeElement(e2);
  }

  // remove 'empty' routes (not servig any request e.g. maybe visiting empty
  // transfer(s))
  for (int i = 0; i < solution->numRoutes; ++i)
    if (solution->routes[i]->isEmpty())
      solution->removeRoute(
          i--);  // after removal, check position i again (anoter route)
}

void RemoveCluster::updateScore(int reward) { score += reward; }
