#ifndef INSERT_BASIC_H
#define INSERT_BASIC_H

#include "Data.h"
#include "Metrics.h"
#include "Operator_base.h"
#include "Route.h"
#include "Solution.h"

#include <algorithm>
#include <queue>

using namespace std;

class InsertBasic : public ALNS_Operator {
 public:
  InsertBasic(string, vector<requestData*>*, Metric*, int,
              default_random_engine*, uniform_real_distribution<double>*,
              double);
  int execute(Solution*);
  void updateScore(int);

 private:
  int selectionOrder;
  int greedyDistance(Solution*);
  int greedyBasic(Solution*);
  int regret2(Solution*);
  int rndBest(Solution*);
  Metric* metricMatrix;
  priority_queue<pair<distanceType, int> > requests_order;
  priority_queue<insertionData> insertions_pq;
  vector<requestData*>* requests;
  vector<bool>
      insertionTracking;  // library implementation may optimize storage so that
                          // each value is stored in a single bit.
  distanceType f_change[ROUTES_PER_SOLUTION][256];  // FIXME
  vector<distanceType> bestValues;
  vector<nodeInfo> customers;  // to check modified earliest or latest times
  array<map<int, transfer_windows>, ROUTES_PER_SOLUTION> transfers;
  set<Route*> routesToUpdate;
  int inserted;
};

#endif
