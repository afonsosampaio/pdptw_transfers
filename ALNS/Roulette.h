#ifndef ROULETTE_H
#define ROULETTE_H

#include <array>
#include <iostream>
#include <random>  //C++11!
#include <vector>

using namespace std;

// TODO: if change ROUTES_PER_SOLUTION, this might need to change
#define MAX_ELEMENTS_COUNT 500

// A roulette wheel for general objects. it must have a getWeigth() method to
// retrieve the weight used in the selection
class RouletteWheel {
 private:
  default_random_engine* generator;
  uniform_real_distribution<double>* distribution;

 public:
  RouletteWheel(default_random_engine* gen,
                uniform_real_distribution<double>* dist)
      : generator(gen), distribution(dist) {}
  template <typename ITEM_TYPE>
  int weighted_choice(vector<ITEM_TYPE>&, int);

  // TODO: if change ROUTES_PER_SOLUTION,this has to change
  template <typename ITEM_TYPE>
  int weighted_choice(array<ITEM_TYPE, MAX_ELEMENTS_COUNT>&, int);  // FIXME...
};

template <typename ITEM_TYPE>
int RouletteWheel::weighted_choice(vector<ITEM_TYPE>& items, int last) {
  double total_sum = 0;
  for (int i = 0; i < last; i++) total_sum += items[i]->getWeight();

  double weight_to_target = (*distribution)(*generator);
  weight_to_target *= total_sum;
  for (int i = 0; i < last; i++) {
    weight_to_target -= items[i]->getWeight();
    if (weight_to_target <= 0) return i;
  }
  return -1;  // should not be the case!
}

template <typename ITEM_TYPE>
int RouletteWheel::weighted_choice(array<ITEM_TYPE, MAX_ELEMENTS_COUNT>& items,
                                   int last) {
  double total_sum = 0;
  for (int i = 0; i < last; i++) total_sum += items[i]->getWeight();

  double weight_to_target = (*distribution)(*generator);
  weight_to_target *= total_sum;
  for (int i = 0; i < last; i++) {
    weight_to_target -= items[i]->getWeight();
    if (weight_to_target <= 0) return i;
  }
  return -1;  // should not be the case!
}

#endif
