#include "insertion_Route.h"

InsertNewRoute::InsertNewRoute(string nm, vector<requestData*>* reqs,
                               map<int, locationsData>* locs, vector<int>* deps,
                               vector<int>* trs, Metric* met,
                               default_random_engine* gen,
                               uniform_real_distribution<double>* dis, double r)
    : ALNS_Operator(nm, gen, dis, r) {
  requests = reqs;
  depots = deps;
  transfers = trs;
  locations = locs;
  metricMatrix = met;
  insertionTracking.reserve(512);  // FIXME

  int n = requests->size();

  // insertion_pool.reserve(n);

  /*clusterize the instance*/
  distanceType dist;
  int ci, cj, c;

  int pi, pj;
  int di, dj;
  for (int i = 0; i < n; ++i) {
    pi = requests->at(i)->pickup;
    clustering[pi] = pi;
    clust_diameters[pi] = 0;
    for (int j = i + 1; j < n; ++j) {
      pj = requests->at(j)->pickup;
      dist = metricMatrix->distance(pi, pj);  // NOTE:assuming symmetry
      pickups_order.push(make_pair(dist, make_pair(pi, pj)));
    }

    di = requests->at(i)->delivery;
    clustering[di] = di;
    clust_diameters[di] = 0;
    for (int j = i + 1; j < n; ++j) {
      dj = requests->at(j)->delivery;
      dist = metricMatrix->distance(di, dj);  // NOTE:assuming symmetry
      delivery_order.push(make_pair(dist, make_pair(di, dj)));
    }
  }

  // pickups
  while (!pickups_order.empty()) {
    r = pickups_order.top().first;
    if (r <= CLUSTER_DIAMETER) {
      pi = pickups_order.top().second.first;
      pj = pickups_order.top().second.second;
      // merge if diameter is not exceeded
      ci = clustering[pi];
      cj = clustering[pj];
      if (ci != cj) {  // not in the same cluster
        c = min(ci, cj);
        cj = max(ci, cj);
        clust_diameters[c] = r < clust_diameters[c] ? clust_diameters[c] : r;
        for (int i = 0; i < n - 1; ++i) {  // merge
          pi = requests->at(i)->pickup;
          if (clustering[pj] == cj) clustering[pj] = c;
        }
      }
    }
    pickups_order.pop();
  }

  // deliveries
  while (!delivery_order.empty()) {
    r = delivery_order.top().first;
    if (r <= CLUSTER_DIAMETER) {
      di = delivery_order.top().second.first;
      dj = delivery_order.top().second.second;
      // merge if diameter is not exceeded
      ci = clustering[di];
      cj = clustering[dj];
      if (ci != cj) {  // not in the same cluster
        c = min(ci, cj);
        cj = max(ci, cj);
        clust_diameters[c] = r < clust_diameters[c] ? clust_diameters[c] : r;
        for (int i = 0; i < n - 1; ++i) {  // merge
          di = requests->at(i)->delivery;
          if (clustering[dj] == cj) clustering[dj] = c;
        }
      }
    }
    delivery_order.pop();
  }

  // build the clusters vectors
  map<int, vector<int>*>::iterator it;
  vector<int>* cluster;
  for (int i = 0; i < n; ++i) {
    pi = requests->at(i)->pickup;
    di = requests->at(i)->delivery;
    // look for the pickup cluster
    it = pickClusters.find(clustering[pi]);
    if (it != pickClusters.end()) {
      cluster = it->second;
      cluster->push_back(pi);
    } else {
      cluster = new vector<int>();
      cluster->push_back(pi);
      pickClusters[clustering[pi]] = cluster;
    }
    // look for the delivery cluster
    it = delivClusters.find(clustering[di]);
    if (it != delivClusters.end()) {
      cluster = it->second;
      cluster->push_back(di);
    } else {
      cluster = new vector<int>();
      cluster->push_back(di);
      delivClusters[clustering[di]] = cluster;
    }
  }
}

int InsertNewRoute::execute(Solution* solution) {
  // ALERT("New Route\n");
  // controls information on the performance of the operator
  local_count++;

  int status;
  // first, tries insertions using the existing transfers in the solution
  status = insertWithTransfer(solution);
  if (status < 0)  // requests still remain to be inserted
    status = insertWithoutTransfer(solution);

  return status;
}

int InsertNewRoute::insertWithTransfer(Solution* solution) {
  // ALERT("New route with transfer\n");
  int n = requests->size();
  int p, d, t, r;
  int depot;
  distanceType cost;
  bool stop = false;
  int toInsert = 0;

  assert(requests_order.empty() == true);  // DEBUG

  for (int i = 0; i < n; ++i) {
    p = requests->at(i)->pickup;
    d = requests->at(i)->delivery;
    insertionTracking[i] = true;
    // entire request is outside the routing?
    if (solution->assignment[p] == nullptr and
        solution->assignment[d] == nullptr) {
      insertionTracking[i] = false;
      // cost = metricMatrix->distance(p, d);
      // requests_order.push(make_pair(cost, i));  //TODO: decide on how to
      // select/order the requests? cout<<p<<" "<<d<<"; ";  //DEBUG
      ++toInsert;
    }
  }
  // cout<<endl;//DEBUG
  // WATCH(toInsert);

  assert(toInsert > 0);  // DEBUG

  // check the clusters and get the one with the largest number of not inserted
  // requests
  map<int, vector<int>*>::iterator it;
  vector<int>* cluster = nullptr;
  int pickups_size, deliveries_size;
  bool flag_type;  // if best set is of pickups: false (same as py code)
  bool count_cluster;

  // pickups
  pickups_size = 0;  // how many clusters have non-inserted pickups
  for (it = pickClusters.begin(); it != pickClusters.end(); ++it) {
    cluster = it->second;
    count_cluster = false;
    for (int i = 0; i < cluster->size() and !count_cluster; ++i) {
      if (solution->assignment[cluster->at(i)] ==
          nullptr)  // NOTE: not very elegant, since we already computed this
                    // above...
      {
        pickups_size++;
        count_cluster = true;
      }
    }
  }
  // deliveries
  deliveries_size = 0;
  for (it = delivClusters.begin(); it != delivClusters.end(); ++it) {
    cluster = it->second;
    count_cluster = false;
    for (int i = 0; i < cluster->size() and !count_cluster; ++i) {
      if (solution->assignment[cluster->at(i)] ==
          nullptr)  // NOTE: not very elegant, since we already computed this
                    // above...
      {
        deliveries_size++;
        count_cluster = true;
      }
    }
  }

  flag_type =
      true;  // insert deliveries in existing routes and create pickup routes
  if (pickups_size > deliveries_size)
    flag_type =
        false;  // insert pickups in existing routes and create delivery routes

  // //DEBUG
  // WATCH(pickups_size);
  // WATCH(deliveries_size);
  // insertion_pool.clear();
  // if(!flag_type)
  //   for(it=pickClusters.begin(); it!=pickClusters.end(); ++it)
  //   {
  //     cluster = it->second;
  //     for(int i=0; i<cluster->size(); ++i)
  //       if(solution->assignment[cluster->at(i)] == nullptr)
  //         insertion_pool.push_back(cluster->at(i));
  //   }
  // else
  //   for(it=delivClusters.begin(); it!=delivClusters.end(); ++it)
  //   {
  //     cluster = it->second;
  //     for(int i=0; i<cluster->size(); ++i)
  //       if(solution->assignment[cluster->at(i)] == nullptr)
  //         insertion_pool.push_back(cluster->at(i));
  //   }

  // ALERT("To insert: ");
  // for(int i =0; i<insertion_pool.size(); ++i)
  //   cout<<insertion_pool[i]<<" ";
  // cout<<endl;

  Route* route_dlv;
  Route* route_pkp;
  timeType E_dpt, L_dpt;
  timeType E_d, L_d, E_p, L_p;
  timeType earliest_at_transfer, earliest_departure;
  timeType earliest_aux;
  timeType latest_at_transfer, latest_departure;
  distanceType best_cost;
  pair<timeType, timeType> infoTransfer;
  transferIterator itT;
  int depot_newRoute, transfer_newRoute;
  bool feasible, flag_inserted, new_connectionP, new_connectionD;
  int idx_newRoutes =
      solution
          ->numRoutes;  // start inserting new routes after the current routes

  routesIterator rIT;

  if (flag_type)  // insert the deliveries and connect new routes serving the
                  // pickups
  {
    for (int re = 0; re < n; ++re) {
      if (insertionTracking[re]) continue;

      p = requests->at(re)->pickup;
      d = requests->at(re)->delivery;

      E_p = solution->instance->locations->at(p).timeWindows.getStart();
      L_p = solution->instance->locations->at(p).timeWindows.getEnd();

      // DEBUG
      // cout<<"****************************************\n";
      // cout<<p<<" "<<d<<endl;
      // insertions_dq should be empty!
      while (!insertions_dq.empty()) insertions_dq.pop();

      for (int ri = 0; ri < solution->numRoutes; ++ri) {
        route_dlv = solution->routes[ri];
        for (itT = route_dlv->first_transfer();
             itT != route_dlv->last_transfer(); ++itT) {
          t = itT->first;

          infoTransfer = route_dlv->earliestInfoTrans(t);
          earliest_departure = infoTransfer.second;

          // check whether the correspondent pickup can be inserted via the
          // selected transfer
          earliest_at_transfer = INF_TIME;
          depot_newRoute = -1;
          for (int di = 0; di < depots->size(); ++di) {
            depot = depots->at(di);
            E_dpt = locations->at(depot).timeWindows.getStart();
            L_dpt = locations->at(depot).timeWindows.getEnd();
            earliest_aux =
                max(E_p, E_dpt + metricMatrix->time(depot, p));  // from pickup
            if (earliest_aux + metricMatrix->time(p, t) +
                    metricMatrix->time(t, depot) <=
                L_dpt)  // m-p-t-m is feasible
              if (earliest_aux + metricMatrix->time(p, t) <
                  earliest_at_transfer) {  // m--p--t--m is feasible
                depot_newRoute = depot;
                earliest_at_transfer = earliest_aux + metricMatrix->time(p, t);
              }
          }
          // assert(depot_newRoute!=-1);

          // pickup route arrives (earliest) at t before (earliest) departure of
          // delivery route?
          if (earliest_at_transfer < earliest_departure &&
              depot_newRoute != -1)  // earliest departure at t does not change
                                     // with insertions after t
          {
            // TODO:
            feasible = route_dlv->evaluateDeliveryTrs(d, t, insertions_dq);
          }
        }  // transfers
      }    // routes

      if (!insertions_dq.empty())  // inserting p,d is feasible
      {
        t = insertions_dq.top().transfer;  // NOTE: assuming the insertion to be
                                           // performed is the best one
        route_dlv = insertions_dq.top().route2;
        infoTransfer = route_dlv->earliestInfoTrans(t);
        earliest_departure = infoTransfer.second;

        // check whether its possible to insert p in the new pickup routes
        flag_inserted = false;
        // insertions_pq should be empty!
        while (!insertions_pq.empty()) insertions_pq.pop();
        for (int rj = idx_newRoutes; rj < solution->numRoutes; ++rj) {
          route_pkp = solution->routes[rj];
          if (route_pkp->evaluatePickupTrs(p, t, earliest_departure,
                                           insertions_pq))
            flag_inserted = true;
        }

        if (!flag_inserted)  // new route need to be inserted!
        {
          solution->addRoute();  // NOTE: WARNING! Should only be called when
                                 // all Route objs are allocated
          route_pkp =
              solution
                  ->routes[solution->numRoutes - 1];  // last route (just added)
          route_pkp->reset();                         // RESET THE ROUTE!!!

          // create the route m-p-t-m
          earliest_at_transfer = INF_TIME;
          depot_newRoute = -1;
          for (int di = 0; di < depots->size(); ++di) {
            depot = depots->at(di);
            E_dpt = locations->at(depot).timeWindows.getStart();
            L_dpt = locations->at(depot).timeWindows.getEnd();

            earliest_aux =
                max(E_p, E_dpt + metricMatrix->time(depot, p));  // from pickup
            if (earliest_aux + metricMatrix->time(p, t) +
                    metricMatrix->time(t, depot) <=
                L_dpt)  // m-p-t-m is feasible
              if (earliest_aux + metricMatrix->time(p, t) <
                  earliest_at_transfer) {  // m--p--t--m is feasible
                depot_newRoute = depot;
                earliest_at_transfer = earliest_aux + metricMatrix->time(p, t);
              }
          }
          assert(depot_newRoute !=
                 -1);  // this should holds here, as only insertions with a
                       // feasible depot were evaluated
          // m--p--t--m
          route_pkp->setDepot(depot_newRoute, depot_newRoute);
          route_pkp->appendNode(p);
          route_pkp->appendTransfer(t);

          route_pkp->flagRoute();
          route_pkp->earliestTimes();
          route_pkp->latestTimes();

          route_dlv = insertions_dq.top().route2;  // delivery route
          assert(route_pkp != nullptr and route_dlv != nullptr);

          route_dlv->addTrsPick(route_pkp, t, *(requests->at(re)));
          route_pkp->addTrsDrop(route_dlv, t, *(requests->at(re)));

          route_dlv->insertCustomer(d, insertions_dq.top().before2,
                                    insertions_dq.top().after2);
          route_dlv->updateEarliest(d);
          route_dlv->updateLatest(d);

          route_pkp->flagRoute();
          route_dlv->flagRoute();
          route_dlv->earliestTimes();  // calls route_pkp->earliestTimes
          route_pkp->latestTimes();    // calls route_pkp->earliestTimes

          // DEBUG
          assert(route_pkp->isFeasible());
          assert(route_dlv->isFeasible());
        } else  // if(!insertions_pq.empty())
        {
          route_dlv = insertions_dq.top().route2;  // delivery route
          route_pkp = insertions_pq.top().route1;  // pickup route
          assert(route_pkp != nullptr and route_dlv != nullptr);

          if (route_pkp != route_dlv) {
            new_connectionD =
                route_dlv->addTrsPick(route_pkp, t, *(requests->at(re)));
            new_connectionP =
                route_pkp->addTrsDrop(route_dlv, t, *(requests->at(re)));

            // respect p,d insertion order (updates make sense)
            route_pkp->insertCustomer(p, insertions_pq.top().before1,
                                      insertions_pq.top().after1);
            route_pkp->updateEarliest(p);
            route_pkp->updateLatest(p);
            route_dlv->insertCustomer(d, insertions_dq.top().before2,
                                      insertions_dq.top().after2);
            route_dlv->updateEarliest(d);
            route_dlv->updateLatest(d);
            // NOTE: if p,t,d is the first connection between routes pkp and
            // dlv, pkp route does not get upddated by updateLatest(d) when time
            // does not change in the path t-d (dlv route)
            if (new_connectionP) route_dlv->updateEarliest(t);
            if (new_connectionD) route_pkp->updateLatest(t);
            // route_pkp->flagRoute();
            // route_dlv->flagRoute();
            // route_dlv->earliestTimes();   //calls route_pkp->earliestTimes
            // route_pkp->latestTimes();     //calls route_pkp->earliestTimes

            // DEBUG
            assert(route_pkp->isFeasible());
            assert(route_dlv->isFeasible());
          } else  // insertion on the same route.
          {
            // NOTE: would have been mmore elegant with insertRequest...
            route_pkp->insertCustomer(p, insertions_pq.top().before1,
                                      insertions_pq.top().after1);
            route_pkp->insertCustomer(d, insertions_dq.top().before2,
                                      insertions_dq.top().after2);
            route_pkp->updateLatest(p);
            route_pkp->updateEarliest(p);
            route_pkp->updateLatest(d);
            route_pkp->updateEarliest(
                d);  // TODO:since same route, just one update is needed

            assert(route_pkp->isFeasible());
          }
        }
        insertionTracking[re] = true;
        toInsert--;
      }  // inserting
      // cout<<"****************************************\n";
    }  // elements
  } else {
    for (int re = 0; re < n; ++re) {
      if (insertionTracking[re]) continue;

      p = requests->at(re)->pickup;
      d = requests->at(re)->delivery;

      // DEBUG
      // cout<<"##############################################\n";
      // cout<<p<<" "<<d<<endl;
      // insertions_pq should be empty!
      while (!insertions_pq.empty()) insertions_pq.pop();

      // tries to insert p,d using the actual transfers
      feasible = false;
      for (int ri = 0; ri < solution->numRoutes; ++ri) {
        route_pkp = solution->routes[ri];
        for (itT = route_pkp->first_transfer();
             itT != route_pkp->last_transfer(); ++itT) {
          t = itT->first;
          feasible = route_pkp->evaluateInsertWithTransfer(
              p, t, -1, -1, nullptr, -1, insertions_pq);
        }                          // transfers
      }                            // routes
      if (!insertions_pq.empty())  //! insertions_pq.empty() => feasible
      {
        // ALERT("Possible to serve with transfers!\n");   //DEBUG
        route_pkp = insertions_pq.top().route1;
        route_dlv = insertions_pq.top().route2;
        assert(route_pkp != nullptr and route_dlv != nullptr);

        assert(route_pkp != route_dlv);

        t = insertions_pq.top().transfer;
        route_pkp->addTrsDrop(route_dlv, t, *(requests->at(re)));
        route_dlv->addTrsPick(route_pkp, t, *(requests->at(re)));

        route_pkp->insertCustomer(p, insertions_pq.top().before1,
                                  insertions_pq.top().after1);
        route_pkp->updateEarliest(p);
        route_pkp->updateLatest(p);
        route_dlv->insertCustomer(d, insertions_pq.top().before2,
                                  insertions_pq.top().after2);
        route_dlv->updateEarliest(d);
        route_dlv->updateLatest(d);

        // route_pkp->flagRoute();
        // route_dlv->flagRoute();
        // route_dlv->earliestTimes();    //calls route_pkp->earliestTimes
        // route_pkp->latestTimes();      //calls route_pkp->earliestTimes

        insertionTracking[re] = true;
        toInsert--;

        // DEBUG
        assert(route_pkp->isFeasible());
        assert(route_dlv->isFeasible());
      } else {
        // ALERT("Need to create a new route!\n"); //DEBUG
        E_d = solution->instance->locations->at(d).timeWindows.getStart();
        L_d = solution->instance->locations->at(d).timeWindows.getEnd();

        timeType latest_departure;
        best_cost = INF_DISTANCE;
        depot_newRoute = -1;
        // evaluate the best route m-t-d-m
        for (int di = 0; di < depots->size(); ++di) {
          depot = depots->at(di);
          E_dpt = locations->at(depot).timeWindows.getStart();
          L_dpt = locations->at(depot).timeWindows.getEnd();
          for (int ti = 0; ti < transfers->size(); ++ti) {
            t = transfers->at(ti);
            earliest_at_transfer = E_dpt + metricMatrix->time(depot, t);
            earliest_departure =
                max(earliest_at_transfer + metricMatrix->time(t, d),
                    E_d);  // delivery
            if ((earliest_departure + metricMatrix->time(d, depot) > L_dpt) or
                (earliest_departure > L_d))
              continue;
            // m-t-d-m is feasible
            cost = metricMatrix->distance(depot, t) +
                   metricMatrix->distance(t, d) +
                   metricMatrix->distance(d, depot);
            if (best_cost > cost) {
              best_cost = cost;
              depot_newRoute = depot;
              transfer_newRoute = t;
            }
          }
        }
        // assert(depot_newRoute!=-1);
        while (!insertions_pkp.empty()) insertions_pkp.pop();

        if (depot_newRoute != -1) {
          // check new route
          E_dpt = locations->at(depot_newRoute).timeWindows.getStart();
          L_dpt = locations->at(depot_newRoute).timeWindows.getEnd();
          earliest_at_transfer =
              E_dpt + metricMatrix->time(depot_newRoute, transfer_newRoute);
          earliest_departure = max(
              earliest_at_transfer + metricMatrix->time(transfer_newRoute, d),
              E_d);  // delivery
          latest_departure =
              min(L_d, L_dpt - metricMatrix->time(d, depot_newRoute));
          latest_at_transfer =
              latest_departure - metricMatrix->time(transfer_newRoute, d);

          for (int ri = 0; ri < solution->numRoutes; ++ri) {
            route_pkp = solution->routes[ri];
            // pickup is inserted before transfer and earliest arrival is before
            // latest departure of delivery route
            route_pkp->evaluatePickupTrs(p, transfer_newRoute,
                                         latest_at_transfer, insertions_pkp);
          }
        }

        if (!insertions_pkp.empty()) {
          assert(depot_newRoute !=
                 -1);  // this should holds here, as only insertions with a
                       // feasible depot were evaluated

          // create the delivery route m-t-d-m
          solution->addRoute();  // NOTE: WARNING! Should only be called when
                                 // all Route objs are allocated
          route_dlv =
              solution
                  ->routes[solution->numRoutes - 1];  // last route (just added)
          route_dlv->reset();                         // RESET THE ROUTE!!!
          route_dlv->setDepot(depot_newRoute, depot_newRoute);
          route_dlv->appendTransfer(transfer_newRoute);
          // route_dlv->appendNode(d);

          route_dlv->flagRoute();
          route_dlv->earliestTimes();
          route_dlv->latestTimes();

          route_pkp = insertions_pkp.top().route1;
          assert(route_pkp != nullptr and route_dlv != nullptr);

          route_dlv->addTrsPick(route_pkp, transfer_newRoute,
                                *(requests->at(re)));
          route_pkp->addTrsDrop(route_dlv, transfer_newRoute,
                                *(requests->at(re)));

          route_pkp->insertCustomer(p, insertions_pkp.top().before1,
                                    insertions_pkp.top().after1);
          route_pkp->updateEarliest(p);
          route_pkp->updateLatest(p);
          route_dlv->insertCustomer(d, transfer_newRoute, depot_newRoute);
          route_dlv->updateEarliest(transfer_newRoute);
          route_dlv->updateLatest(d);

          // route_pkp->show(1);
          // route_dlv->show(1);
          // ALERT("--------------------------------------------------\n");

          // route_pkp->updateEarliest(p);
          // route_pkp->updateLatest(p);
          //
          // route_dlv->flagRoute();
          // route_pkp->flagRoute();
          // route_dlv->earliestTimes();     //calls route_pkp->earliestTimes
          // route_pkp->latestTimes();       //calls route_dlv->latestTimes

          insertionTracking[re] = true;
          toInsert--;

          assert(route_pkp->isFeasible());
          assert(route_dlv->isFeasible());
        }
      }
      // cout<<"##############################################\n";
    }  // requests
  }

  // tries to insert remaining requests using the newly created routes
  // ALERT("Still to insert: \n"); //DEBUG
  for (int re = 0; re < n; ++re) {
    if (insertionTracking[re]) continue;

    p = requests->at(re)->pickup;
    d = requests->at(re)->delivery;
    // cout<<p<<","<<d<<endl;  //DEBUG

    while (!insertions_pq.empty()) insertions_pq.pop();
    for (int rj = idx_newRoutes; rj < solution->numRoutes; ++rj) {
      route_pkp = solution->routes[rj];
      route_pkp->evaluateReqInsertion(p, d, insertions_pq);
    }
    // best insertion over all routes
    if (!insertions_pq.empty())  // its feasible
    {
      route_pkp = insertions_pq.top().route1;
      route_pkp->insertRequest(p, d, insertions_pq.top());
      insertionTracking[re] = true;
      toInsert--;

      // DEBUG
      assert(route_pkp->isFeasible());
    }
    // else
    //   cout<<"Insertion for "<<p<<","<<d<<" is infeasible (new pkp/dlv
    //   routes)...\n";
  }

  // ALERT("Still to insert ");  //DEBUG
  // WATCH(toInsert);
  // exit(1);

  //-----------
  // while(!requests_order.empty())
  //   requests_order.pop();

  if (toInsert > 0) return -1 * toInsert;
  return 0;
}

int InsertNewRoute::insertWithoutTransfer(Solution* solution) {
  int n = requests->size();
  Route* rp;
  Route* rd;
  int p, d, r;
  int depot;
  distanceType cost;
  bool stop = false;
  int toInsert = 0;

  assert(requests_order.empty() == true);  // DEBUG

  for (int i = 0; i < n; ++i) {
    p = requests->at(i)->pickup;
    d = requests->at(i)->delivery;
    insertionTracking[i] = true;
    // entire request is outside the routing?
    if (solution->assignment[p] == nullptr and
        solution->assignment[d] == nullptr) {
      insertionTracking[i] = false;
      cost = metricMatrix->distance(p, d);
      requests_order.push(make_pair(
          cost, i));  // TODO: decide on how to select/order the requests?
      ++toInsert;
    }
  }

  assert(toInsert > 0);  // DEBUG

  solution->addRoute();  // NOTE: WARNING! Should only be called when all Route
                         // objs are allocated
  Route* route =
      solution->routes[solution->numRoutes - 1];  // last route (just added)
  route->reset();                                 // RESET THE ROUTE!!!

  while (!stop) {
    if (requests_order.empty()) {
      stop = true;
      continue;
    }

    r = requests_order.top().second;
    p = requests->at(r)->pickup;
    d = requests->at(r)->delivery;

    // check from where (depot) to serve the request
    // TODO: on each depot, check the maximum number of vehicles

    // insertions_pq should be empty!
    while (!insertions_pq.empty()) insertions_pq.pop();
    for (int i = 0; i < solution->instance->depots->size(); ++i) {
      depot = solution->instance->depots->at(i);
      route->setDepot(depot, depot);
      route->evaluateReqInsertion(p, d, insertions_pq);
    }
    // greedy. TODO: other selection methods?
    if (!insertions_pq.empty()) {
      stop = true;
      depot = insertions_pq.top().before1;
      route->setDepot(depot, depot);
      route->insertRequest(p, d, insertions_pq.top());
      insertionTracking[r] = true;
      toInsert--;
    }
  }

  // TODO: assuming a route can always be inserted. What if a route was not
  // inserted?
  for (int i = 0; i < n; ++i) {
    p = requests->at(i)->pickup;
    d = requests->at(i)->delivery;
    // entire request is outside the routing?
    if (insertionTracking[i] == false) {
      // insertions_pq should be empty!
      while (!insertions_pq.empty()) insertions_pq.pop();
      route->evaluateReqInsertion(p, d, insertions_pq);
      if (!insertions_pq.empty()) {
        route->insertRequest(p, d, insertions_pq.top());
        insertionTracking[i] = true;  // FIXME: wrong r... check!
        toInsert--;
        assert(route->isFeasible());
      }
    }
  }
  // route->show(0);
  // ALERT("Still to insert ");  //DEBUG
  // WATCH(toInsert);
  // exit(1);

  while (!requests_order.empty()) requests_order.pop();

  if (toInsert > 0) return -1 * toInsert;
  return 0;
}

void InsertNewRoute::updateScore(int reward) { score += reward; }
