#ifndef INSERT_BASIC_TRANSFER_H
#define INSERT_BASIC_TRANSFER_H

#include "Data.h"
#include "Metrics.h"
#include "Operator_base.h"
#include "Route.h"
#include "Solution.h"
#include "insertion_Basic.h"

#include <queue>

using namespace std;

class InsertBasicTrs : public ALNS_Operator {
 public:
  // InsertBasic*,
  InsertBasicTrs(string, vector<requestData*>*, Metric*, int,
                 default_random_engine*, uniform_real_distribution<double>*,
                 double);
  int execute(Solution*);
  void updateScore(int);

 private:
  // InsertBasic* basic_insertion; //without transfers, in case *this fails
  int selectionOrder;
  int greedyDistance(Solution*);
  int regretTransfer(Solution*);
  int greedyBasic(Solution*);
  Metric* metricMatrix;
  priority_queue<pair<distanceType, int> > requests_order;
  priority_queue<insertionData> insertions_pq;
  vector<requestData*>* requests;
  // keeps track of inserted elements
  vector<bool>
      insertionTracking;  // library implementation may optimize storage so that
                          // each value is stored in a single bit.
  distanceType f_change[ROUTES_PER_SOLUTION][256];  // FIXME
  insertionData f_changeTR[256];  // FIXME (256 max number of requests)
  vector<nodeInfo> customers;     // to check modified earliest or latest times
  array<map<int, transfer_windows>, ROUTES_PER_SOLUTION> transfers;
  set<Route*> routesToUpdate;
  int inserted;
};

#endif
