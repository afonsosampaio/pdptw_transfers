#include "Metrics.h"
#include <math.h>
#include <sstream>
#include "Data.h"

Metric::Metric(problemData* d) {
  data = d;
  maxDistance = -INF_DISTANCE;
  minDistance = INF_DISTANCE;
}

void Metric::initialize() {
  ofstream matrix_file("matrix.dat");
  if (!matrix_file.good()) {
    ALERT("Could not open distance matrix!\nAborting...\n");
    exit(1);
  }

  int start, end;
  int size;
  distanceType dist;
  timeType tim;
  size = data->locations->size();
  for (auto& id1 : *(data->locations)) {
    start = id1.first;
    int start_x = id1.second.coord_x;
    int start_y = id1.second.coord_y;
    for (auto& id2 : *(data->locations)) {
      end = id2.first;
      if (start != end) {
        int end_x = id2.second.coord_x;
        int end_y = id2.second.coord_y;
        tim = TIME_SCALE *
              ceil(sqrt((start_x - end_x) * (start_x - end_x) +
                        (start_y - end_y) * (start_y - end_y)));  // round
        dist = ceil(sqrt((start_x - end_x) * (start_x - end_x) +
                         (start_y - end_y) * (start_y - end_y)));  // ceil
        timeCache[make_pair(start, end)] = tim;
        distanceCache[make_pair(start, end)] = dist;
        if (dist > maxDistance) maxDistance = dist;
        if (dist < minDistance) minDistance = dist;

        // testing
        matrix_file << dist << " ";
      } else
        matrix_file << INF_DISTANCE << " ";
    }
    matrix_file << endl;
  }
  // for(auto& x : timeCache)
  //   cout<<x.first.first<<","<<x.first.second<<":"<<x.second<<endl;
}

void Metric::initialize(string file_path) {
  vector<vector<distanceType> > aux_matrix(MAX_CLUSTERS);
  vector<vector<double> > original_matrix(MAX_CLUSTERS);
  // distanceType aux_matrix[600][600];
  // double original_matrix[600][600];

  // set every distance pari to inf
  for (int i = 0; i < MAX_CLUSTERS; ++i) {
    aux_matrix[i].resize(MAX_CLUSTERS);
    original_matrix[i].resize(MAX_CLUSTERS);
    for (int j = 0; j < MAX_CLUSTERS; ++j) {
      aux_matrix[i][j] = INF_DISTANCE;
      original_matrix[i][j] = INF_DISTANCE;
    }
  }

  cout << "Opening" << file_path.c_str() << endl;
  ifstream matrix_file(file_path.c_str());
  if (!matrix_file.good()) {
    ALERT("Could not open distance matrix!\nAborting...\n");
    exit(1);
  }

  string line;
  double value;
  int rows;
  int num_line = 0;
  int start, end;

  start = 1;
  while (getline(matrix_file, line)) {
    istringstream iss(line);
    if (num_line == 0)  // first line contains how many lines! not needed...
    {
      iss >> rows;
      num_line++;
      continue;
    }
    end = 1;
    while (iss >> value) {
      aux_matrix[start][end] = ceil(value);
      original_matrix[start][end] = value;
      // timeCache[make_pair(start,end)] = ceil(value);
      // distanceCache[make_pair(start,end)] = ceil(value);
      if (value > maxDistance && start != end) maxDistance = ceil(value);
      if (value < minDistance && start != end) minDistance = ceil(value);
      end++;
    }
    start++;
    num_line++;
  }

  matrix_file.close();

  // only distances between pickup and delivery locations
  int n = 2 * data->requests->size();
  int shift = 0;
  for (auto& id1 : *(data->locations))
    if (id1.first <= n)
      for (auto& id2 : *(data->locations))
        if (id2.first <= n && id1.first != id2.first) {
          timeCache[make_pair(id1.first, id2.first)] =
              aux_matrix[id1.first][id2.first];
          distanceCache[make_pair(id1.first, id2.first)] =
              aux_matrix[id1.first][id2.first];
          valueCache[make_pair(id1.first, id2.first)] =
              original_matrix[id1.first][id2.first];
          // cout<<id1.first<<" "<<id2.first<<endl;
        }

  // distances between pickup/delivery and transfer/depot
  for (auto& id1 : *(data->locations))
    if (id1.first <= n) {
      shift = 1;
      for (auto& tr : *(data->transfers)) {
        timeCache[make_pair(id1.first, tr)] = aux_matrix[id1.first][n + shift];
        distanceCache[make_pair(id1.first, tr)] =
            aux_matrix[id1.first][n + shift];
        timeCache[make_pair(tr, id1.first)] = aux_matrix[n + shift][id1.first];
        distanceCache[make_pair(tr, id1.first)] =
            aux_matrix[n + shift][id1.first];
        // cout<<id1.first<<","<<tr<<" "<<n+shift<<endl;
        valueCache[make_pair(id1.first, tr)] =
            original_matrix[id1.first][n + shift];
        valueCache[make_pair(tr, id1.first)] =
            original_matrix[n + shift][id1.first];
        shift++;
      }
      for (auto& dp : *(data->depots)) {
        timeCache[make_pair(id1.first, dp)] = aux_matrix[id1.first][n + shift];
        distanceCache[make_pair(id1.first, dp)] =
            aux_matrix[id1.first][n + shift];
        timeCache[make_pair(dp, id1.first)] = aux_matrix[n + shift][id1.first];
        distanceCache[make_pair(dp, id1.first)] =
            aux_matrix[n + shift][id1.first];
        // cout<<id1.first<<","<<dp<<" "<<n+shift<<endl;
        valueCache[make_pair(id1.first, dp)] =
            0.0;  // original_matrix[id1.first][n+shift];
        valueCache[make_pair(dp, id1.first)] =
            0.0;  // original_matrix[n+shift][id1.first];
        shift++;
      }
    }

  // distances bewteen transfers/depots
  int loc = n + 1;  // the first non-pickup/delivery location
  for (auto& id1 : *(data->locations))
    if (id1.first > n) {
      shift = 1;
      for (auto& tr : *(data->transfers)) {
        if (tr != id1.first) {
          timeCache[make_pair(id1.first, tr)] = aux_matrix[loc][n + shift];
          distanceCache[make_pair(id1.first, tr)] = aux_matrix[loc][n + shift];
          // cout<<id1.first<<","<<tr<<" "<<loc<<" "<<n+shift<<endl;
          valueCache[make_pair(id1.first, tr)] =
              original_matrix[loc][n + shift];
        }
        shift++;
      }
      for (auto& dp : *(data->depots)) {
        if (dp != id1.first) {
          timeCache[make_pair(id1.first, dp)] = aux_matrix[loc][n + shift];
          distanceCache[make_pair(id1.first, dp)] = aux_matrix[loc][n + shift];
          // cout<<id1.first<<","<<dp<<" "<<loc<<" "<<n+shift<<endl;
          valueCache[make_pair(id1.first, dp)] =
              original_matrix[loc][n + shift];
        }
        shift++;
      }
      loc++;
    }
  // for(auto& x : timeCache)
  //   cout<<x.first.first<<","<<x.first.second<<":"<<x.second<<endl;
  // for(auto& dp: *(data->depots))
  //   for (auto& id1: *(data->locations))
  //     if(dp!=id1.first)
  //       cout<<dp<<" "<<id1.first<<":
  //       "<<timeCache[make_pair(dp,id1.first)]<<"##"<<timeCache[make_pair(id1.first,dp)]<<endl;
  //
  // for(auto& tr: *(data->transfers))
  //   for (auto& id1: *(data->locations))
  //     if(tr!=id1.first)
  //       cout<<tr<<" "<<id1.first<<":
  //       "<<timeCache[make_pair(tr,id1.first)]<<"##"<<timeCache[make_pair(id1.first,tr)]<<endl;

  checkTriangleInequality();
}

timeType Metric::time(int node1, int node2) {
  return timeCache[make_pair(node1, node2)];
}

distanceType Metric::distance(int node1, int node2) {
  return distanceCache[make_pair(node1, node2)];
}

double Metric::matrixValue(int node1, int node2) {
  return valueCache[make_pair(node1, node2)];
}

distanceType Metric::distance(int node, int coord_x, int coord_y) {
  int node_x, node_y;
  pair<int, int> coords;

  coords = data->locations->at(node).coordinates();
  node_x = coords.first;
  node_y = coords.second;

  return ceil(sqrt((node_x - coord_x) * (node_x - coord_x) +
                   (node_y - coord_y) * (node_y - coord_y)));
}

bool Metric::checkTriangleInequality() {
  int aux1;
  int n = 2 * data->requests->size();
  for (auto& id1 : *(data->locations))
    for (auto& id2 : *(data->locations))
      if (id1.first != id2.first)
        for (auto& id3 : *(data->locations))
          if (id1.first != id2.first && id1.first != id3.first &&
              id2.first != id3.first) {
            aux1 = timeCache[make_pair(id1.first, id2.first)] +
                   timeCache[make_pair(id2.first, id3.first)];
            if (aux1 < timeCache[make_pair(id1.first, id3.first)] &&
                id1.first < 10000 && id2.first < 10000 && id3.first < 10000) {
              // cout<<id1.first<<" "<<id2.first<<" "<<id3.first<<" violate
              // triangle inequality\n";
              // cout<<timeCache[make_pair(id1.first,id2.first)]<<"
              // "<<timeCache[make_pair(id2.first,id3.first)]<<" <
              // "<<timeCache[make_pair(id1.first,id3.first)]<<endl;
              aux1 = timeCache[make_pair(id1.first, id3.first)] -
                     (timeCache[make_pair(id1.first, id2.first)] +
                      timeCache[make_pair(id2.first, id3.first)]);
              timeCache[make_pair(id1.first, id3.first)] -= aux1;
              distanceCache[make_pair(id1.first, id3.first)] -= aux1;
            }
          }
  // exit(1);
}

/*int n = 2*data->requests->size();
int r = 3;
vector<bool> v(n);
vector<int> set(n);
fill(v.begin(), v.begin() + r, true);

int nc = 0;

do{
    for (int i = 0; i < n; ++i) {
        if (v[i])
          set[(nc++)%r] = i;//  cout << (i + 1) << " ";
    }
    for(int i=0; i<3; ++i)
    {
        if(timeCache[])
    }
}while (prev_permutation(v.begin(), v.end()));*/

// template class Metric<int,int>;
