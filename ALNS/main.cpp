#include <assert.h>  //DEBUG
#include <math.h>
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <map>
#include <queue>
#include <string>
#include <vector>

// C++11
#include <chrono>
#include <random>
#include <unordered_map>

#include "Data.h"
#include "Metrics.h"
#include "Roulette.h"
#include "Route.h"
#include "Solution.h"
#include "insertion_Basic.h"
#include "insertion_BasicTransfer.h"
#include "insertion_Route.h"
#include "insertion_Transfer.h"
#include "json.h"
#include "removal_BiasTrs.h"
#include "removal_Cluster.h"
#include "removal_Random.h"
#include "removal_Route.h"
#include "removal_Worst.h"

using namespace std;
// using json = nlohmann::json;

// construct a solution where some requets are transferred
bool constructInitialSolution(double, int, Solution*, InsertNewRoute&,
                              vector<requestData*>*, map<int, locationsData>*,
                              vector<int>*, vector<int>*, Metric*);

int main(int argc, char* argv[]) {
  string instance_file;
  string result_file;
  string save_results;
  string params_file;
  string dist_matrix;
  bool json_solution = false;
  bool show_progress = false;
  bool read_matrix = false;
  unsigned int rand_seed = 0;

  // Parse command line arguments
  char param;
  while ((param = getopt(argc, argv, "f:o:p:j:r:m:bh?")) != -1) {
    switch (param) {
      case 'f':  // instance file
        instance_file = string(optarg);
        break;
      case 'o':  // store results
        save_results = string(optarg);
        break;
      case 'j':  // jsonify output
        json_solution = true;
        result_file = string(optarg);
        break;
      case 'p':  // parameters file
        params_file = string(optarg);
        break;
      case 'h':  // show help
        cout << "Usage: ./main-f [instance file] -j [output file]\n";
        exit(0);
        break;
      case 'r':
        rand_seed = atol(optarg);
        break;
      case 'b':
        show_progress = true;
        break;
      case 'm':
        dist_matrix = string(optarg);
        read_matrix = true;
        break;
      default:
        cout << ("Aborting...\n");
        exit(1);
    }
  }

  if (instance_file.empty()) {
    ALERT("Missing instance file (-f)!\nAborting...\n");
    exit(1);
  }

  if (params_file.empty()) {
    ALERT("Missing parameters file (-p)!\nAborting...\n");
    exit(1);
  }

#ifndef NDEBUG
  ALERT("RUNNING DEBUG VERSION\n");
#endif

  // obtain a seed from the system clock:
  unsigned seed;
  if (rand_seed == 0)
    seed = std::chrono::system_clock::now().time_since_epoch().count();
  else
    seed = rand_seed;
  ofstream seed_file("seeds.dat", ios::app);
  seed_file << instance_file << ", " << seed << endl;
  seed_file.close();
  default_random_engine generator(seed);
  // a single number generator is used for the entire code to help
  // reproducibility
  uniform_real_distribution<double> distribution(0.0, 1.0);
  WATCH(seed);
  WATCH(instance_file);

  RouletteWheel wheel(&generator, &distribution);

  // string conversion
  string::size_type sz;

  /*reads ALNS parameters*/
  nlohmann::json alns_parameters;
  ifstream file_handle1(params_file.c_str());  //("params.json");
  if (!file_handle1.good()) {
    ALERT("Missing parameters files (params.json)!\nAborting...\n");
    exit(1);
  }
  file_handle1 >> alns_parameters;

  /*reads the instance*/
  nlohmann::json instance_descriptor;
  // ifstream file_handle2("json/instance.json");  //TODO
  ifstream file_handle2(instance_file.c_str());
  file_handle2 >> instance_descriptor;

  map<int, requestData> requests;
  vector<requestData*> reqs_ptr;
  map<int, locationsData> locations;
  vector<int> depots;
  vector<int> transfers;

  // fill in requests information (pickup and delivery)
  // int req_key = 1;
  for (nlohmann::json::iterator it = instance_descriptor["requests"].begin();
       it != instance_descriptor["requests"].end(); ++it) {
    // string p = (*it)["p"].get<std::string>();
    // string d = (*it)["d"].get<std::string>();
    int pick = (*it)["p"]["id"];
    int deliv = (*it)["d"]["id"];
    int earliest = (*it)["p"]["l"];
    int latest = (*it)["p"]["u"];
    int service = 0;  //(*it)["p"]["s"]; //TODO: include on the json...
    int x_coord = (*it)["p"]["x"];
    int y_coord = (*it)["p"]["y"];
    locations.emplace(
        pick, locationsData(earliest, latest, service, x_coord, y_coord));
    earliest = (*it)["d"]["l"];
    latest = (*it)["d"]["u"];
    service = 0;  //(*it)["d"]["s"];   //TODO...
    x_coord = (*it)["d"]["x"];
    y_coord = (*it)["d"]["y"];
    locations.emplace(
        deliv, locationsData(earliest, latest, service, x_coord, y_coord));
    int req_key = pick;
    requests.emplace(req_key, requestData(pick, deliv));
    // req_key++;
  }

  // fill in infrastructure information (depots and transfers)
  for (nlohmann::json::iterator it = instance_descriptor["depots"].begin();
       it != instance_descriptor["depots"].end(); ++it) {
    // string dep = it.key();
    int id = (*it)["id"];
    int earliest = (*it)["l"];
    int latest = (*it)["u"];
    int x_coord = (*it)["x"];
    int y_coord = (*it)["y"];
    locations.emplace(id, locationsData(earliest, latest, 0, x_coord,
                                        y_coord));  // FIXME: service at depot?
    depots.push_back(id);
  }
  for (nlohmann::json::iterator it = instance_descriptor["transfers"].begin();
       it != instance_descriptor["transfers"].end(); ++it) {
    // string tr = it.key();
    int id = (*it)["id"];
    int earliest = 0;   //(*it)["l"];
    int latest = 1000;  //(*it)["u"];
    int service = 0;    //(*it)["s"];
    int x_coord = (*it)["x"];
    int y_coord = (*it)["y"];
    locations.emplace(
        id, locationsData(earliest, latest, service, x_coord, y_coord));
    transfers.push_back(id);
  }

  problemData instance_data(&requests, &locations, &depots, &transfers);
  // cout<<instance_data;

  // for using with the roullet wheel
  for (auto& x : requests) reqs_ptr.push_back(&(x.second));

  int numRequests = reqs_ptr.size();  // total number of requests in the
                                      // instance

  // for(int i=0; i<reqs_ptr.size(); ++i)
  //   cout<<reqs_ptr.at(i)->pickup<<" "<<reqs_ptr.at(i)->delivery<<endl;
  // exit(1);
  // for(auto& d : depots)
  //   cout<<"Depot:"<<d<<"
  //   "<<locations.at(d).timeWindows.getStart()<<","<<locations.at(d).timeWindows.getEnd()<<endl;
  // exit(1);

  // time and distance metrics
  Metric* metrics = new Metric(&instance_data);
  if (!read_matrix)
    metrics->initialize();
  else
    metrics->initialize(dist_matrix);

  // exit(1);
  /*reads a solution*/
  nlohmann::json solution_descriptor;
  // ifstream file_handle3("json/solution.json");
  // file_handle3 >> solution_descriptor;

  // solution only stores customers info i.e. p,d
  Solution* current = new Solution(&instance_data, 2 * numRequests);
  for (auto& x : requests) {  // FIXME: this is not very elegant... (at least
                              // its kept when copying solutions)
    int p = x.second.pickup;
    int d = x.second.delivery;
    current->customers[p].related = d;
    current->customers[d].related = p;
  }

  Route* new_route;
  int converted;
  // for (auto& j : solution_descriptor["routes"])
  for (auto& j : instance_descriptor["routes"]) {
    new_route = new Route();
    new_route->setSolution(current);
    new_route->setMetrics(metrics);
    // new_route->setInstance(&instance_data);

    for (auto& i : j) {
      try {
        converted = stoi(i.get<std::string>());
        if (converted < 1000) new_route->appendNode(converted);
      } catch (invalid_argument& ia) {
        converted = stoi(i.get<std::string>().substr(1));
        if (i.get<std::string>().substr(0, 1).compare("d") == 0)  // depot
        {
          converted *= 10000;
          new_route->setDepot(converted, converted);
        }
        if (i.get<std::string>().substr(0, 1).compare("t") == 0)  // transfer
        {
          converted *= 1000;
          new_route->appendTransfer(converted);
        }
      }
    }
    current->addRoute(new_route);
  }
  // fill in remaining with empty routes
  for (int i = current->numRoutes; i < ROUTES_PER_SOLUTION; ++i) {
    current->routes[i] = new Route();
    current->routes[i]->setSolution(current);
    current->routes[i]->setMetrics(metrics);
    // current->routes[i]->setInstance(&instance_data);
  }
  //--------------------------
  // get transfers in the read solution
  for (nlohmann::json::iterator it = instance_descriptor["operations"].begin();
       it != instance_descriptor["operations"].end(); ++it) {
    // cout<<"Transfer "<<it.key();
    converted = 1000 * stoi(it.key().substr(1));
    for (auto& i : *it) {
      int p = requests[i].pickup;
      int d = requests[i].delivery;
      // p---t---d
      Route* rp = current->assignment[p];
      Route* rd = current->assignment[d];
      rp->addTrsDrop(rd, converted, requests[i]);
      rd->addTrsPick(rp, converted, requests[i]);
    }
  }

  for (int i = 0; i < current->numRoutes; ++i) current->routes[i]->flagRoute();
  for (int i = 0; i < current->numRoutes; ++i) {
    current->routes[i]->earliestTimes();
    current->routes[i]->latestTimes();
  }

  double totalCost = 0;
  for (int i = 0; i < current->numRoutes; ++i) {
    totalCost += current->routes[i]->getCost();
    // cout<<"#"<<i<<":";
    // current->routes[i]->show(1);
  }
  current->totalCost = totalCost;
  cout << "Initial solution cost: " << totalCost << endl;
  cout << "Initial solution fleet: " << current->numRoutes << endl;
  cout << "------------------------------------------\n";
  for (int i = 0; i < current->numRoutes; ++i)  // DEBUG
  {
    if (!current->routes[i]->isFeasible()) current->routes[i]->show(1);
    assert(current->routes[i]->isFeasible());
    assert(current->routes[i]->multipleConnections());
  }

  /********************* ALNS SETUP
   * *********************************************/
  double initial_cost = totalCost;
  double cooling = alns_parameters["sa_schedule"]["cooling"];
  double temperature = alns_parameters["sa_schedule"]["T0"];
  double worse = alns_parameters["sa_schedule"]["worse_initial"];
  double noise_rate = alns_parameters["sa_schedule"]["noise_rate"];
  double reaction = alns_parameters["sa_schedule"]["reaction"];
  int max_iterations = alns_parameters["sa_schedule"]["max_iter"];
  int level_lenght =
      alns_parameters["sa_schedule"]
                     ["length"];  // how often update T (and operators)
  int max_no_improvement = alns_parameters["sa_schedule"]["max_no_improvement"];

  int incumbent_reward = alns_parameters["op_rewards"]["incumbent"];
  int new_local_improved = alns_parameters["op_rewards"]["improve"];
  int new_local_worse = alns_parameters["op_rewards"]["unvisited"];

  double min_removal = alns_parameters["removal_size"]["min"];
  double max_removal = alns_parameters["removal_size"]["max"];
  int max_empty_iterations = alns_parameters["max_empty_transfers"];

  //****************REMOVAL OPERATORS*******************
  vector<ALNS_Operator*> removalOperators;
  RemoveRandom random_removal("rndRem", &reqs_ptr, min_removal, max_removal,
                              &generator, &distribution, reaction);
  random_removal.setWeight(1.0);
  if (alns_parameters["removal_operators"]["random"] == 1)
    removalOperators.push_back(&random_removal);

  RemoveWorst worst_removal("worstRem", &reqs_ptr, min_removal, max_removal,
                            &generator, &distribution, reaction);
  worst_removal.setWeight(1.0);
  if (alns_parameters["removal_operators"]["worst"] == 1)
    removalOperators.push_back(&worst_removal);

  RemoveRoute route_removal("routeRem", &reqs_ptr, min_removal, max_removal,
                            &generator, &distribution, reaction);
  route_removal.setWeight(1.0);
  if (alns_parameters["removal_operators"]["route"] == 1)
    removalOperators.push_back(&route_removal);

  RemoveBiasTransfer biasedTrs_removal("biasRem", &reqs_ptr, min_removal,
                                       max_removal, &generator, &distribution,
                                       reaction);
  biasedTrs_removal.setWeight(1.0);
  if (alns_parameters["removal_operators"]["biased"] == 1)
    removalOperators.push_back(&biasedTrs_removal);

  RemoveCluster cluster_removal("clusterRem", &reqs_ptr, metrics, &generator,
                                &distribution, reaction);
  cluster_removal.setWeight(1.0);
  if (alns_parameters["removal_operators"]["cluster"] == 1)
    removalOperators.push_back(&cluster_removal);

  //****************INSERTION OPERATORS********************
  // should define one per selection method
  vector<ALNS_Operator*> NTinsertionOperators;
  vector<ALNS_Operator*> TinsertionOperators;

  // without transfer
  InsertBasic basic_insertion_2("greedyInsNT", &reqs_ptr, metrics, GREEDY,
                                &generator, &distribution, reaction);
  basic_insertion_2.setWeight(1.0);
  if (alns_parameters["NT_insert_operators"]["greedy"] == 1)
    NTinsertionOperators.push_back(&basic_insertion_2);

  InsertBasic basic_insertion_1("distInsNT", &reqs_ptr, metrics, DISTANCE,
                                &generator, &distribution, reaction);
  basic_insertion_1.setWeight(1.0);
  if (alns_parameters["NT_insert_operators"]["dist"] == 1)
    NTinsertionOperators.push_back(&basic_insertion_1);

  InsertBasic basic_insertion_3("2rgtInsNT", &reqs_ptr, metrics, REGRET,
                                &generator, &distribution, reaction);
  basic_insertion_3.setWeight(1.0);
  if (alns_parameters["NT_insert_operators"]["2regret"] == 1)
    NTinsertionOperators.push_back(&basic_insertion_3);

  InsertBasic basic_insertion_4("rndInsNT", &reqs_ptr, metrics, BEST_RND,
                                &generator, &distribution, reaction);
  basic_insertion_4.setWeight(1.0);
  if (alns_parameters["NT_insert_operators"]["random"] == 1)
    NTinsertionOperators.push_back(&basic_insertion_4);

  // with transfer
  InsertBasicTrs basic_transfer_1("distInsT", &reqs_ptr, metrics, DISTANCE,
                                  &generator, &distribution, reaction);
  basic_transfer_1.setWeight(1.0);
  if (alns_parameters["T_insert_operators"]["dist"] == 1)
    TinsertionOperators.push_back(&basic_transfer_1);

  InsertBasicTrs basic_transfer_2("trgtInsT", &reqs_ptr, metrics, REGRET,
                                  &generator, &distribution, reaction);
  basic_transfer_2.setWeight(1.0);
  if (alns_parameters["T_insert_operators"]["tregret"] == 1)
    TinsertionOperators.push_back(&basic_transfer_2);

  InsertBasicTrs basic_transfer_3("greedyInsT", &reqs_ptr, metrics, GREEDY,
                                  &generator, &distribution, reaction);
  basic_transfer_3.setWeight(1.0);
  if (alns_parameters["T_insert_operators"]["greedy"] == 1)
    TinsertionOperators.push_back(&basic_transfer_3);

  InsertTransfer insert_transfer_1("transfInsT", &reqs_ptr, &locations, &depots,
                                   &transfers, metrics, DISTANCE, &generator,
                                   &distribution, reaction);
  insert_transfer_1.setWeight(1.0);
  if (alns_parameters["T_insert_operators"]["transfer"] == 1)
    TinsertionOperators.push_back(&insert_transfer_1);

  // insertion of new routes. NOT SELECTED BY THE WHEEL, BUT USED WHEN AN
  // INSERTION FAILS TO INSERT ALL REQS
  InsertNewRoute route_insertion("newRoute", &reqs_ptr, &locations, &depots,
                                 &transfers, metrics, &generator, &distribution,
                                 reaction);
  route_insertion.setWeight(1.0);  // not used

  //***********************************

  // if there is requests not inserted, use one of the methods
  // to decide on a transfer
  //*Initial SOLUTION
  double max_detour =
      alns_parameters["initial_transfers_construction"]["max_detour"];
  string s_method =
      alns_parameters["initial_transfers_construction"]["selection"];
  int method = 0;
  if (s_method == "balanced") method = 1;
  if (s_method == "nearest") method = 2;
  if (method == 0) {
    cout << "Undefined selection method for construction heuristic...\n";
    exit(1);
  }
  if (!current->checkAssignment()) {
    ALERT("Constructing initial solution\n");
    constructInitialSolution(max_detour, method, current, route_insertion,
                             &reqs_ptr, &locations, &depots, &transfers,
                             metrics);
    totalCost = 0;
    for (int i = 0; i < current->numRoutes; ++i) {
      totalCost += current->routes[i]->getCost();
      // cout<<"#"<<i<<":";
      // current->routes[i]->show(1);
    }
    current->totalCost = totalCost;
    cout << "New initial solution cost: " << totalCost << endl;
    cout << "New initial solution fleet: " << current->numRoutes << endl;
  }

  // at this point, all requests should be in the (current) solution
  assert(current->checkAssignment());

  /*********** Memory buffers ****************/
  // to keep track of all (unique) visited solutions (update ALNS weights)
  // key is the integer of solution cost
  unordered_multimap<int, Solution*> visited_solutions;
  // visited_solutions.reserve(1000);
  int solution_key;
  /******************************************/

  /*********** solution tracking *************/
  // used to save the current solution before modifying it
  Solution* buffer_solution = new Solution(&instance_data, 2 * numRequests);
  for (int i = 0; i < ROUTES_PER_SOLUTION; ++i) {
    buffer_solution->routes[i] = new Route();
    buffer_solution->routes[i]->setSolution(buffer_solution);
    // buffer_solution->routes[i]->setInstance(&instance_data);
    buffer_solution->routes[i]->setMetrics(metrics);
  }
  Solution* new_solution;
  Solution* incumbent;

  // save inital solution in the visted solutions pool
  new_solution = new Solution(&instance_data, 2 * numRequests);
  *new_solution = *current;
  for (int i = 0; i < current->numRoutes; ++i) {
    new_solution->addRoute(new Route());
    new_solution->routes[i]->setSolution(new_solution);
    new_solution->routes[i]->setMetrics(metrics);
    *(new_solution->routes[i]) = *(current->routes[i]);
    new_solution->routes[i]->construct();
  }
  solution_key = (int)(totalCost);
  visited_solutions.insert(
      {solution_key,
       new_solution});  // totalCost is the cost of the Initial solution
  incumbent = new_solution;

  initial_cost = incumbent->totalCost;
  if (temperature <= 0)
    temperature = initial_cost *
                  (worse / 0.6931);  //-ln(0.5), solution with cost ini*worst
                                     //has P(0.5) of being accepted

  WATCH(current);
  WATCH(new_solution);
  WATCH(temperature);
  ALERT("---------------------------------\n");
  /*******************************************/

  // solution flags
  bool accepted, improved, f_incumbent;
  double dE, energy, uni_prob, noise;
  int reward;
  /******************************************/

  // flags and aux
  bool stop_flag = false;
  int chosen_removal = 0;
  int chosen_NTinsertion = 0;
  int chosen_Tinsertion = 0;
  int pick, deliv;
  bool save_solution = true;
  bool isVisited;
  int remove_status, insert_status;
  bool feasible;
  int auxiliary;
  bool updateNT, updateT, remReqs;

  // counters
  int iterations = 0;            // global #iterations
  int count_new_sols = 0;        //# new solutions found in a segment
  int count_accepted_sols = 0;   //# accepted solutions in a segment
  int count_improved_sols = 0;   //# improved solutions in a segment
  int count_incumbent_sols = 0;  //# incumbent solutions in a segment
  int count_accepted_sols_global =
      0;  //# accepted solutions throgouth the search
  int count_improved_sols_global =
      0;  //# improved solutions throgouth the search
  int count_incumbent_sols_global =
      0;  //# incumbent solutions throgouth the search
  int no_improvement_iterations = 0;  //# elapsed iterations without improvement
  int incumbent_iteration =
      0;  // at which iteration the incumbent solution was found

  cout.unsetf(ios::floatfield);
  cout.setf(ios::fixed);
  cout.precision(2);

  /*MAIN ALNS loop with SA master level to control solution acceptance*/
  auto start = chrono::steady_clock::now();
  bool verbose = false;
  int progress = 0;

  while (stop_flag == false) {
    // if((iterations+1) % 100 == 0)
    // cout<<"Iteration: "<<iterations+1<<endl;
    // if(iterations+1>=480)
    // verbose = true;
    if (verbose) cout << "Iteration " << iterations + 1 << endl;
    for (int i = 0; i < current->numRoutes && verbose; ++i) {
      cout << "#" << i << ":";
      current->routes[i]->show(1);
    }

    if (save_solution) {  // save the current solution before exploring the
                          // neighbohood, so we can roll back if move is not
                          // accepted
      *buffer_solution = *current;  // copy totalCost
      // forward declaration of class route prevents copying routes in Solution
      // class... need to allocate and copy routes outside
      for (int i = 0; i < current->numRoutes; ++i) {
        *(buffer_solution->routes[i]) = *(current->routes[i]);
        buffer_solution->routes[i]->construct();
      }
      buffer_solution->numRoutes = current->numRoutes;
      // NOTE: (re)connect the transfers only if the solution is rejected

      assert(*buffer_solution == *current);  // DEBUG
    }

    /*Removal*/
    chosen_removal =
        wheel.weighted_choice(removalOperators, removalOperators.size());
    remove_status = removalOperators[chosen_removal]->execute(current);
    // if(verbose)
    // ALERT("to Insert:\n");
    // {
    //   WATCH(chosen_removal);
    //   current->showNotIncluded(); //DEBUG
    // }

    // eliminate eventual removed transferred requests, and recompute the new
    // connections
    for (int i = 0; i < current->numRoutes; ++i)
      current->routes[i]->pruneTransfers(max_empty_iterations);
    // remove empty routes....
    for (int i = 0; i < current->numRoutes; ++i)
      if (current->routes[i]->isEmpty())
        current->removeRoute(
            i--);  // after removal, check position i again (anoter route)

    totalCost = 0.0;
    for (int i = 0; i < current->numRoutes; ++i) {
      totalCost += current->routes[i]->getCost();
      current->routes[i]->connectTransfers();
      current->routes[i]->flagRoute();
    }
    current->totalCost = totalCost;
    for (int i = 0; i < current->numRoutes; ++i) {
      current->routes[i]->earliestTimes();
      current->routes[i]->latestTimes();
    }

    if (verbose) {
      ALERT("After removal\n");  // DEBUG
      WATCH(chosen_removal);
    }
    for (int i = 0; i < current->numRoutes && verbose; ++i) {
      cout << "#" << i << ":";
      current->routes[i]->show(1);
    }
#ifndef NDEBUG
    feasible = true;
    for (int i = 0; i < current->numRoutes; ++i)  // DEBUG
    {
      if (current->routes[i]->isFeasible() == false) {
        feasible = false;
        ALERT("Infeasible route:\n");
        current->routes[i]->show(1);
        cout << "Best so far:" << incumbent->totalCost << ";"
             << incumbent->numRoutes << endl;
      }
      assert(feasible == true);
    }
#endif

    /*Insertion*/
    chosen_NTinsertion = wheel.weighted_choice(NTinsertionOperators,
                                               NTinsertionOperators.size());
    chosen_Tinsertion =
        wheel.weighted_choice(TinsertionOperators, TinsertionOperators.size());

    assert(chosen_Tinsertion >= 0 and chosen_NTinsertion >= 0);

    updateNT = updateT = false;
    if (TinsertionOperators[chosen_Tinsertion]->getWeight() >=
        NTinsertionOperators[chosen_NTinsertion]->getWeight()) {
      insert_status = TinsertionOperators[chosen_Tinsertion]->execute(current);
      updateT = true;
      if (insert_status < 0) {
        insert_status =
            NTinsertionOperators[chosen_NTinsertion]->execute(current);
        updateNT = true;
      }
    } else {
      insert_status =
          NTinsertionOperators[chosen_NTinsertion]->execute(current);
      updateNT = true;
      if (insert_status < 0) {
        insert_status =
            TinsertionOperators[chosen_Tinsertion]->execute(current);
        updateT = true;
      }
    }

    // insert remaning requests creating new routes (with and without transfer)
    while (insert_status < 0)  // operator was not able to insert all requests
    {                          // new routes are required...
      // ALERT("INSERTING NEW ROUTE\n");
      insert_status = route_insertion.execute(current);
    }

    // compute new solution objective
    totalCost = 0.0;
    for (int i = 0; i < current->numRoutes; ++i)
      totalCost += current->routes[i]->getCost();
    current->totalCost = totalCost;

    assert(current->checkAssignment() == true);  // DEBUG

#ifndef NDEBUG
    feasible = true;
    for (int i = 0; i < current->numRoutes; ++i)  // DEBUG
    {
      if (current->routes[i]->isFeasible() == false) {
        feasible = false;
        ALERT("Infeasible route:\n");
        current->routes[i]->show(1);
      }
      if (current->routes[i]->multipleConnections() == false) {
        feasible = false;
        ALERT("Invalid route:\n");
        current->routes[i]->show(1);
      }
      assert(feasible == true);
    }
#endif

    // ALERT("After insertion\n");   //DEBUG
    // for(int i=0; i<current->numRoutes; ++i)
    // {
    //   cout<<"#"<<i<<":";
    //   current->routes[i]->show(1);
    // }

    /****** keep track of visited solutions throgouth the search*/
    solution_key = (int)(totalCost);
    auto range = visited_solutions.equal_range(solution_key);
    isVisited = false;
    f_incumbent = false;
    for (unordered_multimap<int, Solution*>::iterator it = range.first;
         it != range.second and !isVisited; ++it)
      if (*current == *(it->second)) {
        isVisited = true;
        // ALERT("Solution already visited\n");
      }
    if (!isVisited)  // store the new visited solution before exploring its
                     // neighbohood
    {
      // ALERT("New solution not visited yet\n");
      new_solution = new Solution(&instance_data, 2 * numRequests);
      *new_solution = *current;  // totalCost is correctly updated
      for (int i = 0; i < current->numRoutes; ++i) {
        new_solution->addRoute(new Route());  // numRoutes is correctly updated
        new_solution->routes[i]->setSolution(new_solution);
        // new_solution->routes[i]->setInstance(&instance_data);
        new_solution->routes[i]->setMetrics(metrics);

        *(new_solution->routes[i]) = *(current->routes[i]);
        new_solution->routes[i]->construct();
      }
      // NOTE: build transfers connections here? Not necessary, since the (new)
      // solution is not gonna be used in the search NOTE: nevertheless, if its
      // the incumbent, have to to it later

      visited_solutions.insert(
          {solution_key,
           new_solution});  // copy the pointer, not the (value) object!
      // keep track of best solution found (ptr to)
      if (new_solution->totalCost < incumbent->totalCost) {
        incumbent = new_solution;
        f_incumbent = true;
        incumbent_iteration = iterations + 1;
      }
    }

    /*********** Controls solution acceptance via a SA approach*/
    accepted = improved = false;
    dE = current->totalCost - buffer_solution->totalCost;
    energy = 0.0;  // not accepted by default
    uni_prob = distribution(generator);
    save_solution = true;

    if (dE < 0)  // it's an improving move!
    {
      improved = true;
      no_improvement_iterations = 0;
    } else {
      // no improvement...
      if (++no_improvement_iterations >=
          max_no_improvement)  // TODO: fraction on max_iterations?
      {
        temperature = incumbent->totalCost * (worse / 0.6931);
        ;
        no_improvement_iterations = 0;
        // ALERT("Temperature reseted...\n");
      }

      if (dE >= 0.001)  // not 0
      {
        energy = exp(-dE / temperature);
        // ALERT("without noise ");
        // WATCH(energy);
      } else  // 0: i.e. costs are (practically equal)
      {
        int min_d = (int)(metrics->getMinDistance() * noise_rate);
        int max_d = (int)(metrics->getMaxDistance() * noise_rate);
        noise = (max_d - min_d) * distribution(generator) + min_d;
        energy = exp(-noise / temperature);  // noise > 0
        // ALERT("with noise");
        // WATCH(energy);
      }
    }

    if (energy >= uni_prob or improved) {
      accepted = true;
      // ALERT("SOLUTION ACCTEPTED\n");
    } else  // solution is not accepted, roll back from buffer
    {
      save_solution =
          false;  // does not need to buffer the solution in the next iteration
      *current = *buffer_solution;  // WARNING! this change the whole routing
                                    // structure! (the routes)
      for (int i = 0; i < buffer_solution->numRoutes; ++i) {
        *(current->routes[i]) = *(buffer_solution->routes[i]);
        current->routes[i]->construct();
      }
      current->numRoutes = buffer_solution->numRoutes;
      current->totalCost = buffer_solution->totalCost;
      // need to reconstruct transfer connctions
      for (int i = 0; i < current->numRoutes; ++i)
        current->routes[i]->connectTransfers();

      assert(*current == *buffer_solution);  // DEBUG
      // ALERT("SOLUTION REJECTED\n");
    }

    assert(current->checkAssignment() == true);
    // TODO: assert E<=L, E>0 for all routes

    // update counters
    if (accepted) {
      count_accepted_sols++;
      count_accepted_sols_global++;
    }
    if (improved) {
      count_improved_sols++;
      count_improved_sols_global++;
    }
    if (f_incumbent) {
      count_incumbent_sols++;
      count_incumbent_sols_global++;
    }
    if (!isVisited) count_new_sols++;

    // reward operators
    if (accepted) {  // NOTE: only unvisited solutions are rewarded
                     // (Ropke,Psinger 2006)
      reward = 0;
      if (f_incumbent)  // new global solution
        reward = incumbent_reward;
      else {
        if (!isVisited and improved)  // improved solution not accepted before
          reward = new_local_improved;
        else if (!isVisited)
          reward = new_local_worse;  // worse solution not accepted before
      }
      // WARNING: the InsertTransfer operator can ONLY be called if current
      // solution did not change (i.e. was accepted), new_route cannot change
      removalOperators[chosen_removal]->updateScore(reward);
      if (updateNT)
        NTinsertionOperators[chosen_NTinsertion]->updateScore(reward);
      if (updateT) TinsertionOperators[chosen_Tinsertion]->updateScore(reward);
    }

    // operator statistics
    removalOperators[chosen_removal]->statistics(accepted, improved,
                                                 f_incumbent);
    if (updateNT)
      NTinsertionOperators[chosen_NTinsertion]->statistics(accepted, improved,
                                                           f_incumbent);
    if (updateT)
      TinsertionOperators[chosen_Tinsertion]->statistics(accepted, improved,
                                                         f_incumbent);

    // update history of transferred requests
    for (int i = 0; i < reqs_ptr.size(); ++i) {
      pick = reqs_ptr[i]->pickup;
      deliv = reqs_ptr[i]->delivery;
      if (accepted)  // TODO:improved? (more restrictive)
        if (current->assignment[pick] != current->assignment[deliv])
          reqs_ptr[i]->counter++;

      if (count_accepted_sols_global > 0)  // TODO:improved?
        reqs_ptr[i]->transfer_bias = (1.0 - ((double)(reqs_ptr[i]->counter) /
                                             count_accepted_sols_global));
    }

    // update SA and operator scores every length iterations
    if (iterations % level_lenght == 0) {
      for (int i = 0; i < removalOperators.size(); ++i)
        removalOperators[i]->updatePerformance(
            count_accepted_sols, count_improved_sols, count_incumbent_sols);
      for (int i = 0; i < NTinsertionOperators.size(); ++i)
        NTinsertionOperators[i]->updatePerformance(
            count_accepted_sols, count_improved_sols, count_incumbent_sols);
      for (int i = 0; i < TinsertionOperators.size(); ++i)
        TinsertionOperators[i]->updatePerformance(
            count_accepted_sols, count_improved_sols, count_incumbent_sols);

      // reset segment counters
      count_improved_sols = count_accepted_sols = count_incumbent_sols =
          count_new_sols = 0;
    }
    temperature = temperature * cooling;
    // cout<<"+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
    // cout<<"Temperature: "<<temperature<<" Best:"<<incumbent->totalCost<<endl;

    // progress status
    if (show_progress)
      if ((int)(100 * (iterations / (double)max_iterations)) != progress) {
        cout << '.';
        progress = (int)(100 * (iterations / (double)max_iterations));
        cout.flush();
      }

    // stop criteria
    if (++iterations >= max_iterations + 1) stop_flag = true;

    // cout<<"---------------------------------------\n";
  }  // while

  auto end = chrono::steady_clock::now();
  auto elapsed = end - start;
  // best solution
  totalCost = 0.0;

  // eliminate eventual removed transferred requests
  // recompute the new connections
  for (int i = 0; i < incumbent->numRoutes; ++i)
    incumbent->routes[i]->pruneTransfers(
        0);  // remove all ununsed transfers, ideally should not have any!

  totalCost = 0.0;
  for (int i = 0; i < incumbent->numRoutes; ++i) {
    totalCost += incumbent->routes[i]->getCost();
    incumbent->routes[i]->connectTransfers();
    incumbent->routes[i]->flagRoute();
  }
  incumbent->totalCost = totalCost;
  for (int i = 0; i < incumbent->numRoutes; ++i) {
    incumbent->routes[i]->earliestTimes();
    incumbent->routes[i]->latestTimes();
  }

  ALERT("Best Solution\n");  // DEBUG
  WATCH(incumbent->totalCost);
  WATCH(incumbent->numRoutes);
  for (int i = 0; i < incumbent->numRoutes; ++i) {
    cout << "#" << i << ":";
    incumbent->routes[i]->show(1);
  }

  // show operators statistics
  for (int i = 0; i < removalOperators.size(); ++i) {
    cout << "Removal Operator " << i << endl;
    removalOperators[i]->showKPIs();
  }
  for (int i = 0; i < NTinsertionOperators.size(); ++i) {
    cout << "Insertion Operator " << i << endl;
    NTinsertionOperators[i]->showKPIs();
  }
  for (int i = 0; i < TinsertionOperators.size(); ++i) {
    cout << "Insertion Operator " << i << endl;
    TinsertionOperators[i]->showKPIs();
  }

  // for(int i=0; i<reqs_ptr.size(); ++i)
  //   cout<<i<<" "<<reqs_ptr[i]->transfer_bias<<endl;

  /*
  ALERT("Solution tracing\n");  //DEBUG
  // for(auto it = visited_solutions.begin();  it != visited_solutions.end();
  ++it) {
  //   cout << it->first << " : " << it->second << endl;
  // }
  cout << "buckets contain:\n";
  for ( unsigned i = 0; i < visited_solutions.bucket_count(); ++i) {
    cout << "bucket #" << i << " contains:";
    for ( auto local_it = visited_solutions.begin(i); local_it!=
  visited_solutions.end(i); ++local_it ) cout << " " << local_it->first << ":"
  << local_it->second; cout << std::endl;
  }

  WATCH(current);
  WATCH(incumbent);
    */

  if (json_solution) {
    nlohmann::json j;
    ofstream file(result_file.c_str());

    j["routes"] = {};
    for (int i = 0; i < incumbent->numRoutes; ++i) {
      j["routes"]["r_" + to_string(i)] = nlohmann::json::array({});
      incumbent->routes[i]->jsonify(j["routes"]["r_" + to_string(i)],
                                    instance_descriptor);
    }
    file << j;
  }

  int total_transferred = 0;
  for (int i = 0; i < reqs_ptr.size(); ++i) {
    pick = reqs_ptr[i]->pickup;
    deliv = reqs_ptr[i]->delivery;
    if (incumbent->assignment[pick] != incumbent->assignment[deliv])
      total_transferred++;
  }
  WATCH(total_transferred);
  auxiliary = incumbent->requestsTransferred();
  WATCH(auxiliary);

  assert(total_transferred == auxiliary);

  ofstream file(save_results.c_str(), ios::app);
  file.unsetf(ios::floatfield);
  file.setf(ios::fixed);
  file.precision(2);
  file << incumbent->totalCost << ";" << incumbent->numRoutes << ";"
       << total_transferred << ";"
       << chrono::duration<double, milli>(elapsed).count() / 1000 << ";"
       << incumbent_iteration << "\n";
  for (int i = 0; i < removalOperators.size(); ++i)
    file << *(removalOperators[i]);
  for (int i = 0; i < NTinsertionOperators.size(); ++i)
    file << *(NTinsertionOperators[i]);
  for (int i = 0; i < TinsertionOperators.size(); ++i)
    file << *(TinsertionOperators[i]);
  file << endl;

  totalCost = 0.0;
  for (int i = 0; i < incumbent->numRoutes; ++i)
    totalCost += incumbent->routes[i]->getMatrixValue(metrics);
  cout << "Cost computed with matrix:" << totalCost << endl;

  return 0;
}

bool constructInitialSolution(double max_tau, int selection,
                              Solution* partial_solution,
                              InsertNewRoute& new_route_insertion,
                              vector<requestData*>* requests,
                              map<int, locationsData>* locs, vector<int>* deps,
                              vector<int>* trans, Metric* met) {
  bool all_inserted = false;
  bool flag;
  bool inserted;
  int n = requests->size();
  timeType dpt_E, dpt_L;
  timeType p_E, p_L, d_E, d_L, E_dpt, L_dpt;
  distanceType best_difference, dpt2pkp, pkp2trf, trf2dlv, dlv2dpt, pkp2dlv;
  int p, d, t, depot, best_transfer, best_depot_pkp, best_depot_dlv;
  priority_queue<insertionData> insertions_pq;
  Route* route_dlv;
  Route* route_pkp;
  transferIterator itT;
  int not_inserted = 0;

  for (int re = 0; re < n; ++re) {
    p = requests->at(re)->pickup;
    d = requests->at(re)->delivery;
    p_E = locs->at(p).timeWindows.getStart();
    p_L = locs->at(p).timeWindows.getEnd();
    d_E = locs->at(d).timeWindows.getStart();
    d_L = locs->at(d).timeWindows.getEnd();

    inserted = true;
    if (partial_solution->assignment[p] == nullptr and
        partial_solution->assignment[d] == nullptr) {  // try to use a transfer
      inserted = false;
      not_inserted++;
      best_transfer = -1;
      best_depot_pkp = -1;
      best_depot_dlv = -1;
      best_difference = INF_DISTANCE;
      for (int it = 0; it < trans->size(); ++it) {
        t = trans->at(it);

        pkp2trf = met->distance(p, t);
        trf2dlv = met->distance(t, d);
        pkp2dlv = met->distance(p, d);

        // if((pkp2trf + trf2dlv) > max_tau*pkp2dlv) //detour is too large
        //   continue;

        // check whether transferring is feasible
        flag = false;

        // check whether the correspondent pickup can be inserted via the
        // selected transfer
        timeType earliest_at_transfer = INF_TIME;
        timeType earliest_aux, earliest_departure;
        distanceType cost;
        distanceType best_cost = INF_DISTANCE;
        distanceType min_distance = -INF_DISTANCE;
        earliest_at_transfer = INF_TIME;
        int depot_pkp = -1;
        for (int di = 0; di < deps->size(); ++di) {
          depot = deps->at(di);
          E_dpt = locs->at(depot).timeWindows.getStart();
          L_dpt = locs->at(depot).timeWindows.getEnd();
          earliest_aux = max(p_E, E_dpt + met->time(depot, p));  // from pickup
          if (earliest_aux + met->time(p, t) + met->time(t, depot) <=
              L_dpt)  // m-p-t-m is feasible
            if (earliest_aux + met->time(p, t) <
                earliest_at_transfer) {  // m--p--t--m is feasible
              depot_pkp = depot;
              earliest_at_transfer = earliest_aux + met->time(p, t);
            }
        }

        int depot_dlv = -1;
        for (int di = 0; di < deps->size(); ++di) {
          depot = deps->at(di);
          E_dpt = locs->at(depot).timeWindows.getStart();
          L_dpt = locs->at(depot).timeWindows.getEnd();
          earliest_aux = max(earliest_at_transfer, E_dpt + met->time(depot, t));
          earliest_departure =
              max(earliest_aux + met->time(t, d), d_E);  // delivery

          if ((earliest_departure + met->time(d, depot) > L_dpt) or
              (earliest_departure > d_L))
            continue;
          // m-t-d-m is feasible
          cost = met->distance(depot, t) + met->distance(t, d) +
                 met->distance(d, depot);
          if (best_cost > cost) {
            best_cost = cost;
            depot_dlv = depot;
            flag = true;  // transferring via t is feasible!
          }
        }

        if (flag and selection == 1) {
          // method_1, balanced detour p-t-d]
          if (best_difference > abs(pkp2trf - trf2dlv)) {
            best_difference = abs(pkp2trf - trf2dlv);
            best_transfer = t;
            best_depot_pkp = depot_pkp;
            best_depot_dlv = depot_dlv;
          }
        }
        if (flag and selection == 2) {
          // method_2, nearest transfer
          if (min_distance < pkp2trf or min_distance < trf2dlv) {
            min_distance = pkp2trf < trf2dlv ? pkp2trf : trf2dlv;
            best_transfer = t;
            best_depot_pkp = depot_pkp;
            best_depot_dlv = depot_dlv;
          }
        }
      }

      // best_transfer holds the transfer to use
      if (best_transfer != -1) {
        // tries to insert p,d using the actual transfers
        while (!insertions_pq.empty()) insertions_pq.pop();
        flag = false;
        // ALERT("Evaluating insertion...\n");
        for (int ri = 0; ri < partial_solution->numRoutes; ++ri) {
          route_pkp = partial_solution->routes[ri];
          for (itT = route_pkp->first_transfer();
               itT != route_pkp->last_transfer(); ++itT) {
            if (itT->first == best_transfer)
              flag = route_pkp->evaluateInsertWithTransfer(
                  p, best_transfer, -1, -1, nullptr, -1, insertions_pq);
          }  // transfers
        }    // routes
        if (!insertions_pq.empty()) {
          // ALERT("Possible to serve with transfers!\n");   //DEBUG
          route_pkp = insertions_pq.top().route1;
          route_dlv = insertions_pq.top().route2;
          assert(route_pkp != nullptr and route_dlv != nullptr);

          assert(route_pkp != route_dlv);

          t = insertions_pq.top().transfer;
          route_pkp->addTrsDrop(route_dlv, t, *(requests->at(re)));
          route_dlv->addTrsPick(route_pkp, t, *(requests->at(re)));

          route_pkp->insertCustomer(p, insertions_pq.top().before1,
                                    insertions_pq.top().after1);
          route_pkp->updateEarliest(p);
          route_pkp->updateLatest(p);
          route_dlv->insertCustomer(d, insertions_pq.top().before2,
                                    insertions_pq.top().after2);
          route_dlv->updateEarliest(d);
          route_dlv->updateLatest(d);

          // DEBUG
          assert(route_pkp->isFeasible());
          assert(route_dlv->isFeasible());
        } else {  // creates m1-p-t-m1 and m2-t-d-m2
          // ALERT("Creating new routes!\n");
          partial_solution->addRoute();  // NOTE: WARNING! Should only be called
                                         // when all Route objs are allocated
          route_pkp = partial_solution->routes[partial_solution->numRoutes -
                                               1];  // last route (just added)
          route_pkp->reset();                       // RESET THE ROUTE!!!
          route_pkp->setDepot(best_depot_pkp, best_depot_pkp);
          route_pkp->appendNode(p);
          route_pkp->appendTransfer(best_transfer);

          // create the delivery route m-t-d-m
          partial_solution->addRoute();  // NOTE: WARNING! Should only be called
                                         // when all Route objs are allocated
          route_dlv = partial_solution->routes[partial_solution->numRoutes -
                                               1];  // last route (just added)
          route_dlv->reset();                       // RESET THE ROUTE!!!
          route_dlv->setDepot(best_depot_dlv, best_depot_dlv);
          route_dlv->appendTransfer(best_transfer);
          route_dlv->appendNode(d);

          route_dlv->addTrsPick(route_pkp, best_transfer, *(requests->at(re)));
          route_pkp->addTrsDrop(route_dlv, best_transfer, *(requests->at(re)));
          route_pkp->flagRoute();
          route_dlv->flagRoute();
          route_dlv->earliestTimes();  // calls route_pkp->earliestTimes
          route_pkp->latestTimes();    // calls route_pkp->earliestTimes

          // route_pkp->show(1);
          // route_dlv->show(1);
          // DEBUG
          assert(route_pkp->isFeasible());
          assert(route_dlv->isFeasible());
        }
        inserted = true;
        not_inserted--;
      }
      // else { cout<<p<<","<<d<<" not feasible to transfer\n";}
    }  // if
  }    // for

  // partial_solution->showNotIncluded();
  for (int i = 0; i < partial_solution->numRoutes; ++i) {
    cout << "#" << i << ":";
    partial_solution->routes[i]->show(1);
  }

  not_inserted *= -1;
  while (not_inserted < 0) {
    not_inserted = new_route_insertion.execute(partial_solution);
    // WATCH(not_inserted);
    // ALERT("Not included:");
    // partial_solution->showNotIncluded();
  }
}
