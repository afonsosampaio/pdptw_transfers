#include "removal_BiasTrs.h"

RemoveBiasTransfer::RemoveBiasTransfer(string nm, vector<requestData*>* reqs,
                                       double min, double max,
                                       default_random_engine* gen,
                                       uniform_real_distribution<double>* dist,
                                       double rho)
    : ALNS_Operator(nm, gen, dist, rho) {
  wheel = new RouletteWheel(gen, dist);
  requests = reqs;
  // uni_dist = dist;
  // rand_eng = gen;
  min_removal = min;
  max_removal = max;
}

int RemoveBiasTransfer::execute(Solution* solution) {
  // controls information on the performance of the operator
  local_count++;

  double rand_nun = (*uni_dist)(*rand_eng);
  int n = requests->size();
  int lower = REMOVAL_INTERVAL_MIN > int(n * min_removal)
                  ? REMOVAL_INTERVAL_MIN
                  : int(n * min_removal);  // at least % of the requests
  int upper = REMOVAL_INTERVAL_MAX > int(n * max_removal)
                  ? REMOVAL_INTERVAL_MAX
                  : int(n * max_removal);  // at most % of the requests

  int q = (upper - lower + 1) * rand_nun + lower;
  // cout<<"removing (bias) "<<q<<endl;
  for (int i = 0; i < n; ++i)
    requests->at(i)->setWeight(requests->at(i)->transfer_bias);

  int chosen;
  for (int i = 0; i < q; ++i) {
    chosen = wheel->weighted_choice(*requests, n);
    // check if there is still non-zero weight requests to be selected
    if (requests->at(chosen)->getWeight() == 0) {
      i = q;  // finishes....
      continue;
    }

    requests->at(chosen)->setWeight(0.0);  // do not select it anymore
    int p = requests->at(chosen)->pickup;
    int d = requests->at(chosen)->delivery;

    Route* r = solution->assignment[p];
    assert(r != nullptr);  // DEBUG
    r->removeElement(p);
    r = solution->assignment[d];
    assert(r != nullptr);  // DEBUG
    r->removeElement(d);
  }

  // remove 'empty' routes (not servig any request e.g. maybe visiting empty
  // transfer(s))
  for (int i = 0; i < solution->numRoutes; ++i)
    if (solution->routes[i]->isEmpty())
      solution->removeRoute(
          i--);  // after removal, check position i again (anoter route)

  return 0;  // TODO: return !=0 if not all q requests were removed
}

void RemoveBiasTransfer::updateScore(int reward) { score += reward; }
