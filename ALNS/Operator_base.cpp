#include "Operator_base.h"

void ALNS_Operator::updatePerformance(int acc, int imp, int inc) {
  global_count += local_count;
  if (local_count > 0)
    weight = (rho * score) / local_count + (1 - rho) * weight;

  local_count = 0;
  score = 0;

  total_acc += acc;
  total_imp += imp;
  total_inc += inc;

  // e.g., statistics for a segment
  if (segments % update_frequency == 0) {
    if (accepted > 0)
      get<0>(charts[i_store]) = (double)accepted / total_acc;
    else
      get<0>(charts[i_store]) = 0;
    if (improved > 0)
      get<1>(charts[i_store]) = (double)improved / total_imp;
    else
      get<1>(charts[i_store]) = 0;
    if (incumbent > 0)
      get<2>(charts[i_store]) = (double)incumbent / total_inc;
    else
      get<2>(charts[i_store]) = 0;
    get<3>(charts[i_store]) = (double)weight;
    i_store++;
    accepted = improved = incumbent = 0;
    total_acc = total_imp = total_inc = 0;
  }
  segments++;
}

// call after applying to a solution and evaluate the result solution
void ALNS_Operator::statistics(bool acc, bool imp, bool inc) {
  if (acc)  // accepted solution
    accepted++;
  if (imp)  // improved solution
    improved++;
  if (inc)  // new incumbent solution
    incumbent++;
  total_incumbent += incumbent;
  total_accepted += accepted;
  total_improved += improved;
}

void ALNS_Operator::showKPIs() {
  // cout<<"Called: "<<global_count<<" times\n";
  // cout<<"Weight: "<<weight<<endl;
  cout << name << ':' << total_accepted << ',' << total_improved << ','
       << total_incumbent << endl;
}

ostream& operator<<(std::ostream& out, ALNS_Operator& op) {
  out << op.name << ',';
  for (int i = 0; i < op.i_store - 1; ++i)
    out << get<0>(op.charts[i]) << ',' << get<1>(op.charts[i]) << ','
        << get<2>(op.charts[i]) << ',' << get<3>(op.charts[i]) << ',';
  out << get<0>(op.charts[op.i_store - 1]) << ','
      << get<1>(op.charts[op.i_store - 1]) << ','
      << get<2>(op.charts[op.i_store - 1]) << ','
      << get<3>(op.charts[op.i_store - 1]) << '\n';
  // for(int i=0; i<op.i_store-1; ++i)
  //   out<<get<0>(op.charts[i])<<',';
  // out<<get<0>(op.charts[op.i_store-1])<<'\n';
  return out;
}
