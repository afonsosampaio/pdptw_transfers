#include "removal_Random.h"

RemoveRandom::RemoveRandom(string nm, vector<requestData*>* reqs, double min,
                           double max, default_random_engine* gen,
                           uniform_real_distribution<double>* dist, double r)
    : ALNS_Operator(nm, gen, dist, r) {
  wheel = new RouletteWheel(gen, dist);
  // uni_dist = dist;
  // rand_eng = gen;
  requests = reqs;
  min_removal = min;
  max_removal = max;
}

int RemoveRandom::execute(Solution* solution) {
  // controls information on the performance of the operator
  local_count++;

  // cout<<"Remove random\n";
  double rand_nun = (*uni_dist)(*rand_eng);
  int n = requests->size();
  int lower = REMOVAL_INTERVAL_MIN > int(n * min_removal)
                  ? REMOVAL_INTERVAL_MIN
                  : int(n * min_removal);  // at least % of the requests
  int upper = REMOVAL_INTERVAL_MAX > int(n * max_removal)
                  ? REMOVAL_INTERVAL_MAX
                  : int(n * max_removal);  // at most % of the requests

  int q = (upper - lower + 1) * rand_nun + lower;
  // cout<<"removing (random)"<<q<<endl;
  for (int i = 0; i < n; ++i)
    requests->at(i)->setWeight(
        1.0);  // all requests equally likely to be selected

  int chosen;
  int p, d;
  Route* r;
  for (int i = 0; i < q; ++i) {
    chosen = wheel->weighted_choice(*requests, requests->size());
    requests->at(chosen)->setWeight(0.0);  // do not select it anymore.
    p = requests->at(chosen)->pickup;
    d = requests->at(chosen)->delivery;

    // cout<<"Removing: "<<p<<" "<<d<<endl;

    r = solution->assignment[p];
    assert(r != nullptr);  // DEBUG
    r->removeElement(p);
    r = solution->assignment[d];
    assert(r != nullptr);  // DEBUG
    r->removeElement(d);
    // solution->assignment[p] = nullptr; //removeElement already null
    // solution->assignment[d] = nullptr;
    // cout<<"Removed: "<<p<<" "<<d<<endl;
  }

  // remove 'empty' routes (not servig any request e.g. maybe visiting empty
  // transfer(s))
  for (int i = 0; i < solution->numRoutes; ++i)
    if (solution->routes[i]->isEmpty())
      solution->removeRoute(
          i--);  // after removal, check position i again (anoter route)

  return 0;  // TODO: return !=0 if not all q requests were removed
}

void RemoveRandom::updateScore(int reward) { score += reward; }
