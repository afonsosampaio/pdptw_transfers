#ifndef RANDOM_H
#define RANDOM_H

#include <iostream>  //TODO: not needed!
#include "Operator_base.h"
#include "Roulette.h"
#include "Route.h"

using namespace std;

class RemoveRandom : public ALNS_Operator {
 public:
  RemoveRandom(string, vector<requestData*>*, double, double,
               default_random_engine*, uniform_real_distribution<double>*,
               double);
  int execute(Solution*);
  void updateScore(int);

 private:
  vector<requestData*>* requests;
  RouletteWheel* wheel;
  // default_random_engine* rand_eng;
  // uniform_real_distribution<double>* uni_dist;
  double min_removal;
  double max_removal;
};

#endif
