#ifndef METRICS_H
#define METRICS_H

#include <algorithm>
#include <fstream>
#include <map>
#include "Data.h"

#define TIME_SCALE 1

using namespace std;

class Metric {
 private:
  problemData* data;
  map<pair<int, int>, timeType> timeCache;
  map<pair<int, int>, distanceType> distanceCache;
  map<pair<int, int>, double> valueCache;
  distanceType maxDistance;
  distanceType minDistance;

 public:
  Metric(problemData*);
  void initialize();
  void initialize(string);  // reads data from a distance/time matrix file
  timeType time(int, int);
  distanceType distance(int, int);
  double matrixValue(int, int);
  distanceType distance(int, int, int);
  distanceType getMaxDistance() { return maxDistance; };
  distanceType getMinDistance() { return minDistance; };

  bool checkTriangleInequality();
};

#endif
