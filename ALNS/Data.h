#ifndef DATA_H
#define DATA_H

#include <map>
#include <set>
#include <unordered_map>
#include <vector>

// #define NDEBUG //use it to disable the asserts!
#include <assert.h>  //DEBUG

#include "TimeWindows.h"

// DEBUG MACROS
#define WATCH(x) cout << #x << "=" << x << endl
#define ALERT(str) cout << str

using namespace std;

// easier than template on the route...
typedef int timeType;
typedef int distanceType;

#define INF_DISTANCE 999999
#define INF_TIME 999999
#define ROUTES_PER_SOLUTION 500  // TOO MUCH?!

#define MAX_CLUSTERS 500  // Careful with this, should be at least #requests
#define CLUSTER_DIAMETER 60

#define REMOVAL_INTERVAL_MIN 3
#define REMOVAL_INTERVAL_MAX 6

// requests selection method for insertion
#define DISTANCE 1
#define GREEDY 2
#define REGRET 3
#define BEST_RND 4

class Route;

struct nodeInfo {
  int pred;
  int succ;
  TimeWindows<timeType> service;
  timeType current_service;
  int related;  // i.e. if node is a pickup, the correspondent delivery

  nodeInfo(int p, int s) : pred(p), succ(s) {}
  nodeInfo() : pred(-1), succ(-1) {}
  void clear() {
    pred = succ = -1;
    current_service = 0;
    service.set(0, 0);
  }
  nodeInfo& operator=(const nodeInfo& info) {
    pred = info.pred;
    succ = info.succ;
    service.set(info.service.getStart(), info.service.getEnd());
    current_service = info.current_service;
    related = info.related;
  }
};

struct transferInfo {
  int pred;  // immediate predecessor
  int succ;  // immediate successor
  TimeWindows<timeType> arrival;
  TimeWindows<timeType> departure;
  // auxiliary
  TimeWindows<timeType> aux_arrival;
  TimeWindows<timeType> aux_departure;

  set<Route*> departure_routes;  // routes that wait for route *this
  set<Route*> arrival_routes;    // routes that *this wait for

  vector<int> pickups;  // i.e. pickup at the transfer (route does the delivery)
  vector<int> drops;    // i.e. drop-off at the transfer

  int emptyIterations;  // how many times the transfer remains empty (no
                        // operations being done)
  bool updated;

  transferInfo() : pred(-1), succ(-1), emptyIterations(0) {}
  void saveValues() {
    aux_arrival = arrival;
    aux_departure = departure;
  }
};

struct requestData {
  int pickup;
  int delivery;
  int q;
  double weight;
  double transfer_bias;
  int counter;
  requestData(int p, int d)
      : pickup(p), delivery(d), transfer_bias(1.0), counter(0) {}
  requestData() : pickup(-1), delivery(-1), transfer_bias(1.0), counter(0) {}
  // for using with the roullet wheel
  void setWeight(double w) { weight = w; }
  double getWeight() { return weight; }
};

struct locationsData {
  TimeWindows<timeType> timeWindows;
  timeType service_time;
  // TODO: define a template for coordinates?
  int coord_x;
  int coord_y;
  locationsData(int e, int l, int s, int x, int y)
      : service_time(s), coord_x(x), coord_y(y) {
    timeWindows.set(e, l);
  }
  pair<int, int> coordinates() { return make_pair(coord_x, coord_y); }
};

struct problemData {
  map<int, requestData>* requests;
  map<int, locationsData>* locations;
  vector<int>* depots;
  vector<int>* transfers;
  problemData(map<int, requestData>* r, map<int, locationsData>* l,
              vector<int>* d, vector<int>* t)
      : requests(r), locations(l), depots(d), transfers(t) {}
};

ostream& operator<<(std::ostream&, problemData&);

struct insertionData {
  distanceType cost;
  int before1, after1;
  int before2, after2;
  int transfer;

  Route* route1;
  Route* route2;
  insertionData()
      : cost(0),
        before1(-1),
        after1(-1),
        before2(-1),
        after2(-1),
        transfer(-1),
        route1(nullptr),
        route2(nullptr) {}
  insertionData(distanceType c, int b1, int a1, int b2, int a2, int t,
                Route* r1, Route* r2)
      : cost(c),
        before1(b1),
        after1(a1),
        before2(b2),
        after2(a2),
        transfer(t),
        route1(r1),
        route2(r2) {}
};

bool operator<(const insertionData&, const insertionData&);

// TODO: [] is O(1) (on average) for unordered_map, and O(log) for map
// but can have linear worst case...
// typedef unordered_map<int, nodeInfo>::iterator nodeIterator;
typedef map<int, transferInfo>::iterator transferIterator;
typedef set<Route*>::iterator routesIterator;
typedef tuple<timeType, timeType, timeType, timeType> transfer_windows;
#endif
