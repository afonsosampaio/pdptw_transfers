#include "removal_Route.h"

RemoveRoute::RemoveRoute(string nm, vector<requestData*>* reqs, double min,
                         double max, default_random_engine* gen,
                         uniform_real_distribution<double>* dist, double r)
    : ALNS_Operator(nm, gen, dist, r) {
  wheel = new RouletteWheel(gen, dist);
  // uni_dist = dist;
  // rand_eng = gen;
  requests = reqs;
  min_removal = min;
  max_removal = max;
}

int RemoveRoute::execute(Solution* solution) {
  // controls information on the performance of the operator
  local_count++;

  // set the weights for routes
  double w = 0.0;
  int chosen, servReqs;
  pair<timeType, timeType> info;
  timeType wait;
  Route* rp;
  Route* rd;
  int p, d;
  int n = requests->size();

  sol = solution;
  deltaRoute = sol->numRoutes;

  for (int i = 0; i < solution->numRoutes; ++i) {  //(*uni_dist)(*rand_eng);
    if (solution->routes[i]->visitsTransfer())
      w = 0.0;
    else {
      servReqs = solution->routes[i]->fullReqsServiced();
      // info = solution->routes[i]->timeLenght();
      wait = solution->routes[i]->totalWaiting();
      servReqs *= 2;  //(total nodes)
      w = ((double)wait / servReqs);
    }
    solution->routes[i]->setWeight(w);
  }

  double rand_nun = (*uni_dist)(*rand_eng);
  int lower = REMOVAL_INTERVAL_MIN > int(n * min_removal)
                  ? REMOVAL_INTERVAL_MIN
                  : int(n * min_removal);  // at least % of the requests
  int upper = REMOVAL_INTERVAL_MAX > int(n * max_removal)
                  ? REMOVAL_INTERVAL_MAX
                  : int(n * max_removal);  // at most % of the requests
  int q = (upper - lower + 1) * rand_nun + lower;
  bool stop = false;
  int totalRemoved = 0;

  // cout<<"removing "<< q <<"(random) requests on route basis \n";
  routesRemoved = 0;
  while (!stop) {
    chosen = wheel->weighted_choice(solution->routes, solution->numRoutes);
    assert(solution->routes[chosen] != nullptr);  // DEBUG
    if (solution->routes[chosen]->getWeight() > 0.0) {
      // solution->routes[chosen]->show(1);
      // WATCH(solution->routes[chosen]->getWeight());
      // cout<<"#########\n";

      // cout<<solution->routes[chosen]->fullReqsServiced()<<endl;
      totalRemoved += 2 * solution->routes[chosen]->fullReqsServiced();
      solution->routes[chosen]
          ->clear();  // assign nullptr to every customer on the route
      solution->removeRoute(chosen);
      routesRemoved++;

      // do not select it anymore
      solution->routes[chosen]->setWeight(0.0);

      // cout<<solution->routes[chosen]->fullReqsServiced()<<endl;
      if (totalRemoved >= q) stop = true;
    } else
      stop = true;
  }

  // solution->removeRoute(solution->routes[chosen]);
  // remove 'empty' routes (not servig any request e.g. maybe visiting empty
  // transfer(s))
  for (int i = 0; i < solution->numRoutes; ++i)
    if (solution->routes[i]->isEmpty()) {
      solution->removeRoute(
          i--);  // after removal, check position i again (anoter route)
      routesRemoved++;
    }

  // WATCH(routesRemoved);
  return 0;
}

void RemoveRoute::updateScore(int reward) {
  if (deltaRoute > sol->numRoutes)
    deltaRoute = 5 * (deltaRoute - sol->numRoutes);
  else
    deltaRoute = 1;
  score += reward * deltaRoute;
}

// remove pickup/delivery of requests which are transferred (i.e. in another
// route)
/*for(int i=0; i<n; ++i)
{
  p = requests->at(i)->pickup;
  d = requests->at(i)->delivery;
  rp = solution->assignment[p];
  rd = solution->assignment[d];
  if(rp==route and rd!=route)
  {
    assert(rd != nullptr); //DEBUG
    rd->removeElement(d);
    tr++;
  }
  if(rd==route and rp!=route)
  {
    assert(rp != nullptr); //DEBUG
    rp->removeElement(p);
    tr++;
  }
}*/
