#include "insertion_Transfer.h"

InsertTransfer::InsertTransfer(string nm, vector<requestData*>* reqs,
                               map<int, locationsData>* locs, vector<int>* deps,
                               vector<int>* trans, Metric* met, int sel,
                               default_random_engine* gen,
                               uniform_real_distribution<double>* dist,
                               double r)
    : ALNS_Operator(nm, gen, dist, r) {
  locations = locs;
  depots = deps;
  transfers = trans;
  requests = reqs;
  metricMatrix = met;
  selectionOrder = sel;
  insertionTracking.reserve(512);  // FIXME
  // coordinates
  pair<int, int> coords_p;
  pair<int, int> coords_d;
  int centerX, centerY;
  distanceType radius, sub_path1, sub_path2;
  timeType p_E, d_L;  // earliest pickup, latest delivery
  // decide on the transfer to use for each request
  int p, d, t;
  int best_transfer;
  distanceType best_difference;
  int n = requests->size();
  for (int i = 0; i < n; ++i) {
    p = requests->at(i)->pickup;
    d = requests->at(i)->delivery;

    p_E = locations->at(p).timeWindows.getStart();
    d_L = locations->at(d).timeWindows.getEnd();

    // NOTE: radius=dist(p,d)/2... but centerX, centerY are required to check if
    // t is inside the circle.
    coords_p = locations->at(p).coordinates();
    coords_d = locations->at(d).coordinates();
    // math.ceil((locations[d]['x'] + locations[p]['x'])/2)
    centerX = ceil((coords_d.first + coords_p.first) / 2);
    centerY = ceil((coords_d.second + coords_p.second) / 2);

    radius = met->distance(p, centerX, centerY);
    // cout<<p<<" "<<d<<" "<<radius<<" "<<met->distance(p,d)<<endl;

    best_transfer = -1;
    best_difference = INF_DISTANCE;
    for (int i = 0; i < transfers->size(); ++i) {
      t = transfers->at(i);
      sub_path1 = met->distance(p, t);
      sub_path2 = met->distance(t, d);
      // TODO: if time!=distance, get the travel time for each sub_path
      if (met->distance(t, centerX, centerY) <= radius)
        if (p_E + sub_path1 + sub_path2 <= d_L)  // sub-path p--t--d is feasible
          if (best_difference > abs(sub_path1 - sub_path2)) {
            best_difference = abs(sub_path1 - sub_path2);
            best_transfer = t;
          }
    }
    best_transfers.push_back(best_transfer);
  }

  // DEBUG
  // for(int i=0; i<n; ++i)
  // {
  //   p = requests->at(i)->pickup;
  //   d = requests->at(i)->delivery;
  //
  //   cout<<p<<" "<<d<<" "<<best_transfers[i]<<endl;
  // }
}

// select requests based on the distance between p and d
// once ordered, insertion does not affect the order
int InsertTransfer::greedyDistance(Solution* solution) {
  // ALERT("InsertTransfer\n");
  int n = requests->size();
  Route* rp;
  Route* rd;
  int p, d, r;
  int depot, transfer, best_depot, best_transfer;
  distanceType cost, best_cost;
  int toInsert = 0;
  bool flag_inserted;

  // path m--p--t--d---m
  distanceType dpt2pkp, pkp2trf, trf2dlv, dlv2dpt;
  // timeType dpt2pkp, pkp2trs, trs2dlv, dlv2dpt; //TODO: optimize
  // time/distance?!
  timeType dpt_E, dpt_L;
  timeType p_E, p_L, d_E, d_L;

  for (int i = 0; i < n; ++i) {
    p = requests->at(i)->pickup;
    d = requests->at(i)->delivery;
    if (insertionTracking[i] == false) {
      cost = metricMatrix->distance(p, d);
      requests_order.push(
          make_pair(-cost, i));  // ordering by increasing distance (p,d)
      toInsert++;
    }
  }

  assert(new_route == nullptr);  // DEBUG
  flag_inserted = false;
  while (!requests_order.empty() and !flag_inserted) {
    r = requests_order.top().second;
    p = requests->at(r)->pickup;
    d = requests->at(r)->delivery;

    // insertions_pq should be empty!
    while (!insertions_pq.empty()) insertions_pq.pop();

    if (best_transfers[r] != -1)  // there is a feasible transfer
    {
      // cout<<p<<","<<d<<","<<best_transfers[r]<<endl;
      p_E = locations->at(p).timeWindows.getStart();
      p_L = locations->at(p).timeWindows.getEnd();
      d_E = locations->at(d).timeWindows.getStart();
      d_L = locations->at(d).timeWindows.getEnd();

      transfer = best_transfers[r];
      best_depot = -1;
      best_cost = INF_DISTANCE;
      for (int i = 0; i < depots->size(); ++i) {
        depot = depots->at(i);
        dpt2pkp = metricMatrix->time(depot, p);
        pkp2trf = metricMatrix->time(p, transfer);
        trf2dlv = metricMatrix->time(transfer, d);
        dlv2dpt = metricMatrix->time(d, depot);

        // check timming
        dpt_E = locations->at(depot).timeWindows.getStart();
        dpt_L = locations->at(depot).timeWindows.getEnd();

        // TODO: if time!=distance
        if (max(dpt_E + dpt2pkp, p_E) <= p_L)  // feasible from depot to pickup
          if (max(max(p_E, dpt_E + dpt2pkp) + pkp2trf + trf2dlv, d_E) <=
              d_L)  // feasible to delivery via transfer
            if (max(max(p_E, dpt_E + dpt2pkp) + pkp2trf + trf2dlv, d_E) +
                    dlv2dpt <=
                dpt_L)  // feasible returning to depot
              if (best_cost > dpt2pkp + pkp2trf + trf2dlv + dlv2dpt) {
                best_cost = dpt2pkp + pkp2trf + trf2dlv + dlv2dpt;
                best_depot = depot;
              }
      }

      if (best_depot != -1)  // feasible path m--p--t--d---m found!
      {
        // insert the new route m--p--t--d---m
        solution->addRoute();  // NOTE: WARNING! Should only be called when all
                               // Route objs are allocated
        Route* route =
            solution
                ->routes[solution->numRoutes - 1];  // last route (just added)
        route->reset();                             // RESET THE ROUTE!!!
        route->setDepot(best_depot, best_depot);
        route->appendNode(p);
        route->appendTransfer(transfer);
        route->appendNode(d);
        // timing
        route->flagRoute();
        route->earliestTimes();
        route->latestTimes();
        insertionTracking[r] = true;
        toInsert--;
        inserted++;
        flag_inserted = true;
        assert(route->isFeasible());
        new_route = route;

        // cout<<"NEW ROUTE(TRANSFER) INSERTED:"<<route<<endl;
        // new_route->show(1);  //DEBUG

        // TODO: evaluate insertions with transfer (route,route,transfer)
      }
    }
    requests_order.pop();
  }

  // WATCH(toInsert);
  return toInsert;
}

int InsertTransfer::execute(Solution* solution) {
  // controls information on the performance of the operator
  local_count++;

  int n = requests->size();
  Route* rp;
  Route* rd;
  int p, d, r;
  int toInsert;
  distanceType cost;

  assert(requests_order.empty() == true);  // DEBUG

  // intialize requests to be inserted
  toInsert = 0;
  for (int i = 0; i < n; ++i) {
    p = requests->at(i)->pickup;
    d = requests->at(i)->delivery;
    insertionTracking[i] = true;
    // entire request is outside the routing?
    if (solution->assignment[p] == nullptr and
        solution->assignment[d] == nullptr) {
      insertionTracking[i] = false;
      toInsert++;
    }
  }
  // WATCH(toInsert);

  inserted = 0;
  new_route = nullptr;

  // the order in which requests are selected for insertion
  switch (selectionOrder) {
    case DISTANCE:  // NOTE: does not need to be reordered after an insertion,
                    // so order and insert
      // ALERT("Insert Transfer\n");
      toInsert = greedyDistance(solution);
      break;
      // case GREEDY:
      //   ALERT("Greedy selection\n");
      //   toInsert = greedyBasic(solution);
      // break;
      // case REGRET:
      // break;
      // case BEST_RND:
      // break;
  }

  // clear the queue
  while (!requests_order.empty()) requests_order.pop();

  if (toInsert > 0)  // there are still requests to be inserted
    return -1 * toInsert;
  return 0;
}

void InsertTransfer::updateScore(int reward) {
  // NOTE: this is needed since the operator only insertes one request to create
  // the route p---t---d but, after, other requests might benefit from the newly
  // created route
  if (new_route != nullptr and inserted != 0)
    inserted = new_route->fullReqsServiced() + new_route->transReqsServiced();

  int multiplier = 1 * inserted;
  score += multiplier * reward;
}
