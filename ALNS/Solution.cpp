#include "Solution.h"

Solution::Solution(problemData* inst, int n) {
  instance = inst;
  size = n;  // FIXME: not needed if we have a ptr to the instance...
  assignment.reserve(n + 1);  // NOTE: not starting from 0
  earliest.reserve(n + 1);
  latest.reserve(n + 1);
  customers.reserve(n + 1);
  for (int i = 0; i <= n; i++) {
    // TODO: ids are fixed 1,n for pickups, n+1,2n for deliveries
    customers.push_back(nodeInfo());
    assignment.push_back(nullptr);
    earliest.push_back(0);
    latest.push_back(0);
  }
  // routes.reserve(10);
  for (int i = 0; i < ROUTES_PER_SOLUTION; i++) routes[i] = nullptr;
  numRoutes = 0;

  totalCost = 0.0;
}

bool Solution::checkAssignment() {
  for (int i = 1; i <= size; ++i)
    if (assignment[i] == nullptr) return false;
  return true;
}

void Solution::addRoute(Route* route) {
  assert(numRoutes < ROUTES_PER_SOLUTION);  // DEBUG
  routes[numRoutes++] = route;
}

// to use on current and buffer solutions (all routes objects are allocated)
void Solution::addRoute() {
  assert(numRoutes < ROUTES_PER_SOLUTION);  // DEBUG
  numRoutes++;
}

void Solution::removeRoute(Route* route) {
  bool stop = false;
  int idx_route = -1;

  for (int i = 0; i < numRoutes and !stop; ++i)
    if (routes[i] == route) {
      idx_route = i;
      stop = true;
    }

  if (idx_route != -1) {
    numRoutes--;
    if (numRoutes > 0) {
      routes[idx_route] = routes[numRoutes];
      routes[numRoutes] = route;  // keep it so do not need to realocate
    }
    // TODO: assign null to customers here? (call clear())
  }
}

void Solution::removeRoute(int idx) {
  assert(idx >= 0);
  Route* route = routes[idx];
  numRoutes--;
  if (numRoutes > 0) {
    routes[idx] = routes[numRoutes];
    routes[numRoutes] = route;
  }
}

void Solution::clear() {
  for (int i = 0; i <= size; i++) {
    customers[i].clear();
    earliest[i] = 0;
    latest[i] = 0;
  }
  totalCost = 0;
}

void Solution::Show() {
  nodeInfo* it;
  for (int i = 1; i <= size; i++) {
    it = &(customers[i]);
    cout << "Node " << i << " pred: " << it->pred << " succ: " << it->succ
         << " route:" << assignment[i] << endl;
  }
  cout << endl;
}

void Solution::showNotIncluded() {
  for (int i = 1; i <= size; ++i)
    if (assignment[i] == nullptr) cout << i << ",";
  cout << endl;
}

int Solution::getNotIncluded() {
  int total = 0;
  for (int i = 1; i <= size; ++i)
    if (assignment[i] == nullptr) total++;
  return total;
}

int Solution::requestsTransferred() {
  nodeInfo* it;
  int total = 0;
  for (int i = 1; i <= size; i++) {
    it = &(customers[i]);
    if (assignment[i] != assignment[it->related]) total++;
  }

  return (int)(total / 2);
}

// bool Solution::compare(Solution* s)
bool operator==(Solution& s1, Solution& s2) {
  // not the same number of transferred requests
  // if(s1.transfers.size() != s2.transfers.size())
  //   return false;

  // routing structure: compare nodes predecessors
  for (int i = 1; i <= s1.size; ++i)
    if (s1.customers[i].pred != s2.customers[i].pred) return false;
  return true;
}

Solution& Solution::operator=(Solution& sol) {
  totalCost = sol.totalCost;
  for (int i = 1; i <= size; i++) customers[i] = sol.customers[i];
}
