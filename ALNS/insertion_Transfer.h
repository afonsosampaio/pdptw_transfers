#ifndef INSERT_TRANSFER_H
#define INSERT_TRANSFER_H

#include "Data.h"
#include "Metrics.h"
#include "Operator_base.h"
#include "Route.h"
#include "Solution.h"

#include <math.h>  //ceil, abs
#include <queue>

using namespace std;

class InsertTransfer : public ALNS_Operator {
 public:
  InsertTransfer(string, vector<requestData*>*, map<int, locationsData>*,
                 vector<int>*, vector<int>*, Metric*, int,
                 default_random_engine*, uniform_real_distribution<double>*,
                 double);
  int execute(Solution*);
  void updateScore(int);

 private:
  int selectionOrder;
  int greedyDistance(Solution*);
  int greedyBasic(Solution*);

  // locations
  vector<int>* depots;
  vector<int>* transfers;
  map<int, locationsData>* locations;

  Metric* metricMatrix;
  priority_queue<pair<distanceType, int> > requests_order;
  priority_queue<insertionData> insertions_pq;

  vector<requestData*>* requests;
  vector<bool>
      insertionTracking;  // library implementation may optimize storage so that
                          // each value is stored in a single bit.
  vector<int> best_transfers;  // decide on the transfer to use for each request
                               // (use same order as requests ptr)

  distanceType f_change[ROUTES_PER_SOLUTION][256];  // FIXME
  int inserted;
  Route* new_route;
};

#endif
