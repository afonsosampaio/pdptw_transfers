import sys
import os
import argparse
import copy
import random
import json
import math
from Utils import distance
from gurobipy import *

def vehicles_range_idx(depot, order, K):
    idx = order.index(depot)
    return range(idx*K, idx*K + K)

def create_formulation(solution, requests, locations, depots, transfers, pick_to_deliv, deliv_to_pick, pd_to_request):
    try:
        model = Model("PDPTW-T")
        model.modelSense = GRB.MINIMIZE
        model.setParam('OutputFlag', 1)
        model.setParam('PreSolve', 1)
        model.setParam('MIPGapAbs', 1.0)
        model.setParam('MIPFocus', 1)
        #1-feasibility
        #2-optimality
        #3-bound
        model.setParam('IntFeasTol', 0.001)
        #model.setParam('FeasibilityTol', 0.001)
        #model.setParam('OptimalityTol', 0.001)
        
        # model.params.LazyConstraints = 1
        model.Params.TIME_LIMIT = 7200

        X = {}
        Y = {}
        T = {}
        S = {}
        A = {}
        D = {}
        N = 2*len(requests)
        K = len(solution['r'])
        nodes = {}
        cost = {}

        depots_vehicles = {x:0 for x in depots}
        depots_order = list(depots_vehicles.keys())
        for r in solution['r']:
            d = r.DepotI
            depots_vehicles[d] = depots_vehicles[d] + 1
        K = max(depots_vehicles.values())

        #vehicles vriables
        #arcs involving depots
        for dpt in depots_order:
            xdpt, ydpt = locations[dpt]['x'], locations[dpt]['y']
            for p in pick_to_deliv:
                xp, yp = locations[p]['x'], locations[p]['y']
                cost[dpt,p] = math.ceil(distance(xdpt,ydpt,xp,yp))
                for k in vehicles_range_idx(dpt, depots_order,K):
                    X[dpt,p,k] = model.addVar(vtype=GRB.BINARY, obj=cost[dpt,p], ub=1, lb=0, name='x_'+str(dpt)+','+str(p)+','+str(k))
            for d in deliv_to_pick:
                xd, yd = locations[d]['x'], locations[d]['y']
                cost[d,dpt] = math.ceil(distance(xd,yd,xdpt,ydpt))
                for k in vehicles_range_idx(dpt, depots_order,K):
                    X[d,dpt,k] = model.addVar(vtype=GRB.BINARY, obj=cost[d,dpt], ub=1, lb=0, name='x_'+str(d)+','+str(dpt)+','+str(k))
            for tr in transfers:
                xt, yt = locations[tr]['x'], locations[tr]['y']
                cost[tr,dpt] = math.ceil(distance(xt,yt,xdpt,ydpt))
                for k in vehicles_range_idx(dpt, depots_order,K):
                    X[tr,dpt,k] = model.addVar(vtype=GRB.BINARY, obj=cost[tr,dpt], ub=1, lb=0, name='x_'+str(tr)+','+str(dpt)+','+str(k))
                    X[dpt,tr,k] = model.addVar(vtype=GRB.BINARY, obj=cost[tr,dpt], ub=1, lb=0, name='x_'+str(dpt)+','+str(tr)+','+str(k))

        #arcs p,d,t
        nodes = [x for x in pick_to_deliv]
        nodes = nodes + [x for x in deliv_to_pick]
        nodes = nodes + [x for x in transfers]
        for i in nodes:
            for j in nodes:
                if i != j:
                    xi, yi = locations[i]['x'], locations[i]['y']
                    xj, yj = locations[j]['x'], locations[j]['y']
                    cost[i,j] = math.ceil(distance(xi,yi,xj,yj))
                    for k in range(0,len(depots_order)*K):
                        X[i,j,k] = model.addVar(vtype=GRB.BINARY, obj=cost[i,j], ub=1, lb=0, name='x_'+str(i)+','+str(j)+','+str(k))

        #requests flow variables
        for r in requests:
            for i in nodes:
                for j in nodes:
                    if i != j:
                        for k in range(0, len(depots_order)*K):
                            Y[i,j,k,r] = model.addVar(vtype=GRB.BINARY, obj=0, ub=1, lb=0, name='y_'+str(i)+','+str(j)+','+str(k)+','+str(r))

        #time variables
        for i in nodes:
            for k in range(0,len(depots_order)*K):
                A[i,k] = model.addVar(vtype=GRB.CONTINUOUS, obj=0, ub=999, lb=0, name='a_'+str(i)+','+str(k))
                D[i,k] = model.addVar(vtype=GRB.CONTINUOUS, obj=0, ub=999, lb=0, name='d_'+str(i)+','+str(k))
        for dpt in depots_order:
            for k in vehicles_range_idx(dpt, depots_order,K):
                A[dpt,k] = model.addVar(vtype=GRB.CONTINUOUS, obj=0, ub=999, lb=0, name='a_'+str(dpt)+','+str(k))
                D[dpt,k] = model.addVar(vtype=GRB.CONTINUOUS, obj=0, ub=999, lb=0, name='d_'+str(dpt)+','+str(k))

        #transfer variables
        for t in transfers:
            for r in requests:
                for k1 in range(0,len(depots_order)*K):
                    for k2 in range(0,len(depots_order)*K):
                        if k1!=k2:
                            S[t,r,k1,k2] = model.addVar(vtype=GRB.BINARY, obj=0, ub=1, lb=0)
        model.update()

        #initial solution:
        used_vehicles = {x:0 for x in depots}
        for dpt in depots_order:
            used_vehicles[dpt] = K*depots_order.index(dpt)
        for r in solution['r']:
            i = r.DepotI
            k = used_vehicles[i]
            used_vehicles[i] = used_vehicles[i] + 1
            for e in r.get():
                X[i,e,k].start = 1
                print(X[i,e,k])
                i = e
            X[i,r.DepotF,k].start = 1
        # print(used_vehicles)
        # quit()

        # vehicles from depots
        for dpt in depots_order:
            for k in vehicles_range_idx(dpt, depots_order,K):
                model.addConstr(quicksum(X[dpt,j,k] for j in nodes if j not in deliv_to_pick) <= 1)

        # vehicles flow
        for dpt in depots_order:
            for k in vehicles_range_idx(dpt, depots_order,K):
                model.addConstr(quicksum(X[dpt,j,k] for j in nodes if j not in deliv_to_pick) -
                                quicksum(X[j,dpt,k] for j in nodes if j not in pick_to_deliv) == 0)

        # flow at non-depot nodes i.e. P+D+T
        for dpt in depots_order:
            for k in vehicles_range_idx(dpt, depots_order,K):
                for i in pick_to_deliv:
                    model.addConstr(quicksum(X[i,j,k] for j in nodes if i!=j) - quicksum(X[j,i,k] for j in nodes+[dpt] if i!=j) == 0)
                for i in deliv_to_pick:
                    model.addConstr(quicksum(X[i,j,k] for j in nodes+[dpt] if i!=j) - quicksum(X[j,i,k] for j in nodes if i!=j) == 0)
                for i in transfers:
                    model.addConstr(quicksum(X[i,j,k] for j in nodes+[dpt] if i!=j) - quicksum(X[j,i,k] for j in nodes+[dpt] if i!=j) == 0)

        for p in pick_to_deliv:
           sum = 0
           for dpt in depots_order:
               sum = sum + quicksum(X[dpt,p,k] for k in vehicles_range_idx(dpt, depots_order, K))
           sum = sum + quicksum(X[i,p,k] for i in nodes if i!=p for k in range(0,len(depots_order)*K))
           model.addConstr(sum == 1)

        for d in deliv_to_pick:
           model.addConstr(quicksum(X[i,d,k] for i in nodes if i!=d for k in range(0,len(depots_order)*K)) == 1)

        #requests flow
        for r in requests:
            p = requests[r]['p']
            d = requests[r]['d']
            model.addConstr(quicksum(Y[p,j,k,r] for j in nodes if p!=j for k in range(0,len(depots_order)*K)) == 1)
            model.addConstr(quicksum(Y[j,d,k,r] for j in nodes if d!=j for k in range(0,len(depots_order)*K)) == 1)

            for i in nodes:
                if i!=p and i!=d and i not in transfers:
                    for k in range(0,len(depots_order)*K):
                        model.addConstr(quicksum(Y[i,j,k,r] for j in nodes if i!=j)-quicksum(Y[j,i,k,r] for j in nodes if i!=j)==0)

            for t in transfers:
                model.addConstr(quicksum(Y[t,j,k,r] for j in nodes if j!=t for k in range(0,len(depots_order)*K)) -
                                quicksum(Y[j,t,k,r] for j in nodes if j!=t for k in range(0,len(depots_order)*K)) == 0)

        # tie vehicle and request flows
        for r in requests:
            for i in nodes:
                for j in nodes:
                    if i!=j:
                        for k in range(0, len(depots_order)*K):
                            model.addConstr(Y[i,j,k,r] <= X[i,j,k])

        #time constraints
        for i in nodes:
            for j in nodes:
                if i!=j:
                    xi, yi = locations[i]['x'], locations[i]['y']
                    xj, yj = locations[j]['x'], locations[j]['y']
                    tau = int(distance(xi,yi,xj,yj))
                    for k in range(0,len(depots_order)*K):
                        model.addConstr(D[i,k]+tau-A[j,k] <= 999*(1-X[i,j,k]))
        for dpt in depots_order:
            xi, yi = locations[dpt]['x'], locations[dpt]['y']
            for j in pick_to_deliv:
                xj, yj = locations[j]['x'], locations[j]['y']
                tau = int(distance(xi,yi,xj,yj))
                for k in vehicles_range_idx(dpt, depots_order,K):
                    model.addConstr(D[dpt,k] + tau - A[j,k] <= 999*(1-X[dpt,j,k]))
            for j in deliv_to_pick:
                xj, yj = locations[j]['x'], locations[j]['y']
                tau = int(distance(xi,yi,xj,yj))
                for k in vehicles_range_idx(dpt, depots_order,K):
                    model.addConstr(D[j,k] + tau - A[dpt,k] <= 999*(1-X[j,dpt,k]))
            for t in transfers:
                xj, yj = locations[t]['x'], locations[t]['y']
                tau = int(distance(xi,yi,xj,yj))
                for k in vehicles_range_idx(dpt, depots_order,K):
                    model.addConstr(D[dpt,k] + tau - A[t,k] <= 999*(1-X[dpt,t,k]))
                    model.addConstr(D[t,k] + tau - A[dpt,k] <= 999*(1-X[t,dpt,k]))

        # time windows and pickup < delivery precedence
        for r in requests:
            p = requests[r]['p']
            d = requests[r]['d']
            for k in range(0,len(depots_order)*K):
                model.addConstr(A[p,k] <= locations[p]['u'])
                model.addConstr(A[d,k] <= locations[d]['u'])
                model.addConstr(D[p,k] >= locations[p]['l'])
                model.addConstr(D[d,k] >= locations[d]['l'])
                model.addConstr(A[p,k]<=D[p,k])
                model.addConstr(A[d,k]<=D[d,k])

        for dpt in depots_order:
            for k in vehicles_range_idx(dpt, depots_order,K):
                model.addConstr(A[dpt,k] <= locations[dpt]['u'])
                model.addConstr(D[dpt,k] == locations[dpt]['l'])

        # #transfer synchronization
        for r in requests:
            for t in transfers:
                for k1 in range(0,len(depots_order)*K):
                    model.addConstr(A[t,k1] <= D[t,k1])
                    for k2 in range(0,len(depots_order)*K):
                        if k1 != k2:
                            model.addConstr(quicksum(Y[j,t,k1,r] for j in nodes if j!=t) + quicksum(Y[t,j,k2,r] for j in nodes if j!=t) <=
                                            S[t,r,k1,k2] + 1)
                            model.addConstr(A[t,k1] - D[t,k2] <= 999*(1-S[t,r,k1,k2]))

        model.write("model.lp")
        model.optimize()    #lazySEC

        # if model.status != GRB.Status.OPTIMAL:
        print('Optimization finished with status %d' % model.status)
        #     model.computeIIS()
        #     model.write('model.ilp')

        first = 0
        for dpt in depots_order:
            for k in range(first,first+K):
                for i in pick_to_deliv:
                    if X[dpt,i,k].x > 0.5:
                        print("start ",dpt,i,k)
                        print('{0:.0f}, {1:.0f}'.format(A[dpt,k].x,D[dpt,k].x))
            first = first + K

        first = 0
        for dpt in depots_order:
            for k in range(first,first+K):
                for i in deliv_to_pick:
                    if X[i,dpt,k].x > 0.5:
                        print("end ",i,dpt,k)
            first = first + K

        for i in nodes:
            for j in nodes:
                if i!=j:
                    for k in range(0, len(depots_order)*K):
                        if X[i,j,k].x > 0.5:
                            print('{0}, {1}, {2}, {3:.0f}, {4:.0f}, {5:.0f}, {6:.0f}'.format(i,j,k,A[i,k].x,D[i,k].x,A[j,k].x,D[j,k].x))


        for r in requests:
            for i in nodes:
                for j in nodes:
                    if i != j:
                        for k in range(0, len(depots_order)*K):
                            if Y[i,j,k,r].x > 0.5:
                                print(i,j,k,r)

    except GurobiError:
        print(GurobiError.message)
