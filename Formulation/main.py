#! /usr/bin/env python3.6

import sys
import os
import argparse
import copy
import random
import time
import json
import statistics
from Exceptions import ALNSException
from Utils import exportGnuplot, exportJSON, weighted_choice, getOverlap
from Instance import *
from Route import *
from PriorityQueue import *
from formulation import *

# @profile
def main(inst_file, out_file, sol_file, tau):
    locations = {}
    requests = {}
    pick_to_deliv = {}
    deliv_to_pick = {}
    pd_to_request = {}

    #TODO: read from file
    L = 180
    depots = {'d1':{'x':30, 'y':30, 'l':0, 'u':L, 'm':50, 'i':10000},
              'd2':{'x':90, 'y':30, 'l':0, 'u':L, 'm':50, 'i':20000},
              'd3':{'x':90, 'y':90, 'l':0, 'u':L, 'm':50, 'i':30000},
              'd4':{'x':30, 'y':90, 'l':0, 'u':L, 'm':50, 'i':40000}}

    locations['d1'] = depots['d1']
    locations['d2'] = depots['d2']
    locations['d3'] = depots['d3']
    locations['d4'] = depots['d4']

    # transfers = {}
    transfers = {'t1':{'x':60, 'y':90, 's': 0, 'l':0, 'u':1000, 'i':1000},
                 't2':{'x':90, 'y':60, 's': 0, 'l':0, 'u':1000, 'i':2000},
                 't3':{'x':60, 'y':30, 's': 0, 'l':0, 'u':1000, 'i':3000},
                 't4':{'x':30, 'y':60, 's': 0, 'l':0, 'u':1000, 'i':4000},
                 't5':{'x':60, 'y':60, 's': 0, 'l':0, 'u':1000, 'i':5000}}

    locations['t1'] = transfers['t1']
    locations['t2'] = transfers['t2']
    locations['t3'] = transfers['t3']
    locations['t4'] = transfers['t4']
    locations['t5'] = transfers['t5']

    run_seed = int(time.time())
    random.seed(run_seed)
    print('running seed {0}'.format(run_seed))

    instance = Instance(requests, locations, transfers, pick_to_deliv, deliv_to_pick, pd_to_request)
    instance.read_file(inst_file)
    requests_copy = copy.deepcopy(requests)
    locations_copy = copy.deepcopy(locations)

    if tau > 0:
        max_travel_time = 0
        for r in requests:
            p = requests[r]['p']
            d = requests[r]['d']
            xp = locations[p]['x']
            yp = locations[p]['y']
            xd = locations[d]['x']
            yd = locations[d]['y']
            t_pd = int(round(distance(xp,yp,xd,yd)))
            if max_travel_time < t_pd:
                max_travel_time = t_pd

        to_remove = []
        for r in requests:
            p = requests[r]['p']
            d = requests[r]['d']
            xp = locations[p]['x']
            yp = locations[p]['y']
            xd = locations[d]['x']
            yd = locations[d]['y']
            t_pd = int(round(distance(xp,yp,xd,yd)))
            if t_pd >= tau * max_travel_time:
                to_remove.append(r)
                print('removed ',p,d)
        for r in to_remove:
            del requests[r]

    start = time.time()
    #NOTE: when solving type 23 instances, remember to change min_veh in parameters file!
    # sol_format23 = instance.createFormat23File(depots, 'instanceT23')   #NOTE:creates and solves!
    sol_format23 = instance.createSolutionDict(depots, transfers, sol_file)
    end = time.time()
    sol_format23['a'] = {}
    total = 0
    for r in sol_format23['r']:
       # total = total + len(r.get())
       r.assign(sol_format23['a'])

    total = len([i for i in sol_format23['a'] if i not in transfers])
    requests = requests_copy

    with open('results.out','a+') as file_d:
       file_d.write('{0}: {1},{2},{3},{4:.2f}\n'.format(inst_file, sol_format23['c'], len(sol_format23['r']),total,end-start))
    exportJSON(sol_format23, locations, requests, transfers, depots, inst_file)

    if(total != 2*len(requests)):
        print('WARNING: NOT ALL REQUESTS ASSGIGNED!!!\n')
        for r in requests:
            p = requests[r]['p']
            d = requests[r]['d']
            if p not in sol_format23['a'] or d not in sol_format23['a']:
                print(p,d,'NOT ASSGIGNED')
                # print(locations[p]['l'],locations[p]['u'],'\n')
                # print(locations[d]['l'],locations[d]['u'],'\n')
                # for dep in depots:
                #     xdep = depots[dep]['x']
                #     ydep = depots[dep]['y']
                #     xp = locations[p]['x']
                #     yp = locations[p]['y']
                #     xd = locations[d]['x']
                #     yd = locations[d]['y']
                #     aux1 = int(round(distance(xdep,ydep,xp,yp)))
                #     aux2 = int(round(distance(xd,yd,xdep,ydep)))
                #     t_pd = int(round(distance(xp,yp,xd,yd)))
                #     print(aux1,aux2,t_pd)
    print('exiting...')

    create_formulation(sol_format23, requests, locations, depots, transfers, pick_to_deliv, deliv_to_pick, pd_to_request)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='PDPTW instance generator')
    parser.add_argument('-f', '--file', type=str, help='Intput file', required=True)
    parser.add_argument('-o', '--output', type=str, help='Output file', required=False)
    parser.add_argument('-t', '--transfer', type=float, help='Transfer', required=True)
    parser.add_argument('-s', '--solution', type=str, help='Warm-up solution', required=False)

    #parse arguments
    args = parser.parse_args()
    # random.seed(args.see)

    main(args.file, args.output, args.solution, args.transfer)
