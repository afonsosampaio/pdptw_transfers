from Utils import distance
import math

#NOTE: maybe Vehicle...
class Route:
    # TODO: store initial and end depot...
    def __init__(self, locs, reqs, p2d, d2p, identifier):
        self.elements = []
        self.travelTime = 0
        self.travelCost = 0
        self.locations = locs
        self.requests = reqs
        self.pick_to_deliv = p2d
        self.deliv_to_pick = d2p
        self.name = identifier
        self.A = {} #arrival
        self.W = {} #waiting (departure for transfers)
        self.F = {} #forwardSlack (waiting for transfers)
        self.E = {} #earliest departure
        self.L = {} #latest departure
        self.Buff = {} #to store auxiliary values at nodes
        self.successors = {}
        self.predecessors = {}
        # store transfers in the route
        self.Transfers = {}
        self.TransfersForward = {}
        self.auxList = []
        self.updatedForward = False
        self.deps = set()
        self.arrs = set()
        self.emptyTransfer = {}
        self.DepotI = None
        self.DepotF = None

    def restat(self, locs, reqs, p2d, d2p, identifier):
        self.locations = locs
        self.requests = reqs
        self.pick_to_deliv = p2d
        self.deliv_to_pick = d2p
        self.name = identifier
        #self.Depot

    def reset(self):
        self.elements.clear()
        self.travelTime = self.travelCost = 0
        self.emptyTransfer.clear()
        self.A.clear()
        self.W.clear()
        self.F.clear()
        self.Buff.clear()
        self.successors.clear()
        self.predecessors.clear()
        self.Transfers.clear()
        self.TransfersForward.clear()
        self.deps.clear()
        self.arrs.clear()
        self.updatedForward = False
        self.DepotI = None  #initial depot
        self.DepotF = None  #final depot

    # NOTE: assuming that time windows [e,l] are feasible w.r.t the depot, so
    # leaving at l from the last customer is ok to be back in time to the depot

    '''Compute forward slack when there are no transfers in the route'''
    def forwardSlackNoTr(self):
        for e in list(reversed(self.elements)):
            succ = self.successors[e]
            if succ != None:
                self.F[e] = min(self.F[e], self.F[succ])
            else:
                tr_aux = math.ceil(distance(self.locations[e]['x'],self.locations[e]['y'],self.locations[self.DepotF]['x'],self.locations[self.DepotF]['y']))
                self.F[e] = min(self.locations[e]['u'] - self.A[e], self.locations[self.DepotF]['u']-(self.A[e]+tr_aux))

    '''Compute backward slack when there are no transfers in the route'''
    def backwardSlackNoTr(self):
        for e in self.elements:
            pred = self.predecessors[e]
            if pred != None:
                tr = math.ceil(distance(self.locations[pred]['x'],self.locations[pred]['y'],self.locations[e]['x'],self.locations[e]['y']))
                self.E[e] = max(self.locations[e]['l'], self.E[pred] + tr)
            else:
                self.E[e] = max(self.locations[e]['l'], self.A[e])

    def getForwardSlack(self, node):
        if node not in self.Transfers:
            return self.F[node]
        else:
            return self.TransfersForward[node][0], self.TransfersForward[node][1]

    def syncForwardTrns(self, assign):
        print(self.name)
        for t in self.Transfers:
            print(t,self.Transfers[t])
            for o in self.Transfers[t]:
                print(self.name, assign[o].__repr__)
        print('--------------\n')

    def dependencies(self, sol):
        self.deps.clear()# routes that depend of route self (i.e. the departure at a transfer denpends on the arrival of self)
        self.arrs.clear()# routes that self depends on (i.e. self departures at a transfer on/after the arrival of a route)

        for t in self.Transfers:
            for op in self.Transfers[t]:
                if op in self.deliv_to_pick:    #another route waits for self route
                    p = self.deliv_to_pick[op]
                    vp = sol['a'][p]    #self!
                    vd = sol['a'][op]
                    if vp!=None and vd!=None:   #should always be!
                        self.deps.add((vd,t))
                if op in self.pick_to_deliv:    #self waits for a route
                    d = self.pick_to_deliv[op]
                    vp = sol['a'][op]    #self!
                    vd = sol['a'][d]
                    if vp!=None and vd!=None:   #should always be!
                        self.arrs.add((vp,t))

    def numTransferredReqs(self):
        num = 0
        for t in self.Transfers:
            for op in self.Transfers[t]:
                num = num + 1
        return num

    def evaluateRemoval(self, node):
        pred = self.predecessors[node]
        succ = self.successors[node]
        nodeL = self.locations[node]
        succL = None
        predL = None
        rem_cost = 0

        if pred == None:
            predL = self.locations[self.DepotI]
        else:
            predL = self.locations[pred]
        if succ == None:
            succL = self.locations[self.DepotF]
        else:
            succL = self.locations[succ]

        profit = math.ceil(distance(predL['x'],predL['y'],nodeL['x'],nodeL['y'])) + math.ceil(distance(nodeL['x'],nodeL['y'],succL['x'],succL['y'])) - math.ceil(distance(predL['x'],predL['y'],succL['x'],succL['y']))

        return profit

    def evaluateInsertion(self, node):
        pos = 0
        min_detour = (-1,999999)  # TODO: differentiate between travel time and distance
        nodeL = self.locations[node]
        # evaluates insertion of nodeL before e
        for e in self.elements:
            available_slack = 0
            curr = self.locations[e]
            if e not in self.Transfers:
                available_slack = self.F[e]
            else:
                available_slack = self.TransfersForward[e][0]
            detour = 0
            if self.predecessors[e] != None:
                pred = self.locations[self.predecessors[e]]
            else:
                pred = self.locations[self.DepotI]
            detour = math.ceil(distance(pred['x'],pred['y'],nodeL['x'],nodeL['y'])) + math.ceil(distance(nodeL['x'],nodeL['y'],curr['x'],curr['y'])) - math.ceil(distance(pred['x'],pred['y'],curr['x'],curr['y']))
            # detour = detour + math.ceil(distance(nodeL['x'],nodeL['y'],curr['x'],curr['y']))

            if detour < available_slack:
                if min_detour[1] > detour:
                    min_detour = (pos,detour)
                # print('{0} can be inserted at {1} before {2}'.format(node, self.name, e))
            pos = pos + 1
        # last insertion position (@ end of the route)
        last = self.elements[-1]
        curr = self.locations[last]
        detour = math.ceil(distance(curr['x'],curr['y'],nodeL['x'],nodeL['y']))
        # TODO: service time
        tr_aux = math.ceil(distance(nodeL['x'],nodeL['y'],self.locations[self.DepotF]['x'],self.locations[self.DepotF]['y']))
        if nodeL['u'] - self.A[last] - detour >= 0:#
            if tr_aux + self.A[last] + detour <= self.locations[self.DepotF]['u']:  #coming back to depot
                if min_detour[1] > detour:
                    min_detour = (pos,detour)

        return min_detour

    # insert node2 before node1
    def evaluateInsertAt(self, node1, node2):
        nodeL = self.locations[node2]
        direct, leg1, leg2 = 0,0,0
        if node1 != None:
            curr = self.locations[node1]
            pred = None
            if self.predecessors[node1] != None:
                pred = self.locations[self.predecessors[node1]]
            else:
                pred = self.locations[self.DepotI]

            leg1 = math.ceil(distance(pred['x'],pred['y'],nodeL['x'],nodeL['y']))
            leg2 = math.ceil(distance(nodeL['x'],nodeL['y'],curr['x'],curr['y']))
            direct = math.ceil(distance(pred['x'],pred['y'],curr['x'],curr['y']))

        # insertion at the end of the route
        else:
            last = self.elements[-1]
            curr = self.locations[last]
            leg1 = math.ceil(distance(curr['x'],curr['y'],nodeL['x'],nodeL['y']))
        return leg1, leg2, direct

    def evaluateReqInsertion(self, req):
        p = self.requests[req]['p']
        d = self.requests[req]['d']
        n = len(self.elements)
        flag = False

        best_insertion = (-1,-1,999999)

        if self.DepotI != None:
            tr = math.ceil(distance(self.locations[self.DepotI]['x'],self.locations[self.DepotI]['y'],self.locations[p]['x'],self.locations[p]['y']))
            earliest = self.locations[self.DepotI]['l'] + tr
            if self.locations[p]['u'] < earliest:
                return best_insertion

        # find the last position (latest) in which p can be inserted
        latest = 0
        for i in range(0,n):
            nodei = self.elements[i]
            tr = math.ceil(distance(self.locations[nodei]['x'],self.locations[nodei]['y'],self.locations[p]['x'],self.locations[p]['y']))
            aux_time = self.A[nodei]
            if nodei in self.Transfers:
                aux_time = self.W[nodei]
            if aux_time + tr <= self.locations[p]['u']:
                latest = latest + 1

        last = min(n, latest+1)

        for i in range(0,last): #including latest
            # print('latest possible{0},{1}({2})'.format(latest+1,n,self.getName()))
            nodei = self.elements[i]
            # evaluate inserting p before i
            l1p,l2p,drp = self.evaluateInsertAt(nodei,p)

            detour_time_p = extra_time = l1p + l2p - drp

            fi = self.F[nodei]
            if nodei in self.Transfers:
                fi = self.TransfersForward[nodei][0]    #slack for the arrival
                # due to waiting, the slack for subsequent nodes is increased
                extra_time = max(0, detour_time_p-self.F[nodei])

            if detour_time_p <= fi:    # insertion is potentially feasible
                # print('inserting p @ {0}, {1}, {2}'.format(nodei, detour_time_p, self.name))
                nodej = None
                for j in range(i+1,n):
                    nodej = self.elements[j]
                    # evaluate inserting d at j
                    l1d,l2d,drd = self.evaluateInsertAt(nodej,d)
                    detour_time_d = l1d+l2d-drd

                    # check poosible arrival time is within time windows
                    pred = self.elements[j-1] #= self.predecessors[nodej]
                    arr = 0
                    if pred != None:
                        if pred in self.Transfers:
                            arr = self.W[pred]
                        else:
                            arr = self.A[pred]
                        arr = arr + l1d + extra_time
                    #else route is not feasible!

                    fj = self.F[nodej] - extra_time
                    if nodej in self.Transfers:
                        fj = self.TransfersForward[nodej][0] - extra_time
                        extra_time = max(0, extra_time-self.F[nodej])

                    if detour_time_d <= fj and arr <= self.locations[d]['u']:
                        # print('inserting d @ {0}, {1}'.format(j,self.name))
                        if best_insertion[2] > detour_time_p + detour_time_d:
                            best_insertion = (i,j,detour_time_p + detour_time_d)

                # evaluate insertion (delivery) at the end
                last = self.elements[-1]
                l1d,l2d,drd = self.evaluateInsertAt(None, d)
                detour_time_d = l1d + l2d - drd
                #check whether it is feasible to come back in time to the depot
                tr_aux = math.ceil(distance(self.locations[d]['x'],self.locations[d]['y'],self.locations[self.DepotF]['x'],self.locations[self.DepotF]['y']))
                #TODO: last is a transfer
                if self.locations[d]['u'] >= self.A[last] + extra_time + detour_time_d:
                    if tr_aux + self.A[last] + extra_time + detour_time_d <= self.locations[self.DepotF]['u']:
                        if best_insertion[2] > detour_time_p + detour_time_d:
                            best_insertion = (i,len(self.elements),detour_time_p + detour_time_d)

            # evaluate both p and d at i (i.e. o(i)->p->d->i)
            l1d,l2d,drd = self.evaluateInsertAt(nodei,d)
            dr = math.ceil(distance(self.locations[p]['x'],self.locations[p]['y'],self.locations[d]['x'],self.locations[d]['y']))
            detour_time = l1p+l2d+dr-drd
            if detour_time < fi:
                # print('P-D insertion at {0} @{1} {2}'.format(i,self.name, detour_time))
                if best_insertion[2] > detour_time:
                    best_insertion = (i,i,detour_time)

        # print('----------------------')
        return best_insertion

    def evaluateReqInsertionTransfer(self, node, transfer, offset):
        latest = 0
        transfer_pos = 99999

        if self.DepotI != None and node in self.pick_to_deliv:
            tr = math.ceil(distance(self.locations[self.DepotI]['x'],self.locations[self.DepotI]['y'],self.locations[node]['x'],self.locations[node]['y']))
            earliest = self.locations[self.DepotI]['l'] + tr
            if self.locations[node]['u'] < earliest:
                return (-1, 9999999)

        # find the last positions (latest) in which p and d can be inserted
        # find the transfer position
        for i in range(0,len(self.elements)):
            nodei = self.elements[i]
            tr = math.ceil(distance(self.locations[nodei]['x'],self.locations[nodei]['y'],
                                    self.locations[node]['x'],self.locations[node]['y']))

            # for the delivery, offset should be taken into account
            # the connecting transfer was decided before (arrival time for the pickup and departure time for delivery)
            # FIXME: update offset along the route
            aux_time = self.A[nodei]
            if nodei in self.Transfers:
                aux_time = self.W[nodei]
            if aux_time + tr + offset <= self.locations[node]['u']:
                latest = latest + 1

            if nodei == transfer:
                transfer_pos = i

        if latest < transfer_pos:
            #actually, only if it is a delivery
            #IDEA: if latest_p < tr, likely not very promissing...
            # print('transfer not possible...')
            return (-1,999999)

        min_detour = 999999
        max_slack  = 0
        best_p = -1
        best_d = -1
        waiting = 0
        if node in self.pick_to_deliv:  #its a pickup, insert before the transfer!
            # check whether pickup insertion is feasible
            for i in range(0, transfer_pos+1):
                nodei = self.elements[i]
                if nodei not in self.Transfers:
                    max_slack = max(max_slack, self.F[nodei])
                else:
                    max_slack = max(max_slack, self.TransfersForward[nodei][0])
                    max_slack = max(max_slack, self.TransfersForward[nodei][1])
                l1, l2, dr = self.evaluateInsertAt(nodei,node)
                detour = l1 + l2 - dr
                min_detour = min(min_detour, detour)
            if min_detour > max_slack:
                # print('pickup insertion not feasible')
                return (-1,999999)
            # decide on an insertion position
            wainting = 0
            min_detour = 999999
            # NOTE *
            for i in range(transfer_pos, -1, -1):
                nodei = self.elements[i]
                fi = 0
                l1, l2, dr = self.evaluateInsertAt(nodei,node)
                if nodei not in self.Transfers:
                    fi = self.F[nodei]
                else:
                    if i!= transfer_pos:
                        waiting = waiting + self.F[nodei]
                    fi = self.TransfersForward[nodei][0]
                detour = max(0, l1 + l2 - dr - waiting)
                if min_detour > detour and fi > l1 + l2 - dr:
                    min_detour = detour
                    best_p = i
            return (best_p, min_detour)

        elif node in self.deliv_to_pick: #delivery after the transfer
            # check whether delivery insertion is feasible (offset should be taken into account)
            # decide on a insertion position (offset should be taken into account)
            # *should only be done for the pickup to compute offset
            for i in range(transfer_pos+1, latest):
                nodei = self.elements[i]
                fi = 0
                if nodei not in self.Transfers:
                    fi = self.F[nodei]-offset
                else:
                    fi = self.TransfersForward[nodei][0]-offset
                    #NOTE: the connecting transfer is not in the for! offset = max(0, offset - self.F[nodei])
                    if self.A[nodei]+offset > self.W[nodei]:
                        offset = self.A[nodei]+offset - self.W[nodei]
                    else:
                        offset = 0

                l1, l2, dr = self.evaluateInsertAt(nodei,node)
                detour = l1 + l2 - dr
                if min_detour > detour and fi > detour:
                    min_detour = detour
                    best_d = i
            return (best_d, min_detour)

    def getMaxSlack(self):
        max_s = 0
        for i in self.F:
            max_s = max(max_s, self.F[i])
        for i in self.TransfersForward:
            max_s = max(max_s, self.TransfersForward[i][0])
            max_s = max(max_s, self.TransfersForward[i][1])
        return max_s

    def setName(self, name):
        self.name = name

    def setTime(self, ele, arr, dep):
        self.A[ele] = arr
        self.W[ele] = dep
        if ele in self.Transfers:
            # self.TransfersForward[ele] = (self.A[ele], self.W[ele])
            self.F[ele] = self.W[ele] - self.A[ele]
        else:
            self.F[ele] = self.locations[ele]['u'] - self.A[ele]

    def setTimeTransfer(self, tr, arr, dep, fwdA, fwdD):
        #NOTE: not checking it tr is in transfers
        self.A[tr] = arr
        self.W[tr] = dep
        self.F[tr] = dep-arr
        self.TransfersForward[tr][0] = fwdA
        self.TransfersForward[tr][1] = fwdD

    #FIXME: general to include transfers
    def setSlkForward(self, node, fws):
        self.F[node] = fws

    def setDepot(self, initial, final):
        self.DepotI = initial
        self.DepotF = final

    def addTransferOp(self, tr, op):
        if tr not in self.Transfers:
            self.Transfers[tr] = list()
            self.A[tr] = 0
            self.W[tr] = 0
            self.F[tr] = 0
            self.TransfersForward[tr] = [0,0]
            self.emptyTransfer[tr] = 0
        self.Transfers[tr].append(op)

    def addTransfer(self, tr, pos, fwda, fwdd):
        #FIXME: check whether tr is already in the map?
        self.Transfers[tr] = list()
        self.A[tr] = 0
        self.W[tr] = 0
        self.F[tr] = 0
        self.TransfersForward[tr] = [0,0]
        self.emptyTransfer[tr] = 0

        if pos == 0:    #transfer is the first route visits
            self.elements.append(tr)
            self.predecessors[tr] = None
            self.successors[tr] = None
            self.TransfersForward[tr][0] = fwda
            self.TransfersForward[tr][1] = fwdd

        elif pos < len(self.elements):
            ele = self.elements[pos]
            pred = self.predecessors[ele]
            self.successors[tr] = ele
            self.predecessors[tr] = pred
            self.predecessors[ele] = tr
            self.successors[pred] = tr

            l1 = math.ceil(distance(self.locations[pred]['x'],self.locations[pred]['y'],self.locations[tr]['x'],self.locations[tr]['y']))
            l2 = math.ceil(distance(self.locations[tr]['x'],self.locations[tr]['y'],self.locations[ele]['x'],self.locations[ele]['y']))

            self.A[tr] = self.A[pred] + l1
            self.W[tr] = self.locations[ele]['u'] - l2
            self.F[tr] = self.W[tr] - self.A[tr]
            self.A[ele] = self.locations[ele]['u']

            self.elements.insert(pos, tr)
        else:
            ele = self.elements[-1]
            self.successors[ele] = tr
            self.predecessors[tr] = ele
            self.successors[tr] = None

            l1 = math.ceil(distance(self.locations[ele]['x'],self.locations[ele]['y'],self.locations[tr]['x'],self.locations[tr]['y']))
            self.A[tr] = self.A[ele] + l1
            self.W[tr] = self.A[tr]
            self.F[tr] = self.W[tr] - self.A[tr]
            if fwda >= 0:
                self.TransfersForward[tr][0] = fwda
            else:
                self.TransfersForward[tr][0] = self.F[tr]
            if fwdd >= 0:
                self.TransfersForward[tr][1] = fwdd
            else:
                latest = self.locations[self.DepotF]['u']
                self.TransfersForward[tr][1] = latest - self.W[tr]

            self.elements.append(tr)

    def checkTransfers(self):
        for tr in self.elements:
            if tr[0]=='t':
                if tr not in self.Transfers:
                    self.Transfers[tr] = list()
                    self.A[tr] = 0
                    self.W[tr] = 0
                    self.F[tr] = 0
                    self.TransfersForward[tr] = [0,0]
                    self.emptyTransfer[tr] = 0

    def get(self):
        return self.elements

    def getSize(self):
        return len(self.elements)

    def getTransfers(self):
        return self.Transfers

    def getName(self):
        return self.name

    def getLast(self):
        return self.elements[-1]

    def getVisitPos(self, node):
        pos = 0
        for e in self.elements:
            if e==node:
                return pos
            pos = pos + 1
        return -1

    def removeTrans(self, t, op):
        self.Transfers[t].remove(op)

    def getNodeInfo(self, node):
        try:
            if node not in self.Transfers:
                return self.A[node], self.F[node]
            else:
                return self.A[node], self.W[node], self.TransfersForward[node][0], self.TransfersForward[node][1]
        except KeyError as e:
            print(self)
            print(self.getTransfers())
            print('node: ', node)
            print(self.deps)
            print(self.arrs)

    def purgeTransfers(self, assign, limit):
        self.auxList.clear()
        for tr in self.Transfers:
            for op in self.Transfers[tr]:
                if assign[op] == None:
                    self.auxList.append((tr,op))
                    # print('{0} via {1} @{2} should be removed'.format(op, tr, self.getName()))
        for (tr,op) in self.auxList:
            self.Transfers[tr].remove(op)
            #TODO: keep the transfer in the route?

        self.auxList.clear()
        for tr in self.Transfers:
            if len(self.Transfers[tr])==0:
                # print('{0} removed from {1}'.format(tr,self.getName()))
                self.emptyTransfer[tr] = self.emptyTransfer[tr] + 1
                if self.emptyTransfer[tr] >= limit:   #FIXME: hardcoded...
                    # print('{0} was idle for {1} iterations'.format(tr, self.emptyTransfer))
                    self.emptyTransfer[tr] = 0
                    self.auxList.append(tr)
            else:
                self.emptyTransfer[tr] = 0
                #self.emptyTransfer = self.emptyTransfer -1

        for t in self.auxList:
            self.emptyTransfer[t] = 0
            del self.Transfers[t]
            self.remove(t)

    def remove(self, node):
        # update successor and predecessor
        succ = self.successors[node]
        pred = self.predecessors[node]
        if succ != None and pred != None:
            self.successors[pred] = succ
            self.predecessors[succ] = pred
        elif succ != None:
            self.predecessors[succ] = None
        elif pred != None:
            self.successors[pred] = None
        #TODO: also remove entry in A,W and F?
        self.elements.remove(node)

    def create(self, ele_list):
        # self.elements = ele_list.copy()
        for e in ele_list:
            self.add(e)
            self.A[e] = 0
            self.W[e] = 0
            self.F[e] = 0
            self.E[e] = 0
            self.L[e] = 0
            self.Buff[e] = 0

    def copy(self):
        return self.elements.copy()

    # add (append) a node to the route
    def add(self, vertice):
        if self.elements:
            last = self.elements[-1]
            self.successors[last] = vertice
            self.predecessors[vertice] = last
        else:
            self.predecessors[vertice] = None
        self.elements.append(vertice)
        self.successors[vertice] = None
        self.A[vertice] = 0
        self.W[vertice] = 0
        self.F[vertice] = 0
        self.E[vertice] = 0
        self.L[vertice] = 0
        self.Buff[vertice] = 0

    # insert node at the given position
    # assuming node is not a transfer location
    def insert(self, node, pos):
        atPos = None
        l1 = l2 = ld = 0
        detour = 0
        if pos < len(self.elements):
            atPos = self.elements[pos]
            l1,l2,ld = self.evaluateInsertAt(atPos, node)
            detour = l1 + l2 - ld   #increase in time
        else:
            l1,l2,ld = self.evaluateInsertAt(None, node)

        # print('inserting {0} before {1} on {2},{3}'.format(node, atPos, self.name, detour))
        # print(self)
        # print('dependent:')
        # for (v,t) in self.deps:
        #     print(v)
        #     print('at {0}'.format(t))
        # print('end')

        # updtate route structure
        atPos = None
        if pos < len(self.elements):
            atPos = self.elements[pos]
            pred = self.predecessors[atPos]
            if pred != None:
                self.successors[pred] = node
            self.predecessors[atPos] = node
            self.successors[node] = atPos
            self.predecessors[node] = pred
        else:
            atPos = self.elements[-1]
            self.successors[atPos] = node
            self.successors[node] = None
            self.predecessors[node] = atPos

        # update arrival time at inserted node
        pred = self.predecessors[node]
        if pred != None:
            arr = self.A[pred]
            # TODO: service time
            self.A[node] = arr + l1
            if pred in self.Transfers:
                self.A[node] = self.W[pred] + l1
        else:
            self.A[node] = self.locations[self.DepotI]['l'] + l1

        self.elements.insert(pos, node)
        last_pos = len(self.elements)-1
        updateFwdList = list()
        flag_last_position_transfer = False
        aux_last_departure = 0
        # update arrival/departure times
        for i in range(pos+1, len(self.elements)):
            if detour > 0:
                atPos = self.elements[i]
                self.A[atPos] = self.A[atPos] + detour
                if atPos in self.Transfers:
                    wait = self.F[atPos]
                    if self.W[atPos] > self.A[atPos]: # detour time is absorbed by transfer waiting
                        self.F[atPos] = self.W[atPos] - self.A[atPos]
                    else:
                        #save departure time at last node to update forward slacks
                        if i == len(self.elements)-1:
                            flag_last_position_transfer = True
                            aux_last_departure = self.W[atPos]

                        forward = self.A[atPos] - self.W[atPos]
                        self.W[atPos] = self.A[atPos]
                        self.F[atPos] = 0   #waiting
                    detour = max(0, detour - wait)
                    # check whether departure times of other routes are affected
                    for (v,t) in self.deps: # e is a transfer visit
                        if atPos == t:
                            ar, wt, at, dt = v.getNodeInfo(atPos)   #assuming it's alredy computed
                            if self.A[atPos] > wt:
                                # print('{0} forwarded by {1} time units at {2}'.format(v.getName(), self.A[atPos]-wt, t))
                                # print(v)
                                v.updateDeparture(atPos, self.A[atPos]-wt)
                                updateFwdList.append(v)
                                # print(v)
                                # v.unMarkNodes()
            if detour <= 0:
                last_pos = i
                # if i != pos + 1:
                #     last_pos = i
                break

        # update forward slacks
        min_slack = 999999
        if last_pos != len(self.elements)-1:
            aux_node = self.elements[last_pos]
            succ = self.successors[aux_node]
            if succ in self.Transfers:
                min_slack = self.TransfersForward[succ][0]
            else:
                min_slack = self.F[succ]
        else:
            last = self.elements[-1]
            if last in self.Transfers:
                tr_aux = math.ceil(distance(self.locations[last]['x'],self.locations[last]['y'],self.locations[self.DepotF]['x'],self.locations[self.DepotF]['y']))
                min_slack = self.locations[self.DepotF]['u'] - tr_aux - self.W[last]
                if aux_last_departure != 0:
                    min_slack = self.TransfersForward[last][1] - (self.W[last]-aux_last_departure)
            else:
                tr_aux = math.ceil(distance(self.locations[last]['x'],self.locations[last]['y'],self.locations[self.DepotF]['x'],self.locations[self.DepotF]['y']))
                min_slack = min(self.locations[last]['u'] - self.A[last], self.locations[self.DepotF]['u'] - (self.A[last]+tr_aux))

        for i in range(last_pos, -1, -1):
            atPos = self.elements[i]
            if atPos not in self.Transfers:
                #the new element is added to the slacks map
                self.F[atPos] = min(min_slack, self.locations[atPos]['u'] - self.A[atPos])
                min_slack = self.F[atPos]
            else:
                self.TransfersForward[atPos][1] = min(min_slack,self.TransfersForward[atPos][1])
                self.TransfersForward[atPos][0] = self.W[atPos] - self.A[atPos] + self.TransfersForward[atPos][1]
                for (v,t) in self.deps: # e is a transfer visit
                    if atPos == t:
                        if v in updateFwdList:
                            # print('updating forward')
                            # print(v)
                            v.backwardPass()
                            updateFwdList.remove(v)
                            # print(v)
                        ar, wt, at, dt = v.getNodeInfo(atPos)   #assuming it's already computed
                        #NOTE: wt cannot be earlier than A[pos]
                        self.TransfersForward[atPos][0] = min(self.TransfersForward[atPos][0], wt - self.A[atPos] + dt)
                min_slack = self.TransfersForward[atPos][0]
                for (v,t) in self.arrs:
                    if atPos == t:
                        ar, wt, at, dt = v.getNodeInfo(atPos)
                        #NOTE: W[pos] cannot be earlier than ar
                        if at > self.TransfersForward[atPos][1] + self.W[atPos] - ar:
                            # print('update needed on {0}!!!'.format(v.getName()))
                            # print(v)
                            # print('{0}, {1}, {2}, {3}'.format(at,self.TransfersForward[atPos][1],self.W[atPos],ar))
                            # ar can be postponed at most till self.TransfersForward[atPos][1] + self.W[atPos]
                            v.updateBckFromTransfer(atPos, self.TransfersForward[atPos][1] + self.W[atPos] - ar)
                            # print(v)
                            # exit()
        # update slacks for routes affected by the insertion
        for r in updateFwdList:
            r.backwardPass()
        # print('last pos: {0},{1}'.format(last_pos,self.elements[last_pos]))
        # print(self)
        # print('--------------------------------------------')

    def updateDeparture(self, node, offset):
        pos = 0
        for e in self.elements:
            if e != node:
                pos = pos + 1
            else:
                atPos = self.elements[pos]
                self.W[atPos] = self.W[atPos] + offset  #departure at transfer
                self.F[atPos] = self.W[atPos] - self.A[atPos]   #wainting at transfer
                break

        for i in range(pos+1, len(self.elements)):
            if offset > 0:
                atPos = self.elements[i]
                self.A[atPos] = self.A[atPos] + offset
                if atPos in self.Transfers:
                    wait = self.F[atPos]
                    if self.W[atPos] > self.A[atPos]: # detour time is absorbed by transfer waiting
                        self.F[atPos] = self.W[atPos] - self.A[atPos]
                    else:
                        forward = self.A[atPos] - self.W[atPos]
                        self.W[atPos] = self.A[atPos]
                        self.F[atPos] = 0   #waiting
                    offset = max(0, offset - wait)
                    # check whether departure times of other routes are affected
                    for (v,t) in self.deps: # e is a transfer visit
                        if atPos == t:
                            ar, wt, at, dt = v.getNodeInfo(atPos)   #assuming it's alredy computed
                            if self.A[atPos] > wt:
                                # print('route {0} need to be updated'.format(v.getName()))
                                # print('{0}, {1}'.format(self.A[atPos],wt))
                                # print(v)
                                v.updateDeparture(atPos, self.A[atPos]-wt)
                                # print(v)
            else:
                break

    # pointer to the route in the solution data structure
    # FIXME: should be done when inserting the elements...
    def assign(self, li):
        for e in self.elements:
            if e not in self.Transfers:
                li[e] = self

    def getDistance(self):
        locs = self.locations
        current = locs[self.elements[0]]
        if self.DepotI != None:
            current = locs[self.DepotI]
        total = 0

        for o in self.elements:
            total = total + math.ceil(distance(current['x'],current['y'],locs[o]['x'],locs[o]['y']))
            current = locs[o]
        if self.DepotF != None:
            total = total + math.ceil(distance(current['x'],current['y'],locs[self.DepotF]['x'],locs[self.DepotF]['y']))

        return total

    def free(self):
        self.elements.clear()
        self.W.clear()
        self.A.clear()
        self.start = 0

    def isForwardUpdated(self):
        return self.updatedForward

    def unMarkNodes(self):
        self.updatedForward = False
        for e in self.elements:
            if e in self.Transfers:
                self.TransfersForward[e][0] = 999999
                self.TransfersForward[e][1] = 999999
            else:
                self.F[e] = 999999

    def backwardPass(self):
        # print('enter')  #DEBUG
        for e in list(reversed(self.elements)):
            succ = self.successors[e]
            if e not in self.Transfers:
                if succ != None:
                    if succ in self.Transfers:
                        self.F[e] = min(self.locations[e]['u'] - self.A[e], self.TransfersForward[succ][0])
                    else:
                        self.F[e] = min(self.locations[e]['u'] - self.A[e], self.F[succ])
                else:
                    # self.F[e] = self.locations[e]['u'] - self.A[e]  #FIXME
                    tr_aux = math.ceil(distance(self.locations[e]['x'],self.locations[e]['y'],self.locations[self.DepotF]['x'],self.locations[self.DepotF]['y']))
                    self.F[e] = min(self.locations[e]['u'] - self.A[e], self.locations[self.DepotF]['u']-(self.A[e]+tr_aux))
            else:
                # update departure slack
                if succ in self.Transfers:
                    self.TransfersForward[e][1] = self.TransfersForward[succ][0]
                elif succ != None:
                    self.TransfersForward[e][1] = self.F[succ]
                else:   #transfer is the last location visited
                    tr_aux = math.ceil(distance(self.locations[e]['x'],self.locations[e]['y'],self.locations[self.DepotF]['x'],self.locations[self.DepotF]['y']))
                    self.TransfersForward[e][1] = self.locations[self.DepotF]['u'] - (tr_aux + self.W[e])
                    # self.TransfersForward[e][1] = 600 - self.W[e]
                # update arrival slack
                min_f = self.TransfersForward[e][1] + (self.W[e]-self.A[e])
                for (v,t) in self.deps: # e is a transfer visit
                    if e == t:
                        # a, d = v.getForwardSlack(e)
                        ar, wt, at, dt = v.getNodeInfo(e)
                        if dt == 999999: # not computed yet
                            v.backwardPass()
                            ar, wt, at, dt = v.getNodeInfo(e)
                        min_f = min(min_f, wt - self.A[e] + dt)
                self.TransfersForward[e][0] = min_f
        self.updatedForward = True
        # print('exit')   #debug

    def updateBckFromTransfer(self, transfer, slack):
        current = transfer
        pred = self.predecessors[current]
        min_f = min(self.TransfersForward[current][0], slack)
        self.TransfersForward[current][0] = min_f

        while pred != None:
            current = pred
            pred = self.predecessors[current]
            if current in self.Transfers:
                self.TransfersForward[current][1] = min(min_f, self.TransfersForward[current][1])
                self.TransfersForward[current][0] = min(self.TransfersForward[current][0], self.W[current] - self.A[current] + self.TransfersForward[current][1])
                min_f = self.TransfersForward[current][0]
            else:
                min_f = min(self.F[current], min_f)
                self.F[current] = min_f

    def checkFeasibleSlacks(self):
        value = 0
        for e in self.elements:
            if e in self.Transfers:
                value = min(self.TransfersForward[e][0],self.TransfersForward[e][1])
            else:
                value = self.F[e]
            if value < 0:
                return False
        return True

    def isOnlyTransfers(self):
        for e in self.elements:
            if e not in self.Transfers:
                return False
        return True

    #NOTE: assuming no transfers
    def departureTimes(self):
        earliest_dep = self.locations[self.DepotI]['l']
        last_node = self.DepotI
        for e in self.elements:
            travel = math.ceil(distance(self.locations[last_node]['x'],self.locations[last_node]['y'],self.locations[e]['x'],self.locations[e]['y']))
            self.A[e] = max(self.locations[e]['l'], earliest_dep + travel)
            earliest_dep = self.A[e]
            last_node = e

    '''
    Magic methdos
    '''
    def __str__(self):
        str_ = self.name+": "+self.DepotI+" "
        for v in self.elements:
            # str_ += '{0}({1},{2}) '.format(str(v),str(self.A[v]),str(self.W[v]))
            if v in self.Transfers:
                a_f, d_f = self.TransfersForward[v][0], self.TransfersForward[v][1]
                str_ += '{0}({1},{2},{3},{4}) '.format(str(v),str(self.A[v]),str(self.W[v]),str(a_f),str(d_f))
            else:
                str_ += '{0}({1},{2}) '.format(str(v),str(self.A[v]),str(self.F[v]))
            # str_ += '{0}({1}) '.format(str(v),str(self.successors[v]))
            # str(v) + '('+str(self.A[v])+','+str(self.W[v])+')'
        return str_

    def __repr__(self):
        return self.name

    # when inserting a route in PriorityQueue, if equal priority, r_1 < r_2
    def __lt__(self, other):
        if self.getSize() != other.getSize():
            return self.getSize() < other.getSize()
        else:
            return self.name < other.name
