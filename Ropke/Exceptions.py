class ALNSException(Exception):
    def __init__(self, rea, sol, cur, lst_op, bank):
        self.reason = rea
        self.solution = sol
        self.current = cur
        self.last_operation = lst_op
        self.pool = bank
