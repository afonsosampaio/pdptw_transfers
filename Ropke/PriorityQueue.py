import heapq

class PriorityQueue:
    def __init__(self):
        self.elements = []

    def clear(self):
        self.elements.clear()

    def empty(self):
        return len(self.elements) == 0

    def put(self, item, priority):
        heapq.heappush(self.elements, (priority, item))

    def get(self):
        return heapq.heappop(self.elements)[1]

    def peek(self):
        return self.elements[0]

    def size(self):
        return len(self.elements)

    def getElements(self):
        return self.elements

    def show(self):
        for e in self.elements:
            print(e, end=' ')
        print('\n')
