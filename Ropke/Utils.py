import subprocess
import math
import random
import json
import functools

# Define a function for running commands and capturing stdout line by line
# (Modified from Vartec's solution because it wasn't printing all lines)
def runProcess(exe):
    p = subprocess.Popen(exe, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    return iter(p.stdout.readline, b'')

#NOTE parameters should be integers!
@functools.lru_cache(maxsize = 1000)
def distance(x1,y1,x2,y2):
    return math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))

def exportJSON(solution, locations, requests, transfers, depots, inst_file):
    # generate a json file describing the solution
    json_solution = {'requests':{}, 'transfers':{}, 'depots': {}, 'routes': {}, 'factor':0}
    data_requests = {}
    i = 1
    n = len(requests)
    average_od_distance = 0
    for r in requests:
        p = requests[r]['p']
        d = requests[r]['d']
        q = requests[r]['q']
        data_requests['r'+str(r)] = {}
        data_requests['r'+str(r)]['p'] = {'id':i,'x':locations[p]['x'],'y':locations[p]['y'],'l':locations[p]['l'],'u':locations[p]['u']}
        data_requests['r'+str(r)]['d'] = {'id':n+i,'x':locations[d]['x'],'y':locations[d]['y'],'l':locations[d]['l'],'u':locations[d]['u']}
        data_requests['r'+str(r)]['q'] = q
        i += 1
        #instance properties
        xp = locations[p]['x']
        yp = locations[p]['y']
        xd = locations[d]['x']
        yd = locations[d]['y']
        t_pd = math.ceil(distance(xp,yp,xd,yd))
        average_od_distance = average_od_distance + t_pd

    #instance properties
    Lx = 120    #TODO: info is just available when generating the instance
    Ly = 120
    average_od_distance = average_od_distance / n
    index = average_od_distance / (math.sqrt(Lx*Lx + Ly*Ly))
    #json encoding of floats is no good
    str_number = "{:.2f}".format(index)
    json_solution['factor'] = float(str_number)

    json_solution['requests'] = data_requests

    for d in depots:
        json_solution['depots'][d] = {'id':depots[d]['i'], 'x':depots[d]['x'], 'y':depots[d]['y'], 'l':depots[d]['l'], 'u':depots[d]['u']}

    for t in transfers:
        json_solution['transfers'][t] = {'id':transfers[t]['i'], 'x':transfers[t]['x'], 'y':transfers[t]['y']}


    # json_solution.clear()
    json_solution['routes'] = {}
    i = 0
    for r in solution['r']: #TODO:
        json_solution['routes']['r_'+str(i)] = [r.DepotI]
        for e in r.get():
            json_solution['routes']['r_'+str(i)].append(e)
        json_solution['routes']['r_'+str(i)].append(r.DepotF)
        i += 1

    with open(inst_file.replace('.ins','.json'), 'w') as fh:
        json.dump(json_solution, fh)
    with open('solution.json', 'w') as fh:
        json.dump(json_solution['routes'], fh)

def exportGnuplot(solution, locations, transfers, pickups, deliveries, file):
    pallete = ['#0072bd','#d95319','#edb120','#7e2f8e','#77ac30','#4dbeee','#a2142f','#1B9E77', '#D95F02', '#7570B3', '#E7298A', '#66A61E', '#E6AB02', '#A6761D', '#666666']

    # blue,orange,yellow,purple,green,light-blue,red,dark teal,dark orange,dark lilac,dark magenta,dark lime green,dark banana,dark tan,dark gray

    with open(file, 'w+') as fh:
        fh.write('set terminal png size 1800,1800 enhanced font "Helvetica,20\n')
        fh.write("set output 'output.png'\n")
        fh.write("plot '-' with points pointtype 9 pointsize 5 linecolor -1 title 'Transfers', '-' with points pointtype 1 pointsize 2 linecolor -1 title 'Pickups', '-' with points pointtype 2 pointsize 2 linecolor -1 title 'Deliveries'")
        c = 0
        for r in solution['r']:
            fh.write(", '-' w lines linestyle 1 lc rgb '{0}'".format(pallete[c]))
            c = c + 1
        fh.write('\n')

        for t in transfers:
            fh.write('{0} {1}\n'.format(locations[t]['x'], locations[t]['y']))
        fh.write('e\n')

        for p in pickups:
            fh.write('{0} {1}\n'.format(locations[p]['x'], locations[p]['y']))
        fh.write('e\n')

        for d in deliveries:
            fh.write('{0} {1}\n'.format(locations[d]['x'], locations[d]['y']))
        fh.write('e\n')

        for r in solution['r']:
            for o in r.get():
                fh.write('{0} {1}\n'.format(locations[o]['x'], locations[o]['y']))
            fh.write('e\n')

# a roulette wheel selection for items
def weighted_choice(items):
    total_weight = sum(items[key]['weight'] for key in items)
    weight_to_target = random.uniform(0, total_weight)
    for key in items:
        weight_to_target -= items[key]['weight']
        if weight_to_target <= 0:
            return key

#
def getOverlap(a, b):
    return max(0, min(a[1],b[1]) - max(a[0],b[0]) + 1)

'''vectorial math'''
'''
def lengh(u):
    return math.sqrt(u[0]*u[0]+u[1]*u[1])
def dot_product(u,v):
    return u[0]*v[0]+u[1]*v[1]
def determinant(u,v):
    return u[0]*v[1]-v[0]*u[0]
def inner_angle(u,v):
    cosx = dot_product(u,v)/(lengh(u)*lengh(v))
    if math.fabs(cosx-1) < 0.0001:
        print(cosx)
        cosx = 1.00
    if math.fabs(cosx+1) < 0.0001:
        print(cosx)
        cosx = -1.00
    rad = math.acos(cosx)
    return rad*180/math.pi
def clockwise_angle(u,v):
    in_angle = inner_angle(u,v)
    if determinant(u, v) < 0:
        return in_angle
    else:
        return 360-in_angle
'''
