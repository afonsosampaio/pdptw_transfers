import re
import math
import random
from Route import *
from Utils import runProcess
from Utils import distance
from Utils import exportJSON

'''
Represents an instance within Ropkes's ALNS-PDPTW
'''
class Instance:
    def __init__(self, req, loc, tra, p2d, d2p, pd2r):
        self.requests = req
        self.locations = loc
        self.transfers = tra
        self.pick_to_deliv = p2d
        self.deliv_to_pick = d2p
        self.pd_to_request = pd2r
        self.transfer_operations = {}
        self.transfers_sections = {}
        self.size = 0
        self.cap = 0
        self.veh = 0
        self.orderP = []
        self.orderD = []
        self.orderIdx = 0
        self.Solution = {}
        self.E = 0
        self.L = 0

    def __str__(self):
        return ""+str(self.size)+" "+str(self.veh)+" "+str(self.cap)

    def solve_pickupdelivery(self, file):
        # Run the command and capture the output line by line.
        cmd = './Ropke/ALNS-PDPTW -f '+ file + ' -t 3 -s '+file+'.sol'
        print(cmd)
        for line in runProcess(cmd.split()):
            # print(line)
            pass
        #parse the solution file
        solution = {}
        id = 0  #vehicle id
        with open(file+'.sol') as fh:
            for line in fh:
                solution[id] = []
                # line = fh.readline()
                solution[id] = [self.getKeyByPosition(int(s.strip())) for s in re.split('{ | }| |\n',line) if s.strip()]
                for loc in solution[id]:
                    if loc in self.Solution:
                        self.Solution[loc]['v'] = id
                id = id + 1

        # return a solution stored in a data structure used by the alns
        sol_ADT = {'r':[], 't':[]}   # routes, assigment, transfers, cost
        total_cost = 0
        # tr_visited = dict()
        # not_included = list()
        transfers_sections = list()

        for r in solution:
            # parse the solution to prune auxiliary requests(@transfers)
            print('------------------')
            print(solution[r])
            i,j,t = -1,-1,-1
            flag = False
            pos = 0
            transfers_sections.clear()
            for e in solution[r]:
                node = e.split('_')[0]
                if node in self.transfers: #is a transfer operation
                    if flag == False:
                        i = pos
                        t = node
                        flag = True
                    elif node == t: # an operation on the same initial transfer
                        j = pos
                    else: # a (different) transfer in sequence
                        transfers_sections.append((i,j,t))
                        i = pos
                        j = -1
                        t = node
                else:
                    if flag == True:
                        transfers_sections.append((i,j,t))
                        i,j = -1,-1
                        flag = False
                pos += 1

            # build the final soltuion from the routes list
            # print(solution[r])
            transfers_visits = {}
            for (i,j,t) in transfers_sections:
                if t in transfers_visits:
                    transfers_visits[t][0] = transfers_visits[t][0] + 1
                    if j-i > transfers_visits[t][2] - transfers_visits[t][1]:
                        transfers_visits[t][1] = i
                        transfers_visits[t][2] = j
                else:
                    transfers_visits[t] = [1,i,j]

            # for t in transfers_visits:
            #     if transfers_visits[t][0] > 1:
            #         print('{0} visits at {1}. {2},{3}'.format(transfers_visits[t][0], t, transfers_visits[t][1], transfers_visits[t][2]))

            idxPos = 0
            toRemove = list()
            for (i,j,t) in transfers_sections:
                if transfers_visits[t][0] > 1 and (j!=transfers_visits[t][2] or i!=transfers_visits[t][1]):
                    # print('removing {0},{1},{2}'.format(i,j,t))
                    toRemove.append((i,j,t))
                idxPos = idxPos + 1

            for (i,j,t) in toRemove:
                transfers_sections.remove((i,j,t))

            # for (i,j,t) in transfers_sections:
            #     print(i,j,t)

            aux_route = Route(self.locations, self.requests, self.pick_to_deliv, self.deliv_to_pick, 'R'+str(r))
            for (i,j,t) in transfers_sections:
                if i > 0:
                    if j < 0:
                        j = i
                    trs = solution[r][i:j+1]
                    # replace transfer requests with just one node in the route
                    # solution needs to store the vehicle operations at transfers...
                    tr_op = solution[r][i].split('_')
                    # aux_route.addTransferOp(tr_op[0], tr_op[1])
                    for it in range(i+1,j+1):
                        if solution[r][it] != 'r':
                            aux_re = solution[r][it].split('_')
                            # aux_route.addTransferOp(aux_re[0], aux_re[1])
                            solution[r][it] = 'r'
                    solution[r][i] = t

            toRemove.clear()
            for it in range(0,len(solution[r])):
                aux_re = solution[r][it].split('_')
                if len(aux_re) != 1:
                    if aux_re[1] in self.pick_to_deliv:
                        toRemove.append(self.pick_to_deliv[aux_re[1]])
                        # print('should remove {0}!'.format(self.pick_to_deliv[aux_re[1]]))
                    elif aux_re[1] in self.deliv_to_pick:
                        toRemove.append(self.deliv_to_pick[aux_re[1]])
                        # print('should remove {0}!'.format(self.deliv_to_pick[aux_re[1]]))
                    solution[r][it] = 'r'

            for i in range(solution[r].count('r')):
                solution[r].remove('r')

            for rem in toRemove:
                solution[r].remove(rem)

            sol_ADT['r'].append(aux_route)
            aux_route.create(solution[r])

            print(solution[r])
            print('------------------')

        # return sol_ADT
        sol_ADT['c'] = total_cost
        return sol_ADT

    # def filterRequestsByDistance(self, limit):
    #     # comprehesion?
    #     filtered = list()
    #     trans_asg = dict()
    #     for r in self.requests:
    #         p = self.locations[self.requests[r]['p']]
    #         d = self.locations[self.requests[r]['d']]
    #         dist = distance(p['x'],p['y'],d['x'],d['y'])
    #         if dist > limit:
    #             best = -1,999999
    #             for tr in self.transfers:
    #                 t = self.locations[tr]
    #                 travel_pt = math.ceil(distance(p['x'],p['y'],t['x'],t['y']))
    #                 travel_td = math.ceil(distance(t['x'],t['y'],d['x'],d['y']))
    #                 if travel_pt + travel_td < best[1]:
    #                     best = tr, travel_pt + travel_td
    #             filtered.append(r)
    #             trans_asg[r] = best[0]
    #     return filtered, trans_asg

    def filterRequestsByDistance(self,limit):
        filtered = list()
        trans_asg = dict()
        for r in self.requests:
            p = self.locations[self.requests[r]['p']]
            d = self.locations[self.requests[r]['d']]
            dist = math.ceil(distance(p['x'],p['y'],d['x'],d['y']))
            if dist > limit:
                best = -1, 999999, 999999
                for tr in self.transfers:
                    t = self.locations[tr]
                    travel_pt = math.ceil(distance(p['x'],p['y'],t['x'],t['y']))
                    travel_td = math.ceil(distance(t['x'],t['y'],d['x'],d['y']))
                    if math.fabs(travel_pt-travel_td) < best[1]:
                        if travel_pt + travel_td < best[2]:
                            best = tr, math.fabs(travel_pt-travel_td), travel_pt + travel_td
                filtered.append(r)
                trans_asg[r] = best[0]
        return filtered, trans_asg

    #NOTE: nearest to the pickup
    def filterRequestsByNearestTr(self, limit):
        filtered = list()
        trans_asg = dict()
        for r in self.requests:
            p = self.locations[self.requests[r]['p']]
            d = self.locations[self.requests[r]['d']]
            dist = distance(p['x'],p['y'],d['x'],d['y'])    #direct distance
            closest = (-1,99999)
            for tr in self.transfers:
                t = self.locations[tr]
                pt = distance(p['x'],p['y'],t['x'],t['y'])
                td = distance(t['x'],t['y'],d['x'],d['y'])
                # aux = min(pt, td)
                aux = pt
                if aux < limit and aux > limit/2 and dist >= 2*limit:
                    if closest[1] > aux:
                        closest = (tr, aux)
            if closest[0] != -1:
                filtered.append(r)
                trans_asg[r] = closest[0]
        return filtered, trans_asg

    # def filterRequestsByCluster(self, limit):
    #     # reqPairs = [(ri,rj) for ri in self.requests for rj in self.requests if ri<rj]
    #     # IDEA: iterate rj in reverse (ordered lists...)
    #     candidates = list()
    #     for ri in self.requests:
    #         candidates.clear()
    #         for rj in self.requests:
    #             if ri < rj:
    #                 di = self.locations[self.requests[ri]['d']]
    #                 dj = self.locations[self.requests[rj]['d']]
    #                 dist = math.ceil(distance(di['x'],di['y'],dj['x'],dj['y']))
    #                 if dist < limit:
    #                     pi = self.locations[self.requests[ri]['p']]
    #                     pj = self.locations[self.requests[rj]['p']]
    #                     dist = math.ceil(distance(pi['x'],pi['y'],pj['x'],pj['y']))
    #                     if dist > 3*limit:
    #                         candidates.append(rj)
    #         print(candidates)

    # locations['0'] should have already been set...
    def filterRequestsByDepot(self, depots, depot):
        filtered = list()
        for r in self.requests:
            p = self.locations[self.requests[r]['p']]
            d = self.locations[self.requests[r]['d']]
            best = -1,999999
            for dep in depots:
                t_0p = math.ceil(distance(depots[dep]['x'],depots[dep]['y'],p['x'],p['y']))
                t_d0 = math.ceil(distance(d['x'],d['y'],depots[dep]['x'],depots[dep]['y']))
                if best[1] > t_0p + t_d0:
                    best = dep, t_0p + t_d0
            if best[0] == depot:
                filtered.append(r)
        return filtered

    def defineTransferredRequestsDepot(self, depots, limit):
        filtered = list()
        assignment = {}
        for r in self.requests:
            p = self.locations[self.requests[r]['p']]
            d = self.locations[self.requests[r]['d']]
            dist = math.ceil(distance(p['x'],p['y'],d['x'],d['y']))
            if dist > limit:
                best_pick = -1,999999
                best_deliv = -1,999999
                for dep in depots:
                    t_0p = math.ceil(distance(depots[dep]['x'],depots[dep]['y'],p['x'],p['y']))
                    t_d0 = math.ceil(distance(d['x'],d['y'],depots[dep]['x'],depots[dep]['y']))
                    if t_0p < best_pick[1]:
                        best_pick = dep, t_0p
                    if t_d0 < best_deliv[1]:
                        best_deliv = dep, t_d0
                if best_pick[0] != best_deliv[0]:
                    filtered.append(r)
                    assignment[r] = {'p':best_pick[0], 'd':best_deliv[0]}
        return filtered, assignment

    def getKeyByPosition(self, pos):
        if pos <= self.orderIdx:
            return self.orderP[pos-1]
        else:
            return self.orderD[pos-self.orderIdx-1]

    def getVehicles(self, r):
        p = self.requests[r]['p']
        d = self.requests[r]['d']

        return self.Solution[p]['v'], self.Solution[d]['v']

    def getParameters(self):
        return self.E, self.L

    def read_file(self, file):
        # read using Ropke's format
        with open(file) as fh:
            #first line: size vehicles cap
            line = fh.readline()
            #TODO: check if this could be a problem later
            result = [int(s) for s in line.split('\t')]
            self.size, self.veh, self.cap = result
            #second line: depot information
            line = fh.readline()
            result = line.split('\t')
            self.locations[result[0]] = {}
            self.locations[result[0]]['x'] = int(result[1])
            self.locations[result[0]]['y'] = int(result[2])
            self.locations[result[0]]['l'] = int(result[4])
            self.locations[result[0]]['u'] = int(result[5])
            self.E = int(result[4])
            self.L = int(result[5])
            #subsequent lines: pickup and delivery locations
            for i in range(1, self.size+1):
                line = fh.readline()
                result = re.split('\t|\n',line)
                self.locations[result[0]] = {}
                self.locations[result[0]]['x'] = int(result[1])
                self.locations[result[0]]['y'] = int(result[2])
                self.locations[result[0]]['l'] = int(result[4])
                self.locations[result[0]]['u'] = int(result[5])
                self.locations[result[0]]['s'] = int(result[6])
                if int(result[3]) > 0:
                    self.requests[i] = {}
                    self.requests[i]['p'] = result[0]
                    self.requests[i]['d'] = result[8]
                    self.requests[i]['q'] = int(result[3])
                    self.Solution[result[0]] = {'v':-1, 'a': '0'}
                    self.Solution[result[8]] = {'v':-1, 'a': '0'}
                    self.pick_to_deliv[result[0]] = result[8]
                    self.deliv_to_pick[result[8]] = result[0]
                    self.pd_to_request[result[0]] = i
                    self.pd_to_request[result[8]] = i

    # deliv_to_pick = {d : p for p, d in self.pick_to_deliv.items()}

    def createFormat23File(self, depots, file):
        n = len(self.requests)
        m = 0  #number of VEHICLES!

        alpha = 1
        beta = 0
        gamma = 10000

        vehicles_order = []
        locations_order = []

        with open(file,'w') as hdl:
            for d in depots:
                m = m + depots[d]['m']
            hdl.write('{0} {1}\n{2} {3} {4}\n'.format(n,m,alpha,beta,gamma))
            id = 0
            cap = 100   #TODO vehicle capacity
            for d in depots:
                x = self.locations[d]['x']
                y = self.locations[d]['y']
                e = self.locations[d]['l']
                l = self.locations[d]['u']
                # onde line for each vehicle in the depot
                for veh in range(0,depots[d]['m']):
                    hdl.write('{0} {1} {2} {3} {4} {5} {6} {7}\n'.format(id,cap,x,y,x,y,e,l))
                    vehicles_order.append(d)    #start
                    vehicles_order.append(d)    #end
                    id = id + 1

            id = 0
            for r in self.requests:
                p = self.requests[r]['p']
                d = self.requests[r]['d']
                locations_order.append(p)
                locations_order.append(d)
                q = self.requests[r]['q']
                px = self.locations[p]['x']
                py = self.locations[p]['y']
                pe = self.locations[p]['l']
                pl = self.locations[p]['u']
                dx = self.locations[d]['x']
                dy = self.locations[d]['y']
                de = self.locations[d]['l']
                dl = self.locations[d]['u']
                hdl.write('{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} '.format(id,q,px,py,dx,dy,pe,pl,de,dl))
                hdl.write('0 0 -1\n')
                id = id + 1

            for d in vehicles_order:
                locations_order.append(d)

            # distance matrix
            line = []
            for i in range(0,len(locations_order)):
                line.clear()
                s = locations_order[i]
                sx = self.locations[s]['x']
                sy = self.locations[s]['y']
                for j in range(0,len(locations_order)):
                    e = locations_order[j]
                    ex = self.locations[e]['x']
                    ey = self.locations[e]['y']
                    dist = math.ceil(distance(sx,sy,ex,ey))
                    line.append(dist)
                for value in line:
                    hdl.write('{0} '.format(value))
                hdl.write('\n')

            # travel matrix
            line = []
            for i in range(0,len(locations_order)):
                line.clear()
                s = locations_order[i]
                sx = self.locations[s]['x']
                sy = self.locations[s]['y']
                for j in range(0,len(locations_order)):
                    e = locations_order[j]
                    ex = self.locations[e]['x']
                    ey = self.locations[e]['y']
                    dist = math.ceil(distance(sx,sy,ex,ey))
                    line.append(dist)
                for value in line:
                    hdl.write('{0} '.format(value))
                hdl.write('\n')

        cmd = './Ropke/ALNS-PDPTW -f '+ file + ' -t 23 -s '+file+'.sol'
        print(cmd)
        for line in runProcess(cmd.split()):
            # print(line)
            pass
        #parse the solution file
        solution = {}
        id = 0  #vehicle id
        with open(file+'.sol') as fh:
            for line in fh:
                solution[id] = []
                # line = fh.readline()
                aux = line[line.find('{') : line.find('}')]
                solution[id] = [int(s.strip()) for s in re.split('{ | }| |\n',aux) if s.strip()]
                for loc in solution[id]:
                    if loc in self.Solution:
                        self.Solution[loc]['v'] = id
                id = id + 1

        sol_ADT = {'r':[], 't':[], 'c':999999}   # routes, assigment, transfers, cost
        converted = []
        total_cost = 0
        for r in solution:
            aux_route = Route(self.locations, self.requests, self.pick_to_deliv, self.deliv_to_pick, 'R'+str(r))
            converted.clear()
            for i in solution[r]:
                node = locations_order[i]
                if node in depots:
                    aux_route.setDepot(node,node)
                else:
                    converted.append(node)

            if aux_route.DepotI != None:    #not an empty route
                aux_route.create(converted)
                if len(aux_route.get())>0:
                    total_cost = total_cost + aux_route.getDistance()
                    # print(aux_route)
                    sol_ADT['r'].append(aux_route)
        sol_ADT['c'] = total_cost

        for r in sol_ADT['r']:
            r.departureTimes()
            r.unMarkNodes()
        for r in sol_ADT['r']:
            if r.isForwardUpdated() == False:
                r.backwardPass()
        for r in sol_ADT['r']:
            print(r)
        print(sol_ADT)
        # exportGnuplot(incumbent, locations, transfers, pick_to_deliv, deliv_to_pick, out_file)
        # exportJSON(sol_ADT, self.locations, self.requests, [], depots)
        return sol_ADT

    def createFilefrom(self, filtered, transfer_assign, file):
        # remove infeasbile transfers first
        locations_fake = {}
        for r in filtered:
            p = self.requests[r]['p']
            d = self.requests[r]['d']

            t = transfer_assign[r]
            travel_pt = math.ceil(distance(self.locations[p]['x'],self.locations[p]['y'],self.transfers[t]['x'],self.transfers[t]['y']))
            travel_td = math.ceil(distance(self.transfers[t]['x'],self.transfers[t]['y'],self.locations[d]['x'],self.locations[d]['y']))
            lb = self.locations[p]['l'] + travel_pt
            ub = self.locations[d]['u'] - travel_td
            if ub - lb > 60:    #TODO: parameterized
                slack = (ub-lb)/2 if (ub-lb)%2==0 else (ub-lb+1)/2
                self.transfer_operations[r] = t  #, lb, int(lb+slack), ub
                locations_fake[t+'_'+p] = {'x':self.transfers[t]['x'], 'y':self.transfers[t]['y'], 'l':int(lb+slack), 'u':ub, 's':0}
                locations_fake[t+'_'+d] = {'x':self.transfers[t]['x'], 'y':self.transfers[t]['y'], 'l':lb, 'u':int(lb+slack), 's':0}
                # print(p,d,lb,int(lb+slack),ub)
            else:   # not possible to transfer
                filtered.remove(r)
                print('request {0} removed...'.format(r))

        with open(file, 'w') as fh:    #w+?
            n = len(self.requests) + len(filtered)  # each req in filtered = 2 reqs: p-t-d
            print('number of request {0}'.format(n))
            self.orderIdx = n
            self.orderP.clear()
            self.orderD.clear()
            line = (str(2*n) + '\t' + str(self.veh) + '\t' + str(self.cap) + '\n')
            fh.write(line)
            line = '0\t'+str(self.locations['0']['x'])+'\t'+str(self.locations['0']['y'])+'\t'
            line += '0\t'+str(self.E)+'\t'+str(self.L)+'\t'
            # line += '0\t'+str(self.locations['0']['l'])+'\t'+str(self.locations['0']['u'])+'\t'
            line += '0\t0\t0\n'
            fh.write(line)
            line_p = ''
            line_d = ''
            key = 1 #TODO: this is just for the format used in Ropke's ALNS
            for req in self.requests:
                if req not in filtered:
                    p = self.requests[req]['p']
                    d = self.requests[req]['d']
                    line_p += str(key) + '\t' + str(self.locations[p]['x']) + '\t' + str(self.locations[p]['y'])
                    line_p += '\t' + str(self.requests[req]['q'])
                    line_p += '\t' + str(self.locations[p]['l']) + '\t' + str(self.locations[p]['u'])
                    line_p += '\t' + str(self.locations[p]['s']) + '\t0\t' + str(n+key) + '\n'
                    line_d += str(n+key) + '\t' + str(self.locations[d]['x']) + '\t' + str(self.locations[d]['y'])
                    line_d += '\t' + str(-1*self.requests[req]['q'])
                    line_d += '\t' + str(self.locations[d]['l']) + '\t' + str(self.locations[d]['u'])
                    line_d += '\t' + str(self.locations[d]['s']) + '\t' + str(key) + '\t0\n'
                    self.orderP.append(p)
                    self.orderD.append(d)
                    # print(p,d,key,n+key)
                    key += 1

            # requests at transfers
            for req in filtered:
                p = self.requests[req]['p']
                d = self.requests[req]['d']
                t = self.transfer_operations[req]
                # p---t
                line_p += str(key) + '\t' + str(self.locations[p]['x']) + '\t' + str(self.locations[p]['y'])
                line_p += '\t' + str(self.requests[req]['q'])
                line_p += '\t' + str(self.locations[p]['l']) + '\t' + str(self.locations[p]['u'])
                line_p += '\t' + str(self.locations[p]['s']) + '\t0\t' + str(n+key) + '\n'
                line_d += str(n+key) + '\t' + str(locations_fake[t+'_'+d]['x']) + '\t' + str(locations_fake[t+'_'+d]['y'])
                line_d += '\t' + str(-1*self.requests[req]['q'])
                line_d += '\t' + str(locations_fake[t+'_'+d]['l']) + '\t' + str(locations_fake[t+'_'+d]['u'])
                line_d += '\t' + str(locations_fake[t+'_'+d]['s']) + '\t' + str(key) + '\t0\n'

                self.orderP.append(p)
                self.orderD.append(t+'_'+d)
                # print(p,t+d,key,n+key)
                key += 1

                # t---d
                line_p += str(key) + '\t' + str(locations_fake[t+'_'+p]['x']) + '\t' + str(locations_fake[t+'_'+p]['y'])
                line_p += '\t' + str(self.requests[req]['q'])
                line_p += '\t' + str(locations_fake[t+'_'+p]['l']) + '\t' + str(locations_fake[t+'_'+p]['u'])
                line_p += '\t' + str(locations_fake[t+'_'+p]['s']) + '\t0\t' + str(n+key) + '\n'
                line_d += str(n+key) + '\t' + str(self.locations[d]['x']) + '\t' + str(self.locations[d]['y'])
                line_d += '\t' + str(-1*self.requests[req]['q'])
                line_d += '\t' + str(self.locations[d]['l']) + '\t' + str(self.locations[d]['u'])
                line_d += '\t' + str(self.locations[d]['s']) + '\t' + str(key) + '\t0\n'

                self.orderP.append(t+'_'+p)
                self.orderD.append(d)
                # print(t+p,d,key,n+key)
                key += 1
            fh.write(line_p)
            fh.write(line_d)
        fh.close()
