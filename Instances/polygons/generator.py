#! /usr/bin/env python3

import sys
#import numpy as np
import math
import json
import argparse
import re
import itertools
#import matplotlib.pyplot as plt

def main(n, vertices, outputFile):
    outfile = open(outputFile+'.pdp', 'w')

    js_data = {}
    js_data['requests'] = {}
    nReq = 0
    R = n*(n-1)

    verticeList = vertices[str(n)]

    for [xp,yp] in verticeList:
        for [xd,yd] in verticeList:
            if xp!=xd or yp!=yd:
                nReq = nReq + 1
                js_data['requests']['r_'+str(nReq)] = {}
                js_data['requests']['r_'+str(nReq)]['p'] = {}
                js_data['requests']['r_'+str(nReq)]['d'] = {}
                js_data['requests']['r_'+str(nReq)]['q'] = 1
                js_data['requests']['r_'+str(nReq)]['p']['id'] = nReq
                js_data['requests']['r_'+str(nReq)]['p']['x'] = int(xp)
                js_data['requests']['r_'+str(nReq)]['p']['y'] = int(yp)
                js_data['requests']['r_'+str(nReq)]['p']['l'] = 100
                js_data['requests']['r_'+str(nReq)]['p']['u'] = 205
                js_data['requests']['r_'+str(nReq)]['d']['id'] = int(nReq + R)
                js_data['requests']['r_'+str(nReq)]['d']['x'] = int(xd)
                js_data['requests']['r_'+str(nReq)]['d']['y'] = int(yd)
                js_data['requests']['r_'+str(nReq)]['d']['l'] = 200
                js_data['requests']['r_'+str(nReq)]['d']['u'] = 305

    #parameters
    outfile.write(str(2*R)+'\t1\t'+str(300)+'\n') #number of vehicles is irrelevant if min vehicles

    #depot
    H = 405
    outfile.write('0\t'+str(100)+'\t'+str(100)+'\t0\t0\t'+str(H)+'\t0\t0\t0\n')

    #pickups
    for r in sorted(js_data['requests'], key=lambda x: js_data['requests'][x]['p']['id']):
        aux = str(js_data['requests'][r]['p']['id']) + '\t' + str(js_data['requests'][r]['p']['x']) + '\t'
        aux = aux + str(js_data['requests'][r]['p']['y']) + '\t' + str(js_data['requests'][r]['q']) + '\t'
        aux = aux + str(js_data['requests'][r]['p']['l']) + '\t' + str(js_data['requests'][r]['p']['u']) + '\t0\t0\t'
        aux = aux + str(js_data['requests'][r]['d']['id'])+'\n'
        outfile.write(aux)
    #deliveries
    for r in sorted(js_data['requests'], key=lambda x: js_data['requests'][x]['d']['id']):
        aux = str(js_data['requests'][r]['d']['id']) + '\t' + str(js_data['requests'][r]['d']['x']) + '\t'
        aux = aux + str(js_data['requests'][r]['d']['y']) + '\t' + str(-1*js_data['requests'][r]['q']) + '\t'
        aux = aux + str(js_data['requests'][r]['d']['l']) + '\t' + str(js_data['requests'][r]['d']['u']) + '\t0\t'
        aux = aux + str(js_data['requests'][r]['p']['id'])+'\t0\n'
        outfile.write(aux)
    outfile.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='PDPTW instance generator')
    parser.add_argument('-f', '--file', type=str, help='Output file', required=True)
    parser.add_argument('-p', '--param', type=str, help='Parameters file', required=True)
    parser.add_argument('-n', '--n', type=int, help='Polygon size', required=True)

    #parse arguments
    args = parser.parse_args()

    # read file with the vertices coordinates
    json_file = open(args.param)
    json_data = json.load(json_file)
    json_file.close()


    main(args.n, json_data, args.file)
