#! /usr/bin/env python3.6

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import csv
import itertools
# from matplotlib.font_manager import FontProperties

plt.style.use('seaborn-colorblind')
# print(plt.style.available)
# fontP = FontProperties()
# fontP.set_size('small')

# print(mpl.matplotlib_fname())
#ind = np.linspace(1, 50, 50)
font = {'family' : 'normal',
        'size'   : 16}

mpl.rc('font', **font)
fig, axs = plt.subplots(3,4)
plt.subplots_adjust(left=0.03, bottom=0.06, right=0.99, top=0.98, wspace=0.15, hspace=0.20)

ind = np.linspace(1, 50, 50)
ind_p = np.linspace(0, 51, 51)

with open('5_300SR.csv', 'r') as csv_file:
    try:
        reader = csv.reader(csv_file)
        ratio = {}
        weights = {}
        row_num = 0
        for row in reader:
            if row_num < 1 or len(row) < 1:
                row_num = row_num + 1
                continue
            ratio[row[0]] = []
            col_num = 0
            for col in row:
                if col_num%4 == 1:
                    ratio[row[0]].append(float(col))
                col_num = col_num + 1

            weights[row[0]] = [1.0]
            col_num = 0
            for col in row:
                if col_num%4 == 0 and col_num > 0:
                    weights[row[0]].append(float(col))
                col_num = col_num + 1
    except ValueError:
        print(row)
        print('col:',col)
        quit()

#removal
axs[0,0].set_xlabel('Iterations (x500)')
axs[0,0].set_ylabel('Ratio')
axs[0,0].set_title("Removal operators")
axs[0,0].set_xticks(np.arange(0,51, step=5))
axs[0,0].set_xlim(0.5,51)
acc = [ratio['rndRem'][j]+ratio['worstRem'][j]+ratio['routeRem'][j]+ratio['clusterRem'][j] for j in range(len(ratio['rndRem']))]
ratio['biasRem'] = [ratio['biasRem'][j] if acc[j]+ratio['biasRem'][j] < 0.98 else 1.0-acc[j] for j in range(len(acc))]
b1 = axs[0,0].bar(ind, ratio['worstRem'], 1.01, hatch='_', label='worst')
b2 = axs[0,0].bar(ind, ratio['rndRem'], 1.01, bottom=[ratio['worstRem'][j] for j in range(len(ratio['rndRem']))], hatch = '/',label='rnd')
b3 = axs[0,0].bar(ind, ratio['routeRem'], 1.01, bottom=[ratio['rndRem'][j]+ratio['worstRem'][j] for j in range(len(ratio['rndRem']))], hatch = 'x',label='route')
b4 = axs[0,0].bar(ind, ratio['biasRem'], 1.01, bottom=[ratio['rndRem'][j]+ratio['worstRem'][j]+ratio['routeRem'][j] for j in range(len(ratio['rndRem']))], hatch = '.',label='bias')
b5 = axs[0,0].bar(ind, ratio['clusterRem'], 1.01, bottom=[ratio['rndRem'][j]+ratio['worstRem'][j]+ratio['routeRem'][j]+ratio['biasRem'][j] for j in range(len(ratio['rndRem']))], hatch='+',label='cluster')
# axs[0,0].legend((b1[0],b2[0],b3[0],b4[0],b5[0]),('rndRem','worstRem','routeRem','biasRem','clusterRem'))

axs[0,1].set_xlabel('Iterations (x500)')
axs[0,1].set_ylabel('Weight')
axs[0,1].set_xticks(np.arange(0, 51, step=5))
axs[0,1].set_xlim(0,51)
axs[0,1].grid()
p1 = axs[0,1].plot(ind_p, weights['worstRem'], label='worstRem')
p2 = axs[0,1].plot(ind_p, weights['rndRem'], label='rndRem')
p3 = axs[0,1].plot(ind_p, weights['routeRem'], label='routeRem')
p4 = axs[0,1].plot(ind_p, weights['biasRem'], label='biasRem')
p5 = axs[0,1].plot(ind_p, weights['clusterRem'], label='clusterRem')
axs[0,1].legend((b1[0],b2[0],b3[0],b4[0],b5[0]),(r'$O^-_1$',r'$O^-_2$',r'$O^-_3$',r'$O^-_4$',r'$O^-_5$'),loc='upper center', ncol=3)

#insertion
#NT
axs[1,0].set_xlabel('Iterations (x500)')
axs[1,0].set_ylabel('Ratio')
axs[1,0].set_title("Insert operators (no-transfers)")
axs[1,0].set_xticks(np.arange(0, 51, step=5))
axs[1,0].set_xlim(0.5,51)
acc = [ratio['greedyInsNT'][j]+ratio['2rgtInsNT'][j]+ratio['rndInsNT'][j] for j in range(len(ratio['greedyInsNT']))]
ratio['distInsNT'] = [ratio['distInsNT'][j] if acc[j]+ratio['distInsNT'][j] < 0.98 else 1.0-acc[j] for j in range(len(acc))]
b1 = axs[1,0].bar(ind, ratio['distInsNT'], 1.01, label='distInsNT', hatch='x', color='b')
b2 = axs[1,0].bar(ind, ratio['greedyInsNT'], 1.01, bottom=ratio['distInsNT'], label='greedyInsNT', hatch='.', color='g')
b3 = axs[1,0].bar(ind, ratio['2rgtInsNT'], 1.01, bottom=[ratio['greedyInsNT'][j]+ratio['distInsNT'][j] for j in range(len(ratio['greedyInsNT']))], label='2rgtNT', hatch=',', color='r')
b4 = axs[1,0].bar(ind, ratio['rndInsNT'], 1.01, bottom=[ratio['greedyInsNT'][j]+ratio['distInsNT'][j]+ratio['2rgtInsNT'][j] for j in range(len(ratio['greedyInsNT']))], label='rndNT', hatch='v', color='c')
# axs[0].legend(loc='upper center', ncol=4)

axs[1,1].set_xlabel('Iterations (x500)')
axs[1,1].set_ylabel('Weight')
axs[1,1].set_xticks(np.arange(0, 51, step=5))
axs[1,1].set_xlim(0,51)
axs[1,1].grid()
p1 = axs[1,1].plot(ind_p, weights['distInsNT'], label='distNT', color='b') #, marker='1'
p2 = axs[1,1].plot(ind_p, weights['greedyInsNT'], label='greedyNT', color='g')
p3 = axs[1,1].plot(ind_p, weights['2rgtInsNT'], label='2rgtNT', color='r')
p4 = axs[1,1].plot(ind_p, weights['rndInsNT'], label='rndNT', color='c')
axs[1,1].legend((b1[0],b2[0],b3[0],b4[0]),(r'$O^+_1$',r'$O^+_2$',r'$O^+_3$',r'$O^+_4$'),loc='upper center', ncol=5)

#T
axs[2,0].set_xlabel('Iterations (x500)')
axs[2,0].set_ylabel('Ratio')
axs[2,0].set_title("Insert operators (transfers)")
axs[2,0].set_xticks(np.arange(0, 51, step=5))
axs[2,0].set_xlim(0.5,51)
acc = [ratio['greedyInsT'][j]+ratio['trgtInsT'][j]+ratio['transfInsT'][j] for j in range(len(ratio['greedyInsT']))]
ratio['distInsT'] = [ratio['distInsT'][j] if acc[j]+ratio['distInsT'][j] < 0.98 else 1.0-acc[j] for j in range(len(acc))]
b1 = axs[2,0].bar(ind, ratio['distInsT'], 1.01, label='distT', hatch='x',color='m')
b2 = axs[2,0].bar(ind, ratio['greedyInsT'], 1.01, bottom=[ratio['distInsT'][j] for j in range(len(ratio['distInsT']))], label='distT', hatch='.',color='y')
b3 = axs[2,0].bar(ind, ratio['trgtInsT'], 1.01, bottom=[ratio['distInsT'][j]+ratio['greedyInsT'][j] for j in range(len(ratio['distInsT']))], label='trgtT', hatch=',',color='b')
b4 = axs[2,0].bar(ind, ratio['transfInsT'], 1.01, bottom=[ratio['distInsT'][j]+ratio['trgtInsT'][j]+ratio['greedyInsT'][j] for j in range(len(ratio['distInsT']))], label='trsfT', hatch='v', color='gray')
# axs[2,0].legend(loc='upper center', ncol=4)

axs[2,1].set_xlabel('Iterations (x500)')
axs[2,1].set_ylabel('Weight')
axs[2,1].set_xticks(np.arange(0, 51, step=5))
axs[2,1].set_xlim(0,51)
axs[2,1].grid()
p1 = axs[2,1].plot(ind_p, weights['distInsT'], label='distT',color='m')
p2 = axs[2,1].plot(ind_p, weights['greedyInsT'], label='greedyT',color='y')
p3 = axs[2,1].plot(ind_p, weights['trgtInsT'], label='trgtT',color='b')
p4 = axs[2,1].plot(ind_p, weights['transfInsT'], label='transfT',color='gray')
axs[2,1].legend((b1[0],b2[0],b3[0],b4[0]),(r'$O^+_5$',r'$O^+_6$',r'$O^+_7$',r'$O^+_8$'),loc='upper center', ncol=5)

##########################################################
with open('5_300ST.csv', 'r') as csv_file:
    try:
        reader = csv.reader(csv_file)
        ratio = {}
        weights = {}
        row_num = 0
        for row in reader:
            if row_num < 1 or len(row) < 1:
                row_num = row_num + 1
                continue
            ratio[row[0]] = []
            col_num = 0
            for col in row:
                if col_num%4 == 1:
                    ratio[row[0]].append(float(col))
                col_num = col_num + 1

            weights[row[0]] = [1.0]
            col_num = 0
            for col in row:
                if col_num%4 == 0 and col_num > 0:
                    weights[row[0]].append(float(col))
                col_num = col_num + 1
    except ValueError:
        print(row)
        print('col:',col)
        quit()

#removal
axs[0,2].set_xlabel('Iterations (x500)')
axs[0,2].set_ylabel('Ratio')
axs[0,2].set_title("Removal operators")
axs[0,2].set_xticks(np.arange(0,51, step=5))
axs[0,2].set_xlim(0.5,51)
acc = [ratio['rndRem'][j]+ratio['worstRem'][j]+ratio['routeRem'][j]+ratio['clusterRem'][j] for j in range(len(ratio['rndRem']))]
ratio['biasRem'] = [ratio['biasRem'][j] if acc[j]+ratio['biasRem'][j] < 0.98 else 1.0-acc[j] for j in range(len(acc))]
b1 = axs[0,2].bar(ind, ratio['worstRem'], 1.01, hatch='_', label='worst')
b2 = axs[0,2].bar(ind, ratio['rndRem'], 1.01, bottom=[ratio['worstRem'][j] for j in range(len(ratio['rndRem']))], hatch = '/',label='rnd')
b3 = axs[0,2].bar(ind, ratio['routeRem'], 1.01, bottom=[ratio['rndRem'][j]+ratio['worstRem'][j] for j in range(len(ratio['rndRem']))], hatch = 'x',label='route')
b4 = axs[0,2].bar(ind, ratio['biasRem'], 1.01, bottom=[ratio['rndRem'][j]+ratio['worstRem'][j]+ratio['routeRem'][j] for j in range(len(ratio['rndRem']))], hatch = '.',label='bias')
b5 = axs[0,2].bar(ind, ratio['clusterRem'], 1.01, bottom=[ratio['rndRem'][j]+ratio['worstRem'][j]+ratio['routeRem'][j]+ratio['biasRem'][j] for j in range(len(ratio['rndRem']))], hatch='+',label='cluster')
# axs[0,0].legend((b1[0],b2[0],b3[0],b4[0],b5[0]),('rndRem','worstRem','routeRem','biasRem','clusterRem'))

axs[0,3].set_xlabel('Iterations (x500)')
axs[0,3].set_ylabel('Weight')
axs[0,3].set_xticks(np.arange(0, 51, step=5))
axs[0,3].set_xlim(0,51)
axs[0,3].grid()
p1 = axs[0,3].plot(ind_p, weights['worstRem'], label='worstRem')
p2 = axs[0,3].plot(ind_p, weights['rndRem'], label='rndRem')
p3 = axs[0,3].plot(ind_p, weights['routeRem'], label='routeRem')
p4 = axs[0,3].plot(ind_p, weights['biasRem'], label='biasRem')
p5 = axs[0,3].plot(ind_p, weights['clusterRem'], label='clusterRem')
axs[0,3].legend((b1[0],b2[0],b3[0],b4[0],b5[0]),(r'$O^-_1$',r'$O^-_2$',r'$O^-_3$',r'$O^-_4$',r'$O^-_5$'),loc='upper center', ncol=3)

#insertion
#NT
axs[1,2].set_xlabel('Iterations (x500)')
axs[1,2].set_ylabel('Ratio')
axs[1,2].set_title("Insert operators (no-transfers)")
axs[1,2].set_xticks(np.arange(0, 51, step=5))
axs[1,2].set_xlim(0.5,51)
acc = [ratio['greedyInsNT'][j]+ratio['2rgtInsNT'][j]+ratio['rndInsNT'][j] for j in range(len(ratio['greedyInsNT']))]
ratio['distInsNT'] = [ratio['distInsNT'][j] if acc[j]+ratio['distInsNT'][j] < 0.98 else 1.0-acc[j] for j in range(len(acc))]
b1 = axs[1,2].bar(ind, ratio['distInsNT'], 1.01, label='distInsNT', hatch='x', color='b')
b2 = axs[1,2].bar(ind, ratio['greedyInsNT'], 1.01, bottom=ratio['distInsNT'], label='greedyInsNT', hatch='.', color='g')
b3 = axs[1,2].bar(ind, ratio['2rgtInsNT'], 1.01, bottom=[ratio['greedyInsNT'][j]+ratio['distInsNT'][j] for j in range(len(ratio['greedyInsNT']))], label='2rgtNT', hatch=',', color='r')
b4 = axs[1,2].bar(ind, ratio['rndInsNT'], 1.01, bottom=[ratio['greedyInsNT'][j]+ratio['distInsNT'][j]+ratio['2rgtInsNT'][j] for j in range(len(ratio['greedyInsNT']))], label='rndNT', hatch='v', color='c')
# axs[0].legend(loc='upper center', ncol=4)

axs[1,3].set_xlabel('Iterations (x500)')
axs[1,3].set_ylabel('Weight')
axs[1,3].set_xticks(np.arange(0, 51, step=5))
axs[1,3].set_xlim(0,51)
axs[1,3].grid()
p1 = axs[1,3].plot(ind_p, weights['distInsNT'], label='distNT', color='b') #, marker='1'
p2 = axs[1,3].plot(ind_p, weights['greedyInsNT'], label='greedyNT', color='g')
p3 = axs[1,3].plot(ind_p, weights['2rgtInsNT'], label='2rgtNT', color='r')
p4 = axs[1,3].plot(ind_p, weights['rndInsNT'], label='rndNT', color='c')
axs[1,3].legend((b1[0],b2[0],b3[0],b4[0]),(r'$O^+_1$',r'$O^+_2$',r'$O^+_3$',r'$O^+_4$'),loc='upper center', ncol=5)

#T
axs[2,2].set_xlabel('Iterations (x500)')
axs[2,2].set_ylabel('Ratio')
axs[2,2].set_title("Insert operators (transfers)")
axs[2,2].set_xticks(np.arange(0, 51, step=5))
axs[2,2].set_xlim(0.5,51)
acc = [ratio['greedyInsT'][j]+ratio['trgtInsT'][j]+ratio['transfInsT'][j] for j in range(len(ratio['greedyInsT']))]
ratio['distInsT'] = [ratio['distInsT'][j] if acc[j]+ratio['distInsT'][j] < 0.98 else 1.0-acc[j] for j in range(len(acc))]
b1 = axs[2,2].bar(ind, ratio['distInsT'], 1.01, label='distT', hatch='x',color='m')
b2 = axs[2,2].bar(ind, ratio['greedyInsT'], 1.01, bottom=[ratio['distInsT'][j] for j in range(len(ratio['distInsT']))], label='distT', hatch='.',color='y')
b3 = axs[2,2].bar(ind, ratio['trgtInsT'], 1.01, bottom=[ratio['distInsT'][j]+ratio['greedyInsT'][j] for j in range(len(ratio['distInsT']))], label='trgtT', hatch=',',color='b')
b4 = axs[2,2].bar(ind, ratio['transfInsT'], 1.01, bottom=[ratio['distInsT'][j]+ratio['trgtInsT'][j]+ratio['greedyInsT'][j] for j in range(len(ratio['distInsT']))], label='trsfT', hatch='v', color='gray')
# axs[2,0].legend(loc='upper center', ncol=4)

axs[2,3].set_xlabel('Iterations (x500)')
axs[2,3].set_ylabel('Weight')
axs[2,3].set_xticks(np.arange(0, 51, step=5))
axs[2,3].set_xlim(0,51)
axs[2,3].grid()
p1 = axs[2,3].plot(ind_p, weights['distInsT'], label='distT',color='m')
p2 = axs[2,3].plot(ind_p, weights['greedyInsT'], label='greedyT',color='y')
p3 = axs[2,3].plot(ind_p, weights['trgtInsT'], label='trgtT',color='b')
p4 = axs[2,3].plot(ind_p, weights['transfInsT'], label='transfT',color='gray')
axs[2,3].legend((b1[0],b2[0],b3[0],b4[0]),(r'$O^+_5$',r'$O^+_6$',r'$O^+_7$',r'$O^+_8$'),loc='upper center', ncol=5)

axs[2,1].text(-0.125,-0.2, "(a) Initial solution without transfers", size=20, ha="center", transform=axs[2,1].transAxes)
axs[2,3].text(-0.125,-0.2, "(b) Initial solution with transfers", size=20, ha="center", transform=axs[2,3].transAxes)

fig.tight_layout()
plt.show()
# fig.savefig('5_300S_lower.png',dpi=100)

# from matplotlib2tikz import save as tikz_save
# tikz_save('mytikz.tex',figureheight='8cm', figurewidth='16cm')
