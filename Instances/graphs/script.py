#! /usr/bin/env python3.6

import matplotlib.pyplot as plt
import numpy as np
import csv


# plotly.tools.set_credentials_file(username='afonsohs', api_key='auMInSAZDxE0tXqH90Nx')

with open('results.csv', 'r') as csv_file:
    reader = csv.reader(csv_file)
    weights = {}
    for row in reader:
        if len(row) < 2:
            continue
        weights[row[0]] = []
        col_num = 0
        for col in row:
            if col_num%4 == 0 and col_num > 0:
                weights[row[0]].append(float(col))
            col_num = col_num + 1

x = np.linspace(1, 101, 101)

fig, ax = plt.subplots()
plt.grid()
ax.set_xlim(1,100)
ax.set_ylim(0,15)
plt.xticks(np.arange(1, 101, step=4))

#removal
line1, = ax.plot(x, weights['rndRem'], label='rndRem')
line2, = ax.plot(x, weights['worstRem'], label='worstRem')
line3, = ax.plot(x, weights['routeRem'], label='routeRem')
line4, = ax.plot(x, weights['biasRem'], label='biasRem')
line5, = ax.plot(x, weights['clusterRem'], label='clusterRem')

ax.legend()
plt.show()
