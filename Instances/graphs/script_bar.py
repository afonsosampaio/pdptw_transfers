#! /usr/bin/env python3.6

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import csv
import itertools
# from matplotlib.font_manager import FontProperties

plt.style.use('seaborn-colorblind')
# print(plt.style.available)
# fontP = FontProperties()
# fontP.set_size('small')

# print(mpl.matplotlib_fname())
#ind = np.linspace(1, 50, 50)
font = {'family' : 'normal',
        'size'   : 18}

mpl.rc('font', **font)

with open('results.csv', 'r') as csv_file:
    try:
        reader = csv.reader(csv_file)
        ratio = {}
        weights = {}
        row_num = 0
        for row in reader:
            if row_num < 1 or len(row) < 1:
                row_num = row_num + 1
                continue
            ratio[row[0]] = []
            col_num = 0
            for col in row:
                if col_num%4 == 1:
                    ratio[row[0]].append(float(col))
                col_num = col_num + 1

            weights[row[0]] = [1.0]
            col_num = 0
            for col in row:
                if col_num%4 == 0 and col_num > 0:
                    weights[row[0]].append(float(col))
                col_num = col_num + 1
    except ValueError:
        print(row)
        print('col:',col)
        quit()

fig, axs = plt.subplots(3,2)
plt.subplots_adjust(left=0.02, bottom=0.02, right=0.99, top=0.98, wspace=0.05, hspace=0.15)

ind = np.linspace(1, 50, 50)
ind_p = np.linspace(0, 51, 51)

#removal
axs[0,0].set_xticks(np.arange(0,51, step=5))
axs[0,0].set_xlim(0.5,51)
b1 = axs[0,0].bar(ind, ratio['rndRem'], 1.01, hatch='_', label='rnd')
b2 = axs[0,0].bar(ind, ratio['worstRem'], 1.01, bottom=ratio['rndRem'], hatch = '/',label='worst')
b3 = axs[0,0].bar(ind, ratio['routeRem'], 1.01, bottom=[ratio['rndRem'][j]+ratio['worstRem'][j] for j in range(len(ratio['rndRem']))], hatch = 'x',label='route')
b4 = axs[0,0].bar(ind, ratio['biasRem'], 1.01, bottom=[ratio['rndRem'][j]+ratio['worstRem'][j]+ratio['routeRem'][j] for j in range(len(ratio['rndRem']))], hatch = '.',label='bias')
b5 = axs[0,0].bar(ind, ratio['clusterRem'], 1.01, bottom=[ratio['rndRem'][j]+ratio['worstRem'][j]+ratio['routeRem'][j]+ratio['biasRem'][j] for j in range(len(ratio['rndRem']))], hatch='+',label='cluster')
# axs[0,0].legend((b1[0],b2[0],b3[0],b4[0],b5[0]),('rndRem','worstRem','routeRem','biasRem','clusterRem'))

axs[0,1].set_xticks(np.arange(0, 51, step=5))
axs[0,1].set_xlim(0,51)
axs[0,1].grid()
p1 = axs[0,1].plot(ind_p, weights['rndRem'], label='rndRem')
p2 = axs[0,1].plot(ind_p, weights['worstRem'], label='worstRem')
p3 = axs[0,1].plot(ind_p, weights['routeRem'], label='routeRem')
p4 = axs[0,1].plot(ind_p, weights['biasRem'], label='biasRem')
p5 = axs[0,1].plot(ind_p, weights['clusterRem'], label='clusterRem')
axs[0,1].legend((b1[0],b2[0],b3[0],b4[0],b5[0]),('rndRem','worstRem','routeRem','biasRem','clusterRem'),loc='upper center', ncol=5)

#insertion
#NT
axs[1,0].set_xticks(np.arange(0, 51, step=5))
axs[1,0].set_xlim(0.5,51)
b1 = axs[1,0].bar(ind, ratio['greedyInsNT'], 1.01, label='greedyNT', hatch='x', color='b')
b2 = axs[1,0].bar(ind, ratio['distInsNT'], 1.01, bottom=ratio['greedyInsNT'], label='distNT', hatch='.', color='g')
b3 = axs[1,0].bar(ind, ratio['2rgtInsNT'], 1.01, bottom=[ratio['greedyInsNT'][j]+ratio['distInsNT'][j] for j in range(len(ratio['greedyInsNT']))], label='2rgtNT', hatch=',', color='r')
b4 = axs[1,0].bar(ind, ratio['rndInsNT'], 1.01, bottom=[ratio['greedyInsNT'][j]+ratio['distInsNT'][j]+ratio['2rgtInsNT'][j] for j in range(len(ratio['greedyInsNT']))], label='rndNT', hatch='v', color='c')
# axs[0].legend(loc='upper center', ncol=4)

axs[1,1].set_xticks(np.arange(0, 51, step=5))
axs[1,1].set_xlim(0,51)
axs[1,1].grid()
p1 = axs[1,1].plot(ind_p, weights['greedyInsNT'], label='greedyNT', color='b') #, marker='1'
p2 = axs[1,1].plot(ind_p, weights['distInsNT'], label='distNT', color='g')
p3 = axs[1,1].plot(ind_p, weights['2rgtInsNT'], label='2rgtNT', color='r')
p4 = axs[1,1].plot(ind_p, weights['rndInsNT'], label='rndNT', color='c')
axs[1,1].legend((b1[0],b2[0],b3[0],b4[0]),('greedyNT','distNT','2rgtNT','rndNT'),loc='upper center', ncol=5)

#T
accumulated_NT = [0 for j in range(len(ratio['distInsT']))]
axs[2,0].set_xticks(np.arange(0, 51, step=5))
axs[2,0].set_xlim(0.5,51)
b1 = axs[2,0].bar(ind, ratio['distInsT'], 1.01, bottom=accumulated_NT, label='distT', hatch='x',color='m')
b2 = axs[2,0].bar(ind, ratio['trgtInsT'], 1.01, bottom=[accumulated_NT[j]+ratio['distInsT'][j] for j in range(len(ratio['distInsT']))], label='trgtT', hatch='.',color='y')
b3 = axs[2,0].bar(ind, ratio['greedyInsT'], 1.01, bottom=[accumulated_NT[j]+ratio['distInsT'][j]+ratio['trgtInsT'][j] for j in range(len(ratio['distInsT']))], label='distT', hatch=',',color='b')
b4 = axs[2,0].bar(ind, ratio['transfInsT'], 1.01, bottom=[accumulated_NT[j]+ratio['distInsT'][j]+ratio['trgtInsT'][j]+ratio['greedyInsT'][j] for j in range(len(ratio['distInsT']))], label='trsfT', hatch='v', color='gray')
# axs[2,0].legend(loc='upper center', ncol=4)

axs[2,1].set_xticks(np.arange(0, 51, step=5))
axs[2,1].set_xlim(0,51)
axs[2,1].grid()
p1 = axs[2,1].plot(ind_p, weights['distInsT'], label='distT',color='m')
p2 = axs[2,1].plot(ind_p, weights['trgtInsT'], label='trgtT',color='y')
p3 = axs[2,1].plot(ind_p, weights['greedyInsT'], label='greedyT',color='b')
p4 = axs[2,1].plot(ind_p, weights['transfInsT'], label='transfT',color='gray')
axs[2,1].legend((b1[0],b2[0],b3[0],b4[0]),('distT','trgtT','greedyT','transfT'),loc='upper center', ncol=5)

# Put a legend below current axis
# plt.legend(loc='upper center', ncol=4)

# fig.tight_layout()
plt.show()

# from matplotlib2tikz import save as tikz_save
# tikz_save('mytikz.tex',figureheight='8cm', figurewidth='16cm')
