#! /usr/bin/env python3.6

import sys
import random
import math
import json
import argparse
import re
import numpy as np

def main(parameters, s, instanceName, gen_JSON):
    outfile = open(instanceName+'.ins', 'w')

    seeds = []
    with open('seeds') as f:
        for line in f:
            # print(int(line))
            seeds.append(int(line))

    random.seed(seeds[s])
    np.random.seed(seeds[s])
    print('seed {0}'.format(seeds[s]))

    js_data = {}
    js_data['requests'] = {}
    nReq = 1

    R = parameters['requests']  #number of requests in the instance
    Q = parameters['capacity']  #capacity (not being used...)
    H = parameters['horizon']   #vehicles shift
    Lx = parameters['length_x'] #
    Ly = parameters['length_y'] #
    L = max(Lx/2, Ly/2)
    depots = parameters['depots']

    t_pd = 0
    t_0p = 0
    t_d0 = 0
    average_od_distance = 0
    min_od_distance = 999999
    max_od_distance = -999999
    for r in range(0,R):
        feasible = False
        t_pd = 999999
        t_0p = 999999
        t_d0 = 0
        while(not feasible):
            xp = random.randint(0,Lx)
            yp = random.randint(0,Ly)
            xd = random.randint(0,Lx)
            yd = random.randint(0,Ly)

            t_pd = math.ceil(distance(xp,yp,xd,yd))

            type = random.randint(0,1)  #for mixed instances

            if(type == 1):
                if(t_pd>L or t_pd<30): #short
                    continue

            if(type == 0):
                if(t_pd<L): #long
                    continue

            if(t_pd>90):
                continue

            lp = np.random.randint(60,480-t_pd)
            ld = lp + t_pd
            ep = lp - 60
            ed = ld - 60

            for dep in depots:
                xdep = depots[dep]['x']
                ydep = depots[dep]['y']
                aux1 = math.ceil(distance(xdep,ydep,xp,yp))
                aux2 = math.ceil(distance(xd,yd,xdep,ydep))
                # if depots[dep]['l'] + aux1 + t_pd + aux2 <= depots[dep]['u']:
                if depots[dep]['l'] + aux1 <= lp:
                    if ld + aux2 <= depots[dep]['u']:
                        feasible = True
                    # if t_0p > depots[dep]['l'] + aux1:
                    #     t_0p = depots[dep]['l'] + aux1
                    # if t_d0 < min(depots[dep]['u'] - aux2, H):
                    #     t_d0 = min(depots[dep]['u'] - aux2, H)

        js_data['requests']['r_'+str(nReq)] = {}
        js_data['requests']['r_'+str(nReq)]['p'] = {}
        js_data['requests']['r_'+str(nReq)]['d'] = {}
        js_data['requests']['r_'+str(nReq)]['q'] = 1

        js_data['requests']['r_'+str(nReq)]['p']['id'] = nReq
        js_data['requests']['r_'+str(nReq)]['p']['x'] = int(xp)
        js_data['requests']['r_'+str(nReq)]['p']['y'] = int(yp)
        js_data['requests']['r_'+str(nReq)]['p']['l'] = ep#int(t_0p)#0
        js_data['requests']['r_'+str(nReq)]['p']['u'] = lp#int(t_d0-t_pd)#int(H - t_d0 - t_pd)#180-t_pd
        js_data['requests']['r_'+str(nReq)]['d']['id'] = int(nReq + R)
        js_data['requests']['r_'+str(nReq)]['d']['x'] = int(xd)
        js_data['requests']['r_'+str(nReq)]['d']['y'] = int(yd)
        js_data['requests']['r_'+str(nReq)]['d']['l'] = ed#int(t_0p+t_pd)#int(t_0p + t_pd)#int(t_pd)
        js_data['requests']['r_'+str(nReq)]['d']['u'] = ld#int(t_d0)#int(H - t_d0)#180

#       print('{0}: [{1},{2}], [{3},{4}]'.format(r,ep,lp,ed,ld))

        # if(int(t_d0-t_pd) < int(t_0p) or int(t_d0) < int(t_0p+t_pd)):
        #     print('request {0} infeasible'.format(r+1))

        js_data['requests']['r_'+str(nReq)]['p']['s'] = 0#np.random.randint(5,16)
        js_data['requests']['r_'+str(nReq)]['d']['s'] = 0#np.random.randint(5,16)
        nReq = nReq + 1

        #instance properties
        average_od_distance = average_od_distance + t_pd
        if(max_od_distance < t_pd):
            max_od_distance = t_pd
        if(min_od_distance > t_pd):
            min_od_distance = t_pd

    #parameters
    outfile.write(str(2*R)+'\t1\t'+str(Q)+'\n') #number of vehicles is irrelevant if min vehicles

    #depot
    outfile.write('0\t'+str(int(Lx/2))+'\t'+str(int(Ly/2))+'\t0\t0\t'+str(H)+'\t0\t0\t0\n')

    #pickups
    for r in sorted(js_data['requests'], key=lambda x: js_data['requests'][x]['p']['id']):
        aux = str(js_data['requests'][r]['p']['id']) + '\t' + str(js_data['requests'][r]['p']['x']) + '\t'
        aux = aux + str(js_data['requests'][r]['p']['y']) + '\t' + str(js_data['requests'][r]['q']) + '\t'
        aux = aux + str(js_data['requests'][r]['p']['l']) + '\t' + str(js_data['requests'][r]['p']['u']) + '\t'
        aux = aux + str(js_data['requests'][r]['p']['s']) + '\t'
        aux = aux + '0\t' + str(js_data['requests'][r]['d']['id'])+'\n'
        outfile.write(aux)
    #deliveries
    for r in sorted(js_data['requests'], key=lambda x: js_data['requests'][x]['d']['id']):
        aux = str(js_data['requests'][r]['d']['id']) + '\t' + str(js_data['requests'][r]['d']['x']) + '\t'
        aux = aux + str(js_data['requests'][r]['d']['y']) + '\t' + str(-1*js_data['requests'][r]['q']) + '\t'
        aux = aux + str(js_data['requests'][r]['d']['l']) + '\t' + str(js_data['requests'][r]['d']['u']) + '\t'
        aux = aux + str(js_data['requests'][r]['d']['s']) + '\t'
        aux = aux + str(js_data['requests'][r]['p']['id'])+'\t0\n'
        outfile.write(aux)
    outfile.close()

    #instance properties
    average_od_distance = average_od_distance / nReq
    index = average_od_distance / (math.sqrt(Lx*Lx + Ly*Ly))
    print('od pairing index:',index)
    print('max od distance:',max_od_distance)
    print('min od distance:',min_od_distance)

    if gen_JSON == 1:
        js_data['depots'] = depots
        js_data['factor'] = float('{0:.3f}'.format(index))
        file_name_json = instanceName
        file_name_json += '.json'
        with open(file_name_json, 'w') as outfile:
          json.dump(js_data, outfile)
    js_data.clear()

    exit(0)

def distance(x1,y1,x2,y2):
    return math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='PDPTW instance generator')
    parser.add_argument('-f', '--file', type=str, help='Output file', required=True)
    parser.add_argument('-s', '--seed', type=int, help='Seed to use on random', required=False, default=0)
    parser.add_argument('-j', '--json', type=int, help='Generate a JSON file', required=False, default=0)
    parser.add_argument('-p', '--param', type=str, help='Parameters file', required=True)

    #parse arguments
    args = parser.parse_args()

    # read the instance file
    json_file = open(args.param)
    json_data = json.load(json_file)
    json_file.close()

    main(json_data, args.seed, args.file, args.json)
