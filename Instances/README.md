#Pickup and Delivery Problem with Time Windows and Transfers (PDPTW-T)

##Pickup and Delivery Requests Information (*.ins file)
Those files contain information regarding the requests. Format is the one used in [Ropke and Psinger,2006] for standart PDPTW instances. The format is the following:

[C V Q]
[i0 x y q e l s p d]
[i1 x y q e l s p d]
...
[iC x y q e l s p d]

The first line contains the number of customer nodes, C (so in total there are 0.5*C requests, as each request has two customer nodes i.e. pickup and delivery), the maximum number of vehicles available, V, and the capacity of each vehicle, Q. Note that V is not used in our experiments, as we consider that there is at least one vehicle per request.

The next lines describes each node. The first number (i) is the identificator of the node (0 is used for the depot). x and y are the coordinates of the location in the plane, q is the customer demand, e and l are the start and end service time windows, respectively, s is the service time, p is the corresponding pickup node id and d the corresponding delivery node id.

Note that information regarding (multiple) depots should be provided separately (for example, in [Ropke and Psinger,2006]'s ALNS you have to use format 23). The *.ins files are used to solve the PDPTW (using [Ropke and Psinger,2006]'s ALNS), and to construct the PDPTW-T instances used in our ALSN as described next.


##Full instances information (*.json files)
Those files contain all information necessary to solve an instance of the PDPTW-T using our proposed ALNS. The format specification is as follows:

{
	"requests":{
		"p": {"id":Number, "x": Number, "y": Number, "l": Number, "u": Number},
 		"d": {"id": Number, "x": Number, "y": Number, "l": Number, "u": Number},
    "q": Number
	},
	"depots":{
		"id": Number, "x": Number, "y": Number, "l": Number, "u": Number
	},
	"transfers":{
		"id": Number, "x": Number, "y": Number
	},
	"routes": {
		"id":Array.of(String)
	},
}

The "requests" field describes customers pickup and delivery requests. For each request, field "p" describes the pickup and field "d" the delivery. Within each field, id is the node identificator, x and y the coordinates, l and u the start and end service time windows, respectively. Field q is the request demand. The format for the "depots" and "transfers" is similar to the "p" and "d" fields (observe that, for depots, l and u specify the time horizon in which vehicles from that depot are available).
The "routes" field is used to specify an initial (partial) solution. For example, the PDPTW solution obtained with [Ropke and Psinger,2006]'s ALSN could be passed in this field. Requests not included in the routes are inserted by the ALNS using the available transfers. A small example of an instance is provided below:

{
	"requests":{
		"r1": {"p": {"id": 1, "x": 8, "y": 116, "l": 0, "u": 119}, "d": {"id": 11, "x": 63, "y": 90, "l": 61, "u": 180}, "q": 1},
		"r2": {"p": {"id": 2, "x": 65, "y": 39, "l": 0, "u": 117}, "d": {"id": 12, "x": 78, "y": 100, "l": 63, "u": 180}, "q": 1},
		"r3": {"p": {"id": 3, "x": 100, "y": 0, "l": 0, "u": 94}, "d": {"id": 13, "x": 101, "y": 85, "l": 86, "u": 180}, "q": 1}
	},
 "depots":{
		"d1": {"id": 10000, "x": 30, "y": 30, "l": 0, "u": 180},
		"d2": {"id": 20000, "x": 90, "y": 30, "l": 0, "u": 180}
	},
	"transfers":{
		"t1": {"id": 1000, "x": 60, "y": 90},
		"t2": {"id": 2000, "x": 90, "y": 60}
	},
	"routes": {
		"r_0": ["d1", "1", "11", "d1"],
		"r_1": ["d1", "3", "13", "d1"],
		"r_2": ["d2", "2", "12", "d2"]
	}
}

The instance contains three requests, two depots and two transfers. An initial solution is provided, where each request is served by a dedicated vehicle (servicing both pickup and delivery of a request).
