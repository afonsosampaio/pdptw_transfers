#! /usr/bin/env python3.6

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import json

json_file = open('../../ALNS/t_stagg25_long2.json')
json_data = json.load(json_file)

fig, axs = plt.subplots(1,1)
plt.subplots_adjust(left=0.05, bottom=0.08, right=0.95, top=0.96, wspace=0.2, hspace=0.30)

xs = []
ys = []

colors = cm.nipy_spectral(np.linspace(0, 10, 250))

ci = 0
for r in json_data['requests']:
    axs.scatter(json_data['requests'][r]['p']['x'], json_data['requests'][r]['p']['y'], s=100, marker='o', c=colors[ci])
    axs.scatter(json_data['requests'][r]['d']['x'], json_data['requests'][r]['d']['y'], s=100, marker='o', c=colors[ci])
    ci = ci + 1
    # xs.append(json_data['requests'][r]['p']['x'])
    # ys.append(json_data['requests'][r]['p']['y'])
    # xs.append(json_data['requests'][r]['d']['x'])
    # ys.append(json_data['requests'][r]['d']['y'])

# s1 = axs.scatter(xs, ys, s=200, marker='o', c='k')

plt.show()
