#! /usr/bin/env python3.6

import sys
import random
import math
import json
import argparse
import re
import pandas as pd

def main(instanceName):
    js_data = {}
    js_data['requests'] = {}
    nReq = 1

    file = open(instanceName, "r")

    matrix_lines = set()

    pickups = []
    deliveries = []
    transfers = []

    #first line veh, req, trans, st, E, L
    line = file.readline()
    inputs = line.strip().split(' ')
    V = int(inputs[0])
    R = int(inputs[1])
    T = int(inputs[2])
    Q = 0
    H = 0

    # Vehicles
    for i in range(1,V): #reads vehicle info //DO IT MANUALLY
        file.readline()
    line = file.readline().strip().split('\t')
    H = line[2]
    Q = line[-1]

    # Requests
    nReq = 1
    for i in range(0,R):   #every pair of lines is a request
        js_data['requests']['r_'+str(nReq)] = {}
        js_data['requests']['r_'+str(nReq)]['p'] = {}
        js_data['requests']['r_'+str(nReq)]['d'] = {}
        js_data['requests']['r_'+str(nReq)]['q'] = 1

        line = file.readline()
        pickup = line.strip().split('\t')
        line = file.readline()
        delivery = line.strip().split('\t')

        pickups.append(pickup[0])
        deliveries.append(delivery[0])

        matrix_lines.add(int(pickup[0]))
        matrix_lines.add(int(delivery[0]))

        js_data['requests']['r_'+str(nReq)]['p']['id'] = nReq
        js_data['requests']['r_'+str(nReq)]['p']['x'] = 0
        js_data['requests']['r_'+str(nReq)]['p']['y'] = 0
        js_data['requests']['r_'+str(nReq)]['p']['l'] = int(pickup[3])
        js_data['requests']['r_'+str(nReq)]['p']['u'] = int(pickup[4])
        js_data['requests']['r_'+str(nReq)]['d']['id'] = int(nReq + R)
        js_data['requests']['r_'+str(nReq)]['d']['x'] = 0
        js_data['requests']['r_'+str(nReq)]['d']['y'] = 0
        js_data['requests']['r_'+str(nReq)]['d']['l'] = int(delivery[3])
        js_data['requests']['r_'+str(nReq)]['d']['u'] = int(delivery[4])

        js_data['requests']['r_'+str(nReq)]['p']['s'] = int(pickup[2])
        js_data['requests']['r_'+str(nReq)]['d']['s'] = int(delivery[2])
        nReq = nReq + 1

    # Transfers
    for i in range(0,T):
        line = file.readline()
        transfer = line.strip().split('\t')
        transfers.append(int(transfer[0]))

    outfile = open('test.ins', 'w')

    #parameters
    outfile.write(str(2*R)+'\t1\t'+str(Q)+'\n') #number of vehicles is irrelevant if min vehicles

    #depot
    outfile.write('0\t'+str(int(0))+'\t'+str(int(0))+'\t0\t0\t'+str(H)+'\t0\t0\t0\n')

    #pickups
    for r in sorted(js_data['requests'], key=lambda x: js_data['requests'][x]['p']['id']):
        aux = str(js_data['requests'][r]['p']['id']) + '\t' + str(js_data['requests'][r]['p']['x']) + '\t'
        aux = aux + str(js_data['requests'][r]['p']['y']) + '\t' + str(js_data['requests'][r]['q']) + '\t'
        aux = aux + str(js_data['requests'][r]['p']['l']) + '\t' + str(js_data['requests'][r]['p']['u']) + '\t'
        aux = aux + str(js_data['requests'][r]['p']['s']) + '\t'
        aux = aux + '0\t' + str(js_data['requests'][r]['d']['id'])+'\n'
        outfile.write(aux)
    #deliveries
    for r in sorted(js_data['requests'], key=lambda x: js_data['requests'][x]['d']['id']):
        aux = str(js_data['requests'][r]['d']['id']) + '\t' + str(js_data['requests'][r]['d']['x']) + '\t'
        aux = aux + str(js_data['requests'][r]['d']['y']) + '\t' + str(-1*js_data['requests'][r]['q']) + '\t'
        aux = aux + str(js_data['requests'][r]['d']['l']) + '\t' + str(js_data['requests'][r]['d']['u']) + '\t'
        aux = aux + str(js_data['requests'][r]['d']['s']) + '\t'
        aux = aux + str(js_data['requests'][r]['p']['id'])+'\t0\n'
        outfile.write(aux)
    outfile.close()

    with open('test.json', 'w') as outfile:
        json.dump(js_data, outfile)
    js_data.clear()

    # print(matrix_lines)
    pk = sorted([int(i) for i in pickups])
    dv = sorted([int(i) for i in deliveries])
    tr = sorted(transfers)
    print(pk)
    print(dv)
    print(transfers)

    col_names = [str(i) for i in range(0,583)]
    matrix = pd.read_csv('adapei_matrix.csv', names=col_names, header=None)

    # matrix format:
    # p1 p2 .. pn d1 d2 ... dn T1 T2 ... T D1 D2 ... D
    # p1
    # p2
    # ...
    # d1
    # d2
    # ...
    # dn
    # T1
    # ...
    # T
    # D1
    # ...
    # D

    all = pk+dv+tr
    df = pd.DataFrame(matrix, columns=[str(i) for i in all], index=[i for i in all])
    print(df.head())

    # dfilter = df[[str(i) for i in pk]]
    # print(matrix.loc[6,[str(i) for i in pk]])

    df.to_csv('filtered.csv')
    exit(0)

def distance(x1,y1,x2,y2):
    return math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='PDPTW instance generator')
    parser.add_argument('-f', '--file', type=str, help='input file', required=True)

    #parse arguments
    args = parser.parse_args()

    main(args.file)
